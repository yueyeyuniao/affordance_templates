/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/

#include <affordance_template_markers/affordance_template.h>

using namespace affordance_template;
using namespace affordance_template_object;
using namespace affordance_template_markers;
using namespace affordance_template_msgs;

AffordanceTemplate::AffordanceTemplate(const ros::NodeHandle nh, 
                                       boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server, 
                                       std::string library_pkg,                               
                                       std::string robot_name, 
                                       std::string template_type,
                                       int id) :
  nh_(nh),
  server_(server),
  library_pkg_(library_pkg),                                       
  robot_name_(robot_name),
  template_type_(template_type),
  id_(id),
  root_object_(""),
  loop_rate_(10.0),
  planning_server_(nh, (template_type + "_" + std::to_string(id) + "/plan_action"), boost::bind(&AffordanceTemplate::planRequest, this, _1), false),
  execution_server_(nh, (template_type + "_" + std::to_string(id) + "/execute_action"), boost::bind(&AffordanceTemplate::executeRequest, this, _1), false)
{
  ROS_DEBUG("AffordanceTemplate::init() -- creating new AffordanceTemplate of type %s for robot: %s", template_type_.c_str(), robot_name_.c_str());
  name_ = template_type_ + ":" + std::to_string(id);

  setupMenuOptions();

  std::string node_name = ros::this_node::getName();

  nh_.param<bool>(node_name+"/scale_ee_speed", scale_ee_speed_, true);

  updated_frames=false;
  build_request=false;

  planning_server_.start();
  execution_server_.start();
 
  updateThread_.reset(new boost::thread(boost::bind(&AffordanceTemplate::run, this)));

  // set to false when template gets destroyed, otherwise we can get a dangling pointer
  running_ = true;
  autoplay_display_ = true;
}


AffordanceTemplate::AffordanceTemplate(const ros::NodeHandle nh, 
                                       boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server,  
                                       boost::shared_ptr<affordance_template_markers::RobotInterface> robot_interface,
                                       std::string library_pkg,
                                       std::string robot_name, 
                                       std::string template_type,
                                       int id) :
AffordanceTemplate(nh, server, library_pkg, robot_name, template_type, id)
{
  setRobotInterface(robot_interface);
}

AffordanceTemplate::~AffordanceTemplate() 
{
  updateThread_->interrupt();
  robot_interface_->getPlanner()->resetAnimation(true);

  plan_status_.clear();
  clearTrajectoryFlags();
}

void AffordanceTemplate::setRobotInterface(boost::shared_ptr<affordance_template_markers::RobotInterface> robot_interface)
{
  robot_interface_ = robot_interface;

  std::map<int, std::string> ee_list = robot_interface_->getEENameMap();
  for (auto ee : ee_list) {
    robot_interface_->getPlanner()->loopAnimation(ee.second, false);
    robot_interface_->getPlanner()->loopAnimation(robot_interface_->getManipulator(ee.second), false);
  }
}

void AffordanceTemplate::setupMenuOptions() 
{
  waypoint_menu_options_.clear();
  waypoint_menu_options_.push_back(MenuConfig("Change End-Effector Pose", false));
  waypoint_menu_options_.push_back(MenuConfig("Hide Controls", true));
  waypoint_menu_options_.push_back(MenuConfig("Compact View", true));
  waypoint_menu_options_.push_back(MenuConfig("Add Waypoint Before", false));
  waypoint_menu_options_.push_back(MenuConfig("Add Waypoint After", false));
  waypoint_menu_options_.push_back(MenuConfig("Delete Waypoint", false));
  waypoint_menu_options_.push_back(MenuConfig("Move Forward", false));
  waypoint_menu_options_.push_back(MenuConfig("Move Back", false));
  waypoint_menu_options_.push_back(MenuConfig("Sync To Actual", false));
  waypoint_menu_options_.push_back(MenuConfig("Change Tool Offset", true));
  waypoint_menu_options_.push_back(MenuConfig("Move Tool Point", true));
  waypoint_menu_options_.push_back(MenuConfig("Use Cartesian Plans", true));
  waypoint_menu_options_.push_back(MenuConfig("Conditioning Metric", false));
  waypoint_menu_options_.push_back(MenuConfig("Position Tolerance", false));
  waypoint_menu_options_.push_back(MenuConfig("Orientation Tolerance", false));

  object_menu_options_.clear();
  object_menu_options_.push_back(MenuConfig("Add Waypoint Before", false));
  object_menu_options_.push_back(MenuConfig("Add Waypoint After", false));
  object_menu_options_.push_back(MenuConfig("Reset", false));
  object_menu_options_.push_back(MenuConfig("Save", false));
  object_menu_options_.push_back(MenuConfig("Hide Controls", true));
  object_menu_options_.push_back(MenuConfig("Choose Trajectory", false));
  object_menu_options_.push_back(MenuConfig("(Re)Play Plan", false));
  object_menu_options_.push_back(MenuConfig("Loop Animation", true));
  object_menu_options_.push_back(MenuConfig("Autoplay", true));

  toolpoint_menu_options_.clear();
  toolpoint_menu_options_.push_back(MenuConfig("Change Tool Offset", true));
  toolpoint_menu_options_.push_back(MenuConfig("Move Tool Point", true));
}

void AffordanceTemplate::setObjectMenuDefaults(std::string obj_name) {
  MenuHandleKey key;
  key[obj_name] = {"Hide Controls"};
  if(object_controls_display_on_.find(obj_name)==object_controls_display_on_.end()) {
    object_controls_display_on_[obj_name] = false;
  }
  if(object_controls_display_on_[obj_name]) {
    marker_menus_[obj_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  } else {
    marker_menus_[obj_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  }

  key[obj_name] = {"Autoplay"};
  if(autoplay_display_) {
    marker_menus_[obj_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    marker_menus_[obj_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
}

void AffordanceTemplate::setWaypointMenuDefaults(std::string wp_name) {
  MenuHandleKey key;

  std::string current_trajectory = current_trajectory_;

  if(waypoint_flags_[current_trajectory].run_backwards) {
    key[wp_name] = {"Compute Backwards Path"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  }  
  if(waypoint_flags_[current_trajectory].auto_execute) {
    key[wp_name] = {"Execute On Move"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  }  
  if(waypoint_flags_[current_trajectory].loop) {
    key[wp_name] = {"Loop Path"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  }  
  if(waypoint_flags_[current_trajectory].controls_on[wp_name]) {
    key[wp_name] = {"Hide Controls"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  } else {
    key[wp_name] = {"Hide Controls"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  }
  if(waypoint_flags_[current_trajectory].compact_view[wp_name]) {
    key[wp_name] = {"Compact View"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    key[wp_name] = {"Compact View"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
  if(waypoint_flags_[current_trajectory].adjust_offset[wp_name]) {
    key[wp_name] = {"Change Tool Offset"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    key[wp_name] = {"Change Tool Offset"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
  if(waypoint_flags_[current_trajectory].move_offset[wp_name]) {
    key[wp_name] = {"Move Tool Point"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    key[wp_name] = {"Move Tool Point"};
    marker_menus_[wp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
}

void AffordanceTemplate::setToolPointMenuDefaults(std::string tp_name) {
  MenuHandleKey key;

  std::string current_trajectory = current_trajectory_;

  std::string wp_name = getWaypointFrame(tp_name);
  if(waypoint_flags_[current_trajectory].adjust_offset[wp_name]) {
    key[tp_name] = {"Change Tool Offset"};
    marker_menus_[tp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    key[tp_name] = {"Change Tool Offset"};
    marker_menus_[tp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
  if(waypoint_flags_[current_trajectory].move_offset[wp_name]) {
    key[tp_name] = {"Move Tool Point"};
    marker_menus_[tp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
  } else {
    key[tp_name] = {"Move Tool Point"};
    marker_menus_[tp_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }
}

bool AffordanceTemplate::loadFromFile(std::string filename, geometry_msgs::Pose pose, AffordanceTemplateStructure &structure)
{
  at_parser_.loadFromFile(filename, structure);

  // store copies in class, one as backup to reset/restore later
  initial_structure_ = structure;
  structure_ = structure;

  bool append = appendIDToStructure(structure_);
  bool created = buildTemplate();

  return (append && created);
}

bool AffordanceTemplate::saveToDisk(std::string& filename, const std::string& image, const std::string& key, bool save_scale_updates)
{
  std::string class_type = template_type_;
  if (filename.empty())
    filename = template_type_ + ".json";
  else
    {
      std::vector<std::string> keys;
      boost::split(keys, filename, boost::is_any_of("."));
      if (keys.size())
        class_type = keys.front();
    }

  std::string root = ros::package::getPath(library_pkg_);
  if (root.empty())
    return false;
  root += "/templates";
  std::string output_path = root + "/" + filename;

  // if filename given is actually directory, return
  if (boost::filesystem::is_directory(output_path))
    {
      ROS_ERROR("[AffordanceTemplate::saveToDisk] error formatting filename!!");
      return false;
    }

  ROS_INFO("[AffordanceTemplate::saveToDisk] writing template to file: %s", output_path.c_str());

  if (!boost::filesystem::exists(output_path)) {
    ROS_WARN("[AffordanceTemplate::saveToDisk] no file found with name: %s. cannot create backup.", filename.c_str());
  } else {
    // count how many bak files we have for this particular template type
    boost::filesystem::recursive_directory_iterator dir_it(root);
    boost::filesystem::recursive_directory_iterator end_it;
    int bak_counter = 0;
    while (dir_it != end_it) {
      if (std::string(dir_it->path().string()).find(".bak") != std::string::npos
          && std::string(dir_it->path().string()).find(class_type) != std::string::npos)
        ++bak_counter;
      ++dir_it;
    }

    // copy current class_type.json into .bak
    std::string bak_path = "";
    if (bak_counter > 0) {
      ROS_INFO("[AffordanceTemplate::saveToDisk] creating backup file: %s.bak%d", filename.c_str(), bak_counter);
      bak_path = output_path + ".bak" + std::to_string(bak_counter);
    } else {
      ROS_INFO("[AffordanceTemplate::saveToDisk] creating backup file: %s.bak", filename.c_str());
      bak_path = output_path + ".bak";
    }
    boost::filesystem::copy_file(output_path, bak_path, boost::filesystem::copy_option::overwrite_if_exists);
  }

  // set structure key as the new key name
  std::vector<std::string> keys;
  boost::split(keys, key, boost::is_any_of(":"));
  if (keys.size())
    structure_.name = keys.front();

  if (at_parser_.saveToFile(output_path, structure_)) {
    buildTemplate();
    return true;
  }

  return false; // should only get here if we couldn't save to file
}

bool AffordanceTemplate::resetTemplate()
{
  ROS_DEBUG("AffordanceTemplate::resetTemplate] resetting current structure to the inital structure.");
  structure_ = initial_structure_;      
  appendIDToStructure(structure_);
  removeAllMarkers();
  clearTrajectoryFlags();
  buildTemplate(); 
  return true;
}


bool AffordanceTemplate::appendIDToStructure(AffordanceTemplateStructure &structure) 
{
  structure.name = appendID(structure.name);
  for(auto &obj : structure.display_objects) {
    obj.name = appendID(obj.name);
    if(obj.parent != "") {
      obj.parent = appendID(obj.parent);
    }
  }
  return true;
}
 
std::string AffordanceTemplate::createWaypointID(int ee_id, int wp_id)
{
  return std::to_string(ee_id) + "." + std::to_string(wp_id) + ":" + name_;
}

std::string AffordanceTemplate::appendID(std::string s) 
{
  return s + ":" + std::to_string(id_);
} 

std::string AffordanceTemplate::removeID(std::string s)
{
  size_t endpos = s.rfind(":");
  if( std::string::npos != endpos ) {
    return s.substr(0, endpos);
  }
}
    
int AffordanceTemplate::getWaypointIDFromName(std::string wp_name)
{
  std::size_t dot_pos = wp_name.find(".");
  std::size_t colon_pos = wp_name.find(":");
  if(dot_pos == std::string::npos || colon_pos == std::string::npos) {
    ROS_ERROR("[AffordanceTemplate::getWaypointIDFromName] malformed wp name %s", wp_name.c_str());
    return -1;
  }
  int wp_id = stoi(wp_name.substr(dot_pos + 1,colon_pos));  
  return wp_id;
}

int AffordanceTemplate::getEEIDFromName(std::string wp_name)
{
  std::size_t dot_pos = wp_name.find(".");
  if(dot_pos == std::string::npos) {
    ROS_ERROR("[AffordanceTemplate::getEEIDFromName] malformed wp name %s", wp_name.c_str());
    return -1;
  }
  int ee_id = stoi(wp_name.substr(0,dot_pos));  
  return ee_id;
}


bool AffordanceTemplate::addTrajectory(const std::string& trajectory_name) 
{
  affordance_template_object::Trajectory traj;
  traj.name = trajectory_name;
  structure_.ee_trajectories.push_back(traj);
  setTrajectory(trajectory_name);
  setupTrajectoryMenu(trajectory_name);
  return buildTemplate(trajectory_name);
}

bool AffordanceTemplate::getTrajectory(TrajectoryList& traj_list, std::string traj_name, Trajectory& traj) 
{
  for (auto &t: traj_list) {
    if(t.name == traj_name) {
      traj = t;
      return true;
    }
  }
  ROS_ERROR("[AffordanceTemplate::getTrajectory] could not find %s in list of trajectories", traj_name.c_str());
  return false;
}

bool AffordanceTemplate::getTrajectoryPlan(const std::string& trajectory, const std::string& ee, PlanStatus& plan)
{
  m_plan_.lock();
  if (plan_status_.find(trajectory) != plan_status_.end()) {
    if (plan_status_[trajectory].find(ee) != plan_status_[trajectory].end()) {
      plan = plan_status_[trajectory][ee];
    } else {
      m_plan_.unlock();
      return false;
    }
  } else {
    m_plan_.unlock();
    return false;
  }
  m_plan_.unlock();
  return true;
}

bool AffordanceTemplate::setTrajectory(const std::string& trajectory_name)
{
  return setCurrentTrajectory( getCurrentStructure().ee_trajectories, trajectory_name);
}

bool AffordanceTemplate::switchTrajectory(const std::string& trajectory_name)
{
  removeAllMarkers();
  if(setTrajectory(trajectory_name)) {
    setupTrajectoryMenu(trajectory_name);
    if(buildTemplate(trajectory_name)) {
      ROS_INFO("[AffordanceTemplate::switchTrajectory] %s succeeded", trajectory_name.c_str());
    } else {
      ROS_ERROR("[AffordanceTemplate::switchTrajectory] %s failed", trajectory_name.c_str());
      return false;
    }
  }
  return true;
}

void AffordanceTemplate::clearTrajectoryFlags()
{
  waypoint_flags_.clear();
}

bool AffordanceTemplate::getWaypointFlags(const std::string& traj, WaypointTrajectoryFlags& flags) {
  if(waypoint_flags_.find(traj)!=waypoint_flags_.end()) {
    flags = waypoint_flags_[traj];
  } else {
    ROS_DEBUG("AffordanceTemplate::getWaypointFlags() -- no traj=%s found", traj.c_str());
    return false;
  }
  return true;
}
    
void AffordanceTemplate::setTrajectoryFlags(Trajectory traj) 
{  
  if(waypoint_flags_.find(traj.name) == std::end(waypoint_flags_)) {
    WaypointTrajectoryFlags wp_flags;
    wp_flags.run_backwards = false;
    wp_flags.loop = false;
    wp_flags.auto_execute = false;
    for(auto &ee : traj.ee_waypoint_list) {
      for(size_t idx=0; idx<ee.waypoints.size(); idx++) {
        std::string wp_name = createWaypointID(ee.id,idx);
        wp_flags.controls_on[wp_name] = false;
        wp_flags.compact_view[wp_name] = true;
        wp_flags.adjust_offset[wp_name] = false;
        wp_flags.move_offset[wp_name] = false;
      }
    }
    waypoint_flags_[traj.name] = wp_flags;
  }
}

bool AffordanceTemplate::isValidTrajectory(Trajectory traj)  
{
  bool valid_trajectory = true;
  for(auto &g: traj.ee_waypoint_list) {
    for(auto &wp: g.waypoints) {
      if(robot_interface_->getEENameMap().find(g.id) == std::end(robot_interface_->getEENameMap())) {
        valid_trajectory = false;
        ROS_DEBUG("AffordanceTemplate::is_valid_trajectory() -- can't find ee ID: %d", g.id);
        return valid_trajectory;
      }
    }
  }
  return valid_trajectory;
} 

// set the default (current) traj to the input one.
// if no input request, find the first valid one
bool AffordanceTemplate::setCurrentTrajectory(TrajectoryList traj_list, std::string traj) 
{
  ROS_DEBUG("AffordanceTemplate::setCurrentTrajectory() -- will attempt to set current trajectory to %s", traj.c_str());
  bool found = false;
  if ( !traj.empty()) {
    for (auto &t: traj_list) {
      if(t.name == traj) {
        if(isValidTrajectory(t)) {
          current_trajectory_ = t.name;
          found=true;
          ROS_DEBUG("AffordanceTemplate::setCurrentTrajectory() -- setting current trajectory to: %s", current_trajectory_.c_str());
          break;
        } else {
          ROS_ERROR("[AffordanceTemplate::setCurrentTrajectory] -- \'%s\' not a valid trajectory", t.name.c_str());
        }
      }
    }
  } else {
    ROS_DEBUG("AffordanceTemplate::setCurrentTrajectory() -- input trajectory is empty");
  }
  // get the first valid trajectory
  if (!found) {
    for (auto &t: traj_list) {
      if(isValidTrajectory(t)) {
        current_trajectory_ = t.name;
        found = true;
        if(traj!=current_trajectory_) {
          ROS_DEBUG("AffordanceTemplate::setCurrentTrajectory() -- \'%s\' not valid, setting current trajectory to: %s", traj.c_str(), current_trajectory_.c_str());
        } else {
          ROS_DEBUG("AffordanceTemplate::setCurrentTrajectory() -- setting current trajectory to: %s", current_trajectory_.c_str());          
        }
        break;
      } 
    }
  } 
  if (!found) // still no valid traj found
    {
      ROS_ERROR("AffordanceTemplate::createDisplayObjects() -- no valid trajectory found");
      return false;
    }
  return true;
}

bool AffordanceTemplate::setTemplatePose(geometry_msgs::PoseStamped ps)
{
  key_ = structure_.name;
  ROS_WARN("AT key %s", key_.c_str());
  mutex_.lock();
  frame_store_[key_] = FrameInfo(key_, ps);
  mutex_.unlock();
  return true;
}
    
bool AffordanceTemplate::setWaypointViewMode(int ee, int wp, bool m)
{
  std::string current_trajectory = current_trajectory_;

  std::string wp_name = createWaypointID(ee, wp);
  waypoint_flags_[current_trajectory].compact_view[wp_name] = m;
  ROS_DEBUG("AffordanceTemplate::setWaypointViewMode() -- setting compact_view for [%s] to %d", wp_name.c_str(), (int)m);
  if(!removeMarkerAndRebuild(wp_name)) {
    ROS_ERROR("AffordanceTemplate::setWaypointViewMode() -- error building template with wp [%s] view mode %d", wp_name.c_str(), (int)m);
    return false;
  }
  return true;
}


bool AffordanceTemplate::configureWaypoint(const affordance_template_msgs::WaypointConfig c)
{

  ROS_INFO("AffordanceTemplate::configureWaypoint() -- %s", c.name.c_str());
  std::string wp_name = c.name;

  // get traj we are operating on
  std::string traj_name;
  if(!c.trajectory_name.empty()) {
    traj_name = c.trajectory_name;
  } else {
    traj_name = current_trajectory_;
  }

  // make sure traj exists 
  if (waypoint_flags_.find(traj_name) == std::end(waypoint_flags_)) {
    ROS_ERROR("AffordanceTemplate::configureWaypoint() -- can't traj=%s", traj_name.c_str());
    return false;
  }

  // set controls on
  if (waypoint_flags_[traj_name].controls_on.find(wp_name) == std::end(waypoint_flags_[traj_name].controls_on)) {
    ROS_ERROR("AffordanceTemplate::configureWaypoint() -- can't find waypoint=%s to set controls", wp_name.c_str());
    return false;
  }
  waypoint_flags_[traj_name].controls_on[wp_name] = c.controls_on;

  // set compact view on
  if (waypoint_flags_[traj_name].compact_view.find(wp_name) == std::end(waypoint_flags_[traj_name].compact_view)) {
    ROS_ERROR("AffordanceTemplate::configureWaypoint() -- can't find waypoint=%s to set compact_view", wp_name.c_str());
    return false;
  }
  waypoint_flags_[traj_name].compact_view[wp_name] = c.compact_view;

  bool found = false;
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == traj_name) {
      ROS_INFO("AffordanceTemplate::configureWaypoint() -- found traj %s", traj_name.c_str());
      // look for the object the user selected in our waypoint list
      for (auto& wp_list: traj.ee_waypoint_list) {
        int wp_id = -1; // init to -1 because we pre-add
        for (auto& wp: wp_list.waypoints) {
          std::string candidate_wp = createWaypointID(wp_list.id, ++wp_id);
          ROS_INFO("AffordanceTemplate::configureWaypoint() -- candidate_wp: %s, wp: %s", candidate_wp.c_str(), wp_name.c_str());
          if (wp_name == candidate_wp) {

            ROS_INFO("AffordanceTemplate::configureWaypoint() -- found it: %s", wp_name.c_str());
         
            // set pose
            ROS_INFO("AffordanceTemplate::configureWaypoint() -- setting pose");

            std::vector<std::string> obj_keys;
            std::string obj_name;
            boost::split(obj_keys, c.waypoint_frame, boost::is_any_of(":"));
            if (obj_keys.size() == 2) {
              obj_name = obj_keys[0];
            } else { 
              ROS_ERROR("AffordanceTemplate::configureWaypoint() -- trouble splitting parent object frame=%s, size:%d", c.waypoint_frame.c_str(), (int)obj_keys.size());
              return false;
            }
            ROS_INFO("AffordanceTemplate::configureWaypoint() -- setting pose parent obj: %s", obj_name.c_str());
            wp.origin =  Vector3MsgToOrigin(c.waypoint_position, c.waypoint_orientation);
            wp.display_object = obj_name;

            // set planner type
            ROS_INFO("AffordanceTemplate::configureWaypoint() -- setting planner");
            if(!c.planner_type.empty()) {
              wp.planner_type = c.planner_type;
            }

            // set conditioning metric
            ROS_INFO("AffordanceTemplate::configureWaypoint() -- setting conditioning metric");
            if(!c.conditioning_metric.empty()) {
              wp.conditioning_metric = c.conditioning_metric;
            }

            // set EE pose ID
            wp.ee_pose = c.end_effector_pose_id;
            
            // set tolerances
            ROS_INFO("AffordanceTemplate::configureWaypoint() -- setting tolerances");

            tolerance_util::ToleranceVal p_tol, o_tol;
            robot_interface_->getPlanner()->toleranceUtil->getToleranceVal("position", c.position_tolerance, p_tol);
            for(int i=0; i < 3; i++){
              wp.bounds.position[i][0] = p_tol[i];
              wp.bounds.position[i][1] = -p_tol[i];
            }
            robot_interface_->getPlanner()->toleranceUtil->getToleranceVal("orientation", c.orientation_tolerance, o_tol);
            for(int i=0; i < 3; i++){
              wp.bounds.orientation[i][0] = o_tol[i];
              wp.bounds.orientation[i][1] = -o_tol[i];
            }
            found = true;
            break;
          }
        }
      }
    }
  }

  if(!found) {
    ROS_ERROR("AffordanceTemplate::setWaypointViewMode() -- couldn't find %s in structure", wp_name.c_str());
    return false;
  }

  ROS_INFO("AffordanceTemplate::configureWaypoint() -- rebuilding...");
  if(!removeMarkerAndRebuild(wp_name)) {
    ROS_ERROR("AffordanceTemplate::setWaypointViewMode() -- error building template after setting wp=%s", wp_name.c_str());
    return false;
  }

  return true;
}


bool AffordanceTemplate::buildTemplate(std::string traj) 
{
  ROS_DEBUG("AffordanceTemplate::buildTemplate() -- %s", template_type_.c_str());
  std::string current_trajectory = current_trajectory_;

  build_request=true;

  // set trajectory to current if empty
  if(traj.empty())
    traj = current_trajectory;

  // set the trajectory, and build the AT interactive markers and data structures
  if(setCurrentTrajectory(structure_.ee_trajectories, traj)) {
    
    if(!createDisplayObjects()) {
      ROS_ERROR("AffordanceTemplate::buildTemplate() -- couldn't createDisplayObjects()");
      build_request=false;
      return false;
    }
    
    if(!createWaypoints()) { 
      ROS_ERROR("AffordanceTemplate::buildTemplate() -- couldn't createWaypoints()"); 
      build_request=false;
      return false;
    }

  } else {
    ROS_ERROR("AffordanceTemplate::buildTemplate() -- couldn't set the current trajectory");
    build_request=false;
    return false;
  }
  ROS_DEBUG("AffordanceTemplate::buildTemplate() -- done creating %s", template_type_.c_str());    
  
  ros::Time timer_=ros::Time::now();
  updated_frames = true;
  
  FrameInfo fi;
  tf::StampedTransform stamped;

  bool all_updated;
  
  std::map<std::string,bool> completed;
  std::string key;

  do {
    all_updated = true;
    for(auto f: frame_store_)  {
      mutex_.lock();
      key = f.first;
      fi = f.second;
      mutex_.unlock();

      if (!isWaypoint(key)) 
        continue;      

      if (completed.find(key)!=completed.end() && completed[key])
        continue;

      try {
        // all_updated = tf_listener_.waitForTransform(fi.second.header.frame_id, fi.first, ros::Time(0), ros::Duration(0.1));
        // if (!all_updated)
        //   break;
        tf_listener_.lookupTransform(fi.second.header.frame_id, fi.first, ros::Time(0), stamped);
        if (stamped.stamp_ < timer_) {
          all_updated = false;
          break;
        }
        completed[key]=true;
      }
      catch(tf::TransformException ex) {
        ROS_WARN_STREAM_THROTTLE(1.0,"No transform between "<<fi.second.header.frame_id<<" "<<fi.first);
        all_updated=false;
        break;
      }
    }

    if (all_updated)
      break;

    ros::Duration(0.01).sleep();

  } while (true);
  
  updated_frames = false;
  build_request = false;

  return true;
}

 
bool AffordanceTemplate::createDisplayObjects() {
  // get AT name (<at_name>:<at_instance>)
  key_ = structure_.name;
  root_frame_ = key_;
  ROS_DEBUG("AffordanceTemplate::createDisplayObjects() -- key: %s", key_.c_str());
  // set the root frame for the AT
  geometry_msgs::PoseStamped ps;
  ps.pose = robot_interface_->getRobotConfig().root_offset;
  ps.header.frame_id = robot_interface_->getRobotConfig().frame_id;
  
  setFrame(key_, ps);

  // go through and draw each display object and corresponding marker
  int idx=0;
  for(auto &obj: structure_.display_objects) {
    if(!createDisplayObject(obj, idx++)) {   
      ROS_ERROR("AffordanceTemplate::createDisplayObjects() -- error creating obj: %s", obj.name.c_str());
      return false;
    }
  }
  return true;
}

bool AffordanceTemplate::createDisplayObject(affordance_template_object::DisplayObject obj, int idx)
{
  ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- creating Display Object: %s", obj.name.c_str());
  ROS_DEBUG("AffordanceTemplate::createDisplayObject() --  parent: %s", obj.parent.c_str());

  // get the parent frame for the object.  If no parent in the structure, set it as AT root frame
  std::string obj_frame;
  if(obj.parent != "") {
    obj_frame = obj.parent;
  } else {
    obj_frame = root_frame_;
  }
  ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- root_frame: %s", obj_frame.c_str());
  
  // set scaling information (if not already set)
  if(object_scale_factor_.find(obj.name) == std::end(object_scale_factor_)) {
    object_scale_factor_[obj.name] = 1.0;
    ee_scale_factor_[obj.name] = 1.0;
    ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- setting scale factor for %s to default 1.0", obj.name.c_str());
  } 

  if(object_scale_factor_.find(obj.parent) == std::end(object_scale_factor_)) {
    object_scale_factor_[obj.parent] = 1.0;
    ee_scale_factor_[obj.parent] = 1.0;
    ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- setting parent scale factor for %s to default 1.0", obj.parent.c_str());
  }

  // get the object origin pose 
  geometry_msgs::PoseStamped display_pose;
  display_pose.header.frame_id = obj_frame;
  display_pose.pose = originToPoseMsg(obj.origin);

  ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- pose: (%.3f,%.3f,%.3f),(%.3f,%.3f,%.3f,%.3f)", 
            display_pose.pose.position.x,display_pose.pose.position.y,display_pose.pose.position.z,
            display_pose.pose.orientation.x,display_pose.pose.orientation.y,display_pose.pose.orientation.z,display_pose.pose.orientation.w);

  // scale the object location according to parent object scale
  if(object_scale_factor_[obj.parent] != 1.0) {
    display_pose.pose.position.x *= object_scale_factor_[obj.parent];
    display_pose.pose.position.y *= object_scale_factor_[obj.parent];
    display_pose.pose.position.z *= object_scale_factor_[obj.parent];
    ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- scaled pose: (%.3f,%.3f,%.3f),(%.3f,%.3f,%.3f,%.3f)", 
              display_pose.pose.position.x,display_pose.pose.position.y,display_pose.pose.position.z,
              display_pose.pose.orientation.x,display_pose.pose.orientation.y,display_pose.pose.orientation.z,display_pose.pose.orientation.w);
  }

  setFrame(obj.name, display_pose);

  // create Interactive Marker for the object
  visualization_msgs::InteractiveMarker int_marker;

  // check to see if the IM server already has a IM with this name and just update it if so 
  if(server_->get(obj.name, int_marker)) {
    ROS_DEBUG("AffordanceTemplate::createDisplayObject() -- updating old object IM %s", obj.name.c_str());
  } else {
    setupObjectMenu(obj);
  }

  // set basic info for IM
  int_marker.header.frame_id = obj_frame;
  int_marker.header.stamp = ros::Time(0);
  int_marker.name = obj.name;
  int_marker.description = obj.name;
  int_marker.scale = obj.controls.scale*object_scale_factor_[obj.name];

  geometry_msgs::PoseStamped obj_ps;
  getPoseFromFrameStore(obj.name, obj_ps);
  int_marker.pose = obj_ps.pose;

  // set up the object display and menus.  Will be a "clickable" hand in RViz.
  visualization_msgs::InteractiveMarkerControl control;
  control.always_visible = true;
  control.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;    
  control.markers.clear();

  // get the shape for the object  
  visualization_msgs::Marker marker;
  if(!createDisplayObjectMarker(obj, marker)){
    ROS_ERROR("AffordanceTemplate::createDisplayObject() -- error getting marker");
    return false;
  }
  marker.id = idx;
  control.markers.push_back(marker);

  //  # int_marker = CreateInteractiveMarker(self.frame_id, obj.name, scale)
  int_marker.controls.clear();
  int_marker.controls.push_back(control);
  if(object_controls_display_on_[obj.name]) {
    std::vector<visualization_msgs::InteractiveMarkerControl> dof_controls;
    dof_controls = rit_utils::MarkerHelper::makeCustomDOFControls(obj.controls.translation[0], obj.controls.translation[1], obj.controls.translation[2],
                                                                  obj.controls.rotation[0], obj.controls.rotation[1], obj.controls.rotation[2]);
    for (auto &c: dof_controls)
      int_marker.controls.push_back(c);
  }

  // setup object menu defualts
  setObjectMenuDefaults(obj.name);

  // add interative marker
  addInteractiveMarker(int_marker);

  return true;
}

bool AffordanceTemplate::createDisplayObjectMarker(affordance_template_object::DisplayObject obj, visualization_msgs::Marker &marker) 
{
  marker.text = obj.name;
  marker.ns = obj.name;
  ROS_DEBUG("AffordanceTemplate::createDisplayObjectMarker() -- obj=%s, scale=%.3f", obj.name.c_str(), object_scale_factor_[obj.name]);
  
  if(obj.shape.type == "mesh") {
    marker.type = visualization_msgs::Marker::MESH_RESOURCE;
    marker.mesh_resource = obj.shape.mesh;
    marker.scale.x = obj.shape.size[0]*object_scale_factor_[obj.name];
    marker.scale.y = obj.shape.size[1]*object_scale_factor_[obj.name];
    marker.scale.z = obj.shape.size[2]*object_scale_factor_[obj.name];
    ROS_DEBUG("AffordanceTemplate::createDisplayObjectMarker() -- drawing Mesh for object %s : %s (scale=%.3f)", obj.name.c_str(), marker.mesh_resource.c_str(), object_scale_factor_[obj.name]);
  } else if(obj.shape.type == "box") {
    marker.type = visualization_msgs::Marker::CUBE;
    marker.scale.x = obj.shape.size[0]*object_scale_factor_[obj.name];
    marker.scale.y = obj.shape.size[1]*object_scale_factor_[obj.name];
    marker.scale.z = obj.shape.size[2]*object_scale_factor_[obj.name];
  } else if(obj.shape.type == "sphere") {
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.scale.x = obj.shape.size[0]*object_scale_factor_[obj.name];
    marker.scale.y = obj.shape.size[1]*object_scale_factor_[obj.name];
    marker.scale.z = obj.shape.size[2]*object_scale_factor_[obj.name];
  } else if(obj.shape.type == "cylinder") {
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.scale.x = obj.shape.radius*object_scale_factor_[obj.name];
    marker.scale.y = obj.shape.length*object_scale_factor_[obj.name];      
  }

  if(obj.shape.type != "mesh") {
    marker.color.r = obj.shape.rgba[0];
    marker.color.g = obj.shape.rgba[1];
    marker.color.b = obj.shape.rgba[2];
    marker.color.a = obj.shape.rgba[3];
  } else {
    marker.mesh_use_embedded_materials = true;
    marker.color.r = 0.6;
    marker.color.g = 0.6;
    marker.color.b = 0.6;
    marker.color.a = 1.0;
  }

  return true;

}

bool AffordanceTemplate::createWaypoints() 
{
  std::string current_trajectory = current_trajectory_;

  ROS_DEBUG("AffordanceTemplate::createWaypoints() -- trajectory: %s", current_trajectory.c_str());

  // get trajectory info from structure
  Trajectory traj;
  if(!getTrajectory(structure_.ee_trajectories, current_trajectory, traj)){
    ROS_ERROR("AffordanceTemplate::createWaypoints() -- couldn't get the request trajectory");
    return false;
  }
  
  // get each the list of waypoints for each EE in the trajectory
  for(auto &wp_list: traj.ee_waypoint_list) {
    int ee_id = wp_list.id;
    ROS_DEBUG("AffordanceTemplate::createWaypoints() creating Trajectory for Waypoint[%d]", ee_id);
    setTrajectoryFlags(traj);  
  
    // go through the trajectory and set up each waypoint (and corresponding itneractive marker)
    int wp_id = 0;
    for(auto &wp: wp_list.waypoints) {
      if(!createWaypoint(wp, ee_id, wp_id)) {
        ROS_ERROR("AffordanceTemplate::createWaypoints() -- error creating Waypoint: %d.%d for trajectory: %s", ee_id, wp_id, current_trajectory.c_str());
        return false;
      }
      wp_id++;
    }
  }
  ROS_DEBUG("AffordanceTemplate::createWaypoints() -- done");
  return true;
}

bool AffordanceTemplate::createWaypoint(affordance_template_object::EndEffectorWaypoint wp, int ee_id, int wp_id) 
{
  std::string current_trajectory = current_trajectory_;

  // create wp_name of form <ee_id>.<wp_id>:<at_name>:<at_id>
  std::string wp_name = createWaypointID(ee_id, wp_id);
  ROS_DEBUG("AffordanceTemplate::createWaypoint() creating Waypoint: %s", wp_name.c_str());

  // get ee_name from ee_id
  std::map<int, std::string> ee_name_map = robot_interface_->getEENameMap();
  std::string ee_name = ee_name_map[ee_id];

  // get the parent object name and scale
  std::string parent_obj = appendID(wp.display_object);
  double parent_scale = object_scale_factor_[parent_obj]*ee_scale_factor_[parent_obj];  

  // get the WP origin pose 
  geometry_msgs::PoseStamped display_pose;
  display_pose.header.frame_id = parent_obj;
  display_pose.header.stamp = ros::Time(0); 
  display_pose.pose = originToPoseMsg(wp.origin);  

  ROS_DEBUG("AffordanceTemplate::createWaypoint() -- pose: (%.3f,%.3f,%.3f),(%.3f,%.3f,%.3f,%.3f)", 
            display_pose.pose.position.x,display_pose.pose.position.y,display_pose.pose.position.z,
            display_pose.pose.orientation.x,display_pose.pose.orientation.y,display_pose.pose.orientation.z,display_pose.pose.orientation.w);

  // scale the WP location according to parent object scale
  if(parent_scale != 1.0) {
    display_pose.pose.position.x *= parent_scale;
    display_pose.pose.position.y *= parent_scale;
    display_pose.pose.position.z *= parent_scale;
    ROS_DEBUG("AffordanceTemplate::createWaypoint() -- scaled pose: (%.3f,%.3f,%.3f),(%.3f,%.3f,%.3f,%.3f)", 
              display_pose.pose.position.x,display_pose.pose.position.y,display_pose.pose.position.z,
              display_pose.pose.orientation.x,display_pose.pose.orientation.y,display_pose.pose.orientation.z,display_pose.pose.orientation.w);
  }
  
  // set up robot-specific frames not stored in AT structure 

  // get EE link frame (planning frame of arm/manipulator)
  geometry_msgs::PoseStamped ee_pose;
  ee_pose.header.frame_id = wp_name;
  ee_pose.pose = robot_interface_->getManipulatorOffsetPose(ee_name);
  std::string ee_frame_name = getEEFrameName(wp_name);

  // get control point for EE (a robot specific offset from planning frame (i.e., the palm rather then the wrist link))
  geometry_msgs::PoseStamped control_pose;
  control_pose.header.frame_id = ee_frame_name;
  control_pose.pose = robot_interface_->getToolOffsetPose(ee_name);
  std::string cp_frame_name = getControlPointFrameName(wp_name);

  // set up FrameStore frames 
  setFrame(wp_name, display_pose);
  setFrame(ee_frame_name, ee_pose);
  setFrame(cp_frame_name, control_pose);

  // create Interactive Marker for waypoint  
  visualization_msgs::InteractiveMarker int_marker;

  // check to see if the IM server already has a IM with this name and just update it if so 
  if(server_->get(wp_name, int_marker)) {
    ROS_DEBUG("AffordanceTemplate::createWaypoint() -- updating old waypoint IM %s", wp_name.c_str());
  } else {
    // only setup menu the first time
    setupWaypointMenu(wp_name);
  }

  // set basic info for IM
  int_marker.header.frame_id = parent_obj;
  int_marker.header.stamp = ros::Time(0);
  int_marker.name = wp_name;
  int_marker.description = wp_name;
  int_marker.scale = wp.controls.scale;

  geometry_msgs::PoseStamped wp_ps;
  getPoseFromFrameStore(wp_name, wp_ps);
  int_marker.pose = wp_ps.pose;
  // int_marker.pose = frame_store_[wp_name].second.pose;

  // set up the EE display and menus.  Will be a "clickable" hand in RViz.
  visualization_msgs::InteractiveMarkerControl menu_control;
  menu_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;    
  menu_control.always_visible = true;
  menu_control.markers.clear();

  // get the markers for the EE and add them to the IM
  visualization_msgs::MarkerArray markers;
  if(!createWaypointMarkers(ee_id, wp_id, wp.ee_pose, markers)) {
    ROS_ERROR("AffordanceTemplate::createWaypoint() -- error creating markers for %s", wp_name.c_str());
    return false;
  }
  for(auto &m : markers.markers)
    menu_control.markers.push_back( m );

  // set up WP controls
  int_marker.controls.clear();
  int_marker.controls.push_back(menu_control);
  if(waypoint_flags_[current_trajectory].controls_on[wp_name]) {
    std::vector<visualization_msgs::InteractiveMarkerControl> dof_controls;
    dof_controls = rit_utils::MarkerHelper::makeCustomDOFControls(wp.controls.translation[0], wp.controls.translation[1], wp.controls.translation[2],
                                                                  wp.controls.rotation[0], wp.controls.rotation[1], wp.controls.rotation[2]);
    for (auto &c: dof_controls) {
      int_marker.controls.push_back(c);
    }
  }

  // set default menu checkbox states
  setWaypointMenuDefaults(wp_name);

  // finally, add the IM and correspodning menu to the server
  addInteractiveMarker(int_marker);

  // setup the toolpoint and cooresponding IM
  if(!createToolpoint(wp, ee_id, wp_id))
    ROS_WARN("AffordanceTemplate::createWaypoint() -- problem setting up ToolPoint for %s", wp_name.c_str());

  return true;
}


bool AffordanceTemplate::createToolpoint(affordance_template_object::EndEffectorWaypoint wp, int ee_id, int wp_id) 
{
  std::string current_trajectory = current_trajectory_;
 
  // create wp_name of form <ee_id>.<wp_id>:<at_name>:<at_id>
  std::string wp_name = createWaypointID(ee_id, wp_id);
  ROS_DEBUG("AffordanceTemplate::createToolpoint() creating Toolpoint for: %s", wp_name.c_str());

  // get the parent object name and scale
  std::string parent_obj = appendID(wp.display_object);
  double parent_scale = object_scale_factor_[parent_obj]*ee_scale_factor_[parent_obj];  

  // get waypoint (tool) offset point and setup the corresponding frame (<wp_name>/tp)
  geometry_msgs::PoseStamped tool_pose;
  tool_pose.header.frame_id = wp_name;
  tool_pose.header.stamp = ros::Time(0);
  std::string tp_frame_name = getToolPointFrameName(wp_name);
  tool_pose.pose = originToPoseMsg(wp.tool_offset); 

  // scale the TP location according to parent object scale
  if(parent_scale != 1.0) {
    tool_pose.pose.position.x *= parent_scale;
    tool_pose.pose.position.y *= parent_scale;
    tool_pose.pose.position.z *= parent_scale;
    ROS_DEBUG("AffordanceTemplate::createToolpoint() -- scaled tool pose: (%.3f,%.3f,%.3f),(%.3f,%.3f,%.3f,%.3f)", 
              tool_pose.pose.position.x,tool_pose.pose.position.y,tool_pose.pose.position.z,
              tool_pose.pose.orientation.x,tool_pose.pose.orientation.y,tool_pose.pose.orientation.z,tool_pose.pose.orientation.w);
  }

  // add tp frame to FrameStore
  setFrame(tp_frame_name, tool_pose);

  if(waypoint_flags_[current_trajectory].adjust_offset[wp_name] || waypoint_flags_[current_trajectory].move_offset[wp_name]) {

    // set up the EE display and menus.  Will be a "clickable" hand in RViz.
    visualization_msgs::InteractiveMarkerControl menu_control;
    menu_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;    
    menu_control.always_visible = true;
    menu_control.markers.clear();

    visualization_msgs::InteractiveMarker tool_offset_marker;
    tool_offset_marker.header.frame_id = tp_frame_name;
    tool_offset_marker.header.stamp = ros::Time(0);
    tool_offset_marker.name = tp_frame_name;
    tool_offset_marker.description = tp_frame_name;      
    tool_offset_marker.scale = 0.1;
    tool_offset_marker.controls.push_back(menu_control);

    std::vector<visualization_msgs::InteractiveMarkerControl> dof_controls;
    dof_controls = rit_utils::MarkerHelper::makeCustomDOFControls(true,true,true,true,true,true);
    for (auto &c: dof_controls) {
      tool_offset_marker.controls.push_back(c);
    }

    // check to see if the IM server already has a IM with this name and just update it if so 
    if(server_->get(tp_frame_name, tool_offset_marker)) {
      ROS_DEBUG("AffordanceTemplate::createToolpoint() -- updating old toolpoint IM %s", tp_frame_name.c_str());
    } else {
      // only setup menu the first time
      setupToolpointMenu(tp_frame_name);
    }

    // set default menu state
    setToolPointMenuDefaults(tp_frame_name);

    // finally add interactive marker
    addInteractiveMarker(tool_offset_marker);
  }

  return true;
}

bool AffordanceTemplate::createWaypointMarkers(int ee_id, int wp_id, int ee_pose, visualization_msgs::MarkerArray &markers)
{
  // return marker array
  markers.markers.clear();

  // create wp_name of form <ee_id>.<wp_id>:<at_name>:<at_id>
  std::string wp_name = createWaypointID(ee_id, wp_id);
  ROS_DEBUG("AffordanceTemplate::createWaypointMarkers() creating MarkersArray for waypoint: %s", wp_name.c_str());

  // get ee_name from ee_id
  std::map<int, std::string> ee_name_map = robot_interface_->getEENameMap();
  std::string ee_name = ee_name_map[ee_id];

  // get the EE pose configuration (hand closed, hand opened, etc.)
  std::string ee_pose_name;
  try {
    ee_pose_name = robot_interface_->getEEPoseNameMap(ee_name)[ee_pose];
  } catch(...) {
    ee_pose_name = "current";
  }
  ROS_DEBUG("AffordanceTemplate::createWaypointMarkers() -- ee_pose_name[%d]: %s", ee_pose, ee_pose_name.c_str());
     
  // use the EE helper to get the Markers for the EE at the specified ee_pose
  rit_utils::MarkerArray ee_markers;
  
  if(!robot_interface_->getPlanner()->getRDFModel()->getMarkersForPose(ee_name, ee_pose_name, ee_markers)) {
    ROS_ERROR("AffordanceTemplate::createWaypointMarkers() -- problem getting pose markers for EE %s, pose: %s", ee_name.c_str(), ee_pose_name.c_str());
    return false;
  } 
  
  // set the EE pose markers
  m_plan_.lock();
  std::string current_trajectory=current_trajectory_;
  double N = (double)getNumWaypoints(current_trajectory, ee_id);
  double current_idx = (double)plan_status_[current_trajectory][ee_name].current_idx;
  m_plan_.unlock();
  double range = 0.6;
  double inc = range/N;
  double sl = N-current_idx;
  double compact_view_scale = 0.02;
  double adjusted_color = (0.9-range)+inc*(N-1.0-wp_id);

  // set WP color according its place in the trajectory 
  std_msgs::ColorRGBA ee_color;
  ee_color = getColorMsg(0.0,adjusted_color,adjusted_color,adjusted_color);

  // "compact view" will just draw a small sphere instead of the full EE
  if(waypoint_flags_[current_trajectory].compact_view[wp_name]) {
    ROS_DEBUG("AffordanceTemplate::createWaypointMarkers() -- displaying %s in COMPACT mode", wp_name.c_str());
    visualization_msgs::Marker m;
    m.header.frame_id = wp_name;
    m.ns = name_;
    m.type = visualization_msgs::Marker::SPHERE;
    m.scale.x = compact_view_scale;
    m.scale.y = compact_view_scale;
    m.scale.z = compact_view_scale;
    m.color = ee_color;
    m.pose.orientation.w = 1;
    m.frame_locked = true;
    markers.markers.push_back( m );
  } else {
    ROS_DEBUG("AffordanceTemplate::createWaypointMarkers() -- displaying %s in FULL mode", wp_name.c_str());
    for(auto &m: ee_markers) {
      visualization_msgs::Marker ee_m = m.first;
      ee_m.header.frame_id = getControlPointFrameName(wp_name);
      ee_m.ns = name_;
      ee_m.pose = m.first.pose;
      ee_m.frame_locked = true;
      ee_m.color = ee_color;
      markers.markers.push_back( ee_m );
    }
  }

  return true;
}

bool AffordanceTemplate::insertWaypointInTrajectory(affordance_template_object::EndEffectorWaypoint wp, int ee_id, int wp_id, std::string traj_name)
{
  std::string current_trajectory = current_trajectory_;

  // set traj to current if not specified
  if(traj_name.empty()) {
    traj_name = current_trajectory;
  }

  // go through and get the right waypoint list in the traj for the specified EE 
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == traj_name) {
      for(auto &ee_list : traj.ee_waypoint_list) {
        if(ee_list.id == ee_id) {
          insertWaypointInList(wp, wp_id, ee_list);
          return true;
        }
      }
      // add a new waypoint list for the ee_id, cause it wasnt found in the current set
      ROS_DEBUG("AffordanceTemplate::insertWaypointInTrajectory() -- creating new EE list for %d", ee_id); 
      affordance_template_object::EndEffectorWaypointList new_ee_list;
      new_ee_list.id = ee_id;
      new_ee_list.waypoints.push_back(wp);
      traj.ee_waypoint_list.push_back(new_ee_list);
      return true;
    }
  }

  ROS_ERROR("AffordanceTemplate::insertWaypointInTrajectory() -- couldn't set wp at %d for ee[%d] in traj[%s]", wp_id, ee_id, traj_name.c_str());
  return false;

}


bool AffordanceTemplate::insertWaypointInList(affordance_template_object::EndEffectorWaypoint wp, int id, affordance_template_object::EndEffectorWaypointList &wp_list) {
  
  ROS_DEBUG("AffordanceTemplate::insertWaypointInList() -- inserting new waypoint at position %d", id);
  
  std::string current_trajectory = current_trajectory_;

  // insert new waypoint into the list
  wp_list.waypoints.emplace(wp_list.waypoints.begin()+id, wp);

  // need to go and move all waypoints after insertion point forward.  
  // will do this from the end to preserve flags
  // wp_name_1 will move into slot wp_name_2.
  std::string wp_name_1, wp_name_2; 
  for(size_t k=wp_list.waypoints.size()-1; (int)k>(int)id; --k) {

    wp_name_1 = createWaypointID(wp_list.id, k-1);
    wp_name_2 = createWaypointID(wp_list.id, k);
    ROS_DEBUG("AffordanceTemplate::insertWaypointInList() -- ID=%d, k=%d -- moving \'%s\' data to \'%s\'", id, (int)k, wp_name_1.c_str(), wp_name_2.c_str());

    // copy waypoint flags
    waypoint_flags_[current_trajectory].controls_on[wp_name_2]   = waypoint_flags_[current_trajectory].controls_on[wp_name_1];
    waypoint_flags_[current_trajectory].compact_view[wp_name_2]  = waypoint_flags_[current_trajectory].compact_view[wp_name_1];
    waypoint_flags_[current_trajectory].adjust_offset[wp_name_2] = waypoint_flags_[current_trajectory].adjust_offset[wp_name_1];
    waypoint_flags_[current_trajectory].move_offset[wp_name_2]   = waypoint_flags_[current_trajectory].move_offset[wp_name_1];

    // remove the waypoint
    removeWaypoint(wp_name_1);
    
  }

  // set default flags for new waypoint
  std::string wp_name = createWaypointID(wp_list.id, id);
  ROS_DEBUG("AffordanceTemplate::insertWaypointInList() -- setting waypoint flags for %s",  wp_name.c_str());
  waypoint_flags_[current_trajectory].controls_on[wp_name] = true;
  waypoint_flags_[current_trajectory].compact_view[wp_name] = true;
  waypoint_flags_[current_trajectory].adjust_offset[wp_name] = false;
  waypoint_flags_[current_trajectory].move_offset[wp_name] = false;           

  return true;
}

bool AffordanceTemplate::deleteWaypointFromTrajectory(int ee_id, int wp_id, std::string traj_name)
{

  std::string current_trajectory = current_trajectory_;

  // set traj to current if not specified
  if(traj_name.empty()) {
    traj_name = current_trajectory;
  }

  // go through and get the right waypoint list in the traj for the specified EE 
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == traj_name) {
      for(auto &ee_list : traj.ee_waypoint_list) {
        if(ee_list.id == ee_id) {
          deleteWaypointFromList(ee_id, wp_id, ee_list);
          return true;
        }
      }
    }
  }

  ROS_ERROR("AffordanceTemplate::deleteWaypointFromTrajectory() -- couldn't delete wp at %d for ee[%d] in traj[%s]", wp_id, ee_id, traj_name.c_str());
  return false;

}


bool AffordanceTemplate::deleteWaypointFromList(int ee_id, int wp_id, affordance_template_object::EndEffectorWaypointList &wp_list) {

  std::string current_trajectory = current_trajectory_;

  std::string wp_name = createWaypointID(ee_id, wp_id);
  ROS_DEBUG("AffordanceTemplate::deleteWaypointFromList() -- deleting wp %s (%d)",  wp_name.c_str(), wp_id);
 
  // erase the waypoint from the list
  wp_list.waypoints.erase(wp_list.waypoints.begin() + wp_id);
  
  // need to go and move all waypoints after deletion point backwards.  
  // will do this from the end to preserve flags
  // wp_name_2 will move into slot wp_name_1.
  std::string wp_name_1, wp_name_2;
  for(size_t k=wp_list.waypoints.size(); (int)k>=(int)wp_id; --k) {

    wp_name_1 = createWaypointID(wp_list.id, k);
    wp_name_2 = createWaypointID(wp_list.id, k+1);
    ROS_DEBUG("AffordanceTemplate::deleteWaypointFromList() -- ID=%d, k=%d -- moving \'%s\' data to \'%s\'", wp_id, (int)k, wp_name_2.c_str(), wp_name_1.c_str());

    waypoint_flags_[current_trajectory].controls_on[wp_name_1] = waypoint_flags_[current_trajectory].controls_on[wp_name_2];
    waypoint_flags_[current_trajectory].compact_view[wp_name_1] = waypoint_flags_[current_trajectory].compact_view[wp_name_2];
    waypoint_flags_[current_trajectory].adjust_offset[wp_name_1] = waypoint_flags_[current_trajectory].adjust_offset[wp_name_2];
    waypoint_flags_[current_trajectory].move_offset[wp_name_1] = waypoint_flags_[current_trajectory].move_offset[wp_name_2];

    // remove the waypoints
    removeWaypoint(wp_name_1);
    removeWaypoint(wp_name_2);
 
  }

}

bool AffordanceTemplate::getWaypoint(std::string trajectory, int ee_id, int wp_id, affordance_template_object::EndEffectorWaypoint &wp)
{
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == trajectory) {
      for(auto &ee_list : traj.ee_waypoint_list) {
        // ROS_WARN_STREAM("looking at ID: "<<ee_list.id);
        if(ee_list.id == ee_id) {
          if(wp_id < ee_list.waypoints.size()) {
            wp = ee_list.waypoints[wp_id];
            return true;
          } else {
            ROS_WARN("AffordanceTemplate::getWaypoint() -- no wp[%d] found for ee[%d] in traj[%s]", wp_id, ee_id, trajectory.c_str());
            return false;
          }  
        }
      }
    }
  }
  ROS_WARN("AffordanceTemplate::getWaypoint() -- no traj[%s] found with ee[%d]", trajectory.c_str(), ee_id);
  return false;
}

bool AffordanceTemplate::setWaypoint(const std::string trajectory, const int ee_id, const int wp_id, const affordance_template_object::EndEffectorWaypoint& wp)
{
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == trajectory) {
      for(auto& ee_list : traj.ee_waypoint_list) {
        if(ee_list.id == ee_id) {
          if(wp_id < ee_list.waypoints.size()) {
            ee_list.waypoints[wp_id] = wp;

            // std::string wp_name = createWaypointID(ee_id, wp_id);
            // geometry_msgs::PoseStamped ps;
            // ps.pose = originToPoseMsg(wp.origin);
            // ps.header = frame_store_[wp_name].second.header;
            // setFrame( wp_name, ps);
            return true;
          } else {
            ROS_WARN("[AffordanceTemplate::setWaypoint] no wp[%d] found for ee[%d] in traj[%s]", wp_id, ee_id, trajectory.c_str());
            return false;
          }  
        }
      }
    }
  }
  ROS_WARN("[AffordanceTemplate::setWaypoint] no traj[%s] found with ee[%d]", trajectory.c_str(), ee_id);
  return false;
}

void AffordanceTemplate::setupObjectMenu(DisplayObject obj)
{
  for(auto& o : object_menu_options_) {
    if(o.first == "Choose Trajectory") 
      setupTrajectoryMenu(obj.name);
    else if (o.first.find("Add Waypoint") != std::string::npos)
      setupAddWaypointMenuItem(obj.name, o.first);
    else 
      setupSimpleMenuItem(obj.name, o.first, o.second);
  }
}

void AffordanceTemplate::setupWaypointMenu(std::string name)
{
  for(auto& o : waypoint_menu_options_) {
    if(o.first == "Change End-Effector Pose")
      setupEndEffectorPoseMenu(name);
    else if (o.first == "Use Cartesian Plans")
      setupCartesianPlans(name, o.first);
    else if (o.first == "Conditioning Metric")
      setupConditioningMetricMenu(name);
    else if (o.first == "Position Tolerance")
      setupPositionToleranceMenu(name);
    else if(o.first == "Orientation Tolerance")
      setupOrientationToleranceMenu(name);
    else
      setupSimpleMenuItem(name, o.first, o.second);
  }
}

void AffordanceTemplate::setupToolpointMenu(std::string name)
{
  for(auto& o : toolpoint_menu_options_) {
    setupSimpleMenuItem(name, o.first, o.second);
  }
}


void AffordanceTemplate::setupSimpleMenuItem(const std::string& name, const std::string& menu_text, bool has_check_box)
{
  MenuHandleKey key;
  key[name] = {menu_text};
  
  if (marker_menus_.find(name) == marker_menus_.end()) {
    ROS_WARN("[AffordanceTemplate::setupSimpleMenuItem] there is no %s in the marker menus map!", name.c_str());
    return;    
  }

  // @seth: 11/1/2016 - this can throw bad alloc sometimes; replacement lines found below
  // group_menu_handles_[key] = marker_menus_[name].insert( menu_text, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) );
  interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( menu_text, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) );
  if (group_menu_handles_.find(key) != group_menu_handles_.end())
    group_menu_handles_.emplace(key, menu);
  else
    group_menu_handles_[key] = menu;
  
  if(has_check_box)
    marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
    
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();
}

void AffordanceTemplate::setupAddWaypointMenuItem(std::string name, std::string menu_text)
{
  interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text);
  for(auto& ee: robot_interface_->getEENameMap()) {
    std::string ee_readable = robot_interface_->getReadableEEName(ee.second);
    MenuHandleKey key;
    key[name] = {menu_text, ee.second};
    interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, ee_readable, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) );
    if (group_menu_handles_.find(key) != group_menu_handles_.end())
      group_menu_handles_.emplace(key, menu);
    else
      group_menu_handles_[key] = menu;
    ROS_DEBUG("[AffordanceTemplate::setupAddWaypointMenuItem] adding submenu text %s to menu item %s", ee_readable.c_str(), menu_text.c_str());
  }
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();
}

void AffordanceTemplate::setupTrajectoryMenu(const std::string& name)
{
  std::string current_trajectory = current_trajectory_;

  std::string menu_text = "Choose Trajectory";
  interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text );
  for(auto &traj: structure_.ee_trajectories) {
    MenuHandleKey key;
    key[name] = {menu_text, traj.name};
    interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, traj.name, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) );   
    if (group_menu_handles_.find(key) != group_menu_handles_.end())
      group_menu_handles_.emplace(key, menu);
    else
      group_menu_handles_[key] = menu;
    if(traj.name == current_trajectory) 
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
    else
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );       
    ROS_DEBUG("[AffordanceTemplate::setupTrajectoryMenu] adding submenu text %s to menu item %s", traj.name.c_str(), menu_text.c_str());
  }
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();

}

void AffordanceTemplate::setupEndEffectorPoseMenu(const std::string& name)
{
  std::string menu_text = "Change End-Effector Pose";
  int ee_id = getEEIDfromWaypointName(name);
  std::string ee_name = robot_interface_->getEEName(ee_id);
  interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text );
  for(auto &pose_name: robot_interface_->getEEPoseNames(ee_name)) {
    MenuHandleKey key;
    key[name] = {menu_text, pose_name};
    interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, pose_name, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) ); 
    if (group_menu_handles_.find(key) != group_menu_handles_.end())
      group_menu_handles_.emplace(key, menu);
    else
      group_menu_handles_[key] = menu;
  }
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();
}

void AffordanceTemplate::setupCartesianPlans(const std::string& name, const std::string& menu_text)
{
  std::string current_trajectory = current_trajectory_;

  MenuHandleKey key;
  key[name] = {menu_text};
  interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( menu_text, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ) );
  if (group_menu_handles_.find(key) != group_menu_handles_.end())
    group_menu_handles_.emplace(key, menu);
  else
    group_menu_handles_[key] = menu;

  // get default value from JSON file
  std::string wp_planner = "CARTESIAN";
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == current_trajectory) {
      for (auto& wp_list: traj.ee_waypoint_list) {
        int wp_id = -1; // init to -1 because we pre-add
        for (auto& wp: wp_list.waypoints) {
          std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
          if (wp_name == name) {
            ROS_DEBUG_STREAM("[AffordanceTemplate::setupCartesianPlans] -- WAYPOINT PLANNER TYPE: " << wp.planner_type.c_str());
            wp_planner = wp.planner_type;
            ROS_DEBUG("[AffordanceTemplate::setupCartesianPlans] -- setting default planner to %s", wp_planner.c_str());
            break;
          }
        }
      }
    }
  }
  if(wp_planner == "CARTESIAN")
    {
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
    }
  else
    {
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
    }

  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();

}

void AffordanceTemplate::setupConditioningMetricMenu(const std::string& name)
{
  std::string current_trajectory = current_trajectory_;

  std::string menu_text = "Conditioning Metric";
  interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text );
  std::vector<std::string> metrics;
  robot_interface_->getPlanner()->getConditioningMetrics(metrics);

  // get default value from JSON file
  std::string wp_metric = "MIN_DISTANCE";
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == current_trajectory) {
      for (auto& wp_list: traj.ee_waypoint_list) {
        int wp_id = -1; // init to -1 because we pre-add
        for (auto& wp: wp_list.waypoints) {
          std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
          if (wp_name == name) {
            wp_metric = wp.conditioning_metric;
            ROS_DEBUG("setting default metric to %s", wp_metric.c_str());
            break;
          }
        }
      }
    }
  }

  for(auto &metric: metrics) {
    MenuHandleKey key;
    key[name] = {menu_text, metric};
    
    interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, metric, boost::bind( &AffordanceTemplate::processConditioningMetricFeedback, this, _1 ) );
    if (group_menu_handles_.find(key) != group_menu_handles_.end())
      group_menu_handles_.emplace(key, menu);
    else
      group_menu_handles_[key] = menu;

    if (metric == wp_metric)
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
    else
      marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
  }

  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();

}


void AffordanceTemplate::setupPositionToleranceMenu(const std::string& name) {

  std::string current_trajectory = current_trajectory_;
  ROS_DEBUG("[AffordanceTemplate::setupPositionToleranceMenu] -- Setup the menu");
  std::vector<std::string> types;
  std::string mode = "position";
  if(robot_interface_->getPlanner()->toleranceUtil->getToleranceTypes(mode, types)) {
    std::string menu_text = "Position Tolerance";
    std::string def_tol;
    robot_interface_->getPlanner()->toleranceUtil->getDefaultToleranceType(mode, def_tol);
    interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text );

    for (auto& traj : structure_.ee_trajectories) {
      if (traj.name == current_trajectory) {
        for (auto& wp_list: traj.ee_waypoint_list) {
          int wp_id = -1; // init to -1 because we pre-add
          for (auto& wp: wp_list.waypoints) {
            std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
            if (wp_name == name) {
              tolerance_util::ToleranceVal p_tol = {wp.bounds.position[0][0], wp.bounds.position[1][0],wp.bounds.position[2][0]};
              robot_interface_->getPlanner()->toleranceUtil->getToleranceType(mode, p_tol, def_tol);
              ROS_DEBUG_STREAM("[AffordanceTemplate::setupPositionToleranceMenu] -- WP "<< name << " pos tolerances are " << def_tol);
              break;
            }
          }
        }
      }
    }
    for(auto &t: types) {
      MenuHandleKey key;
      key[name] = {menu_text, t};
      interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, t, boost::bind( &AffordanceTemplate::processPositionToleranceFeedback, this, _1 ) );
      if (group_menu_handles_.find(key) != group_menu_handles_.end())
        group_menu_handles_.emplace(key, menu);
      else
        group_menu_handles_[key] = menu;

      if(t==def_tol) {
        marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
      } else {
        marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
      }
    }
  }
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();
}

void AffordanceTemplate::setupOrientationToleranceMenu(const std::string& name) {

  std::string current_trajectory = current_trajectory_;
  ROS_DEBUG("[AffordanceTemplate::setupPositionToleranceMenu] -- Setup the menu");
  std::vector<std::string> types;
  std::string mode = "orientation";
  if(robot_interface_->getPlanner()->toleranceUtil->getToleranceTypes(mode, types)) {
    std::string menu_text = "Orientation Tolerance";
    std::string def_tol;
    robot_interface_->getPlanner()->toleranceUtil->getDefaultToleranceType(mode, def_tol);
    interactive_markers::MenuHandler::EntryHandle sub_menu_handle = marker_menus_[name].insert( menu_text );

    for (auto& traj : structure_.ee_trajectories) {
      if (traj.name == current_trajectory) {
        for (auto& wp_list: traj.ee_waypoint_list) {
          int wp_id = -1; // init to -1 because we pre-add
          for (auto& wp: wp_list.waypoints) {
            std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
            if (wp_name == name) {
              tolerance_util::ToleranceVal o_tol = {wp.bounds.orientation[0][0], wp.bounds.orientation[1][0],wp.bounds.orientation[2][0]};
              robot_interface_->getPlanner()->toleranceUtil->getToleranceType(mode, o_tol, def_tol);
              ROS_DEBUG_STREAM("[AffordanceTemplate::setupOrientationToleranceMenu] -- WP "<< name << " orientation tolerances are " << def_tol);
              break;
            }
          }
        }
      }
    }

    for(auto &t: types) {
      MenuHandleKey key;
      key[name] = {menu_text, t};
      interactive_markers::MenuHandler::EntryHandle menu = marker_menus_[name].insert( sub_menu_handle, t, boost::bind( &AffordanceTemplate::processOrientationToleranceFeedback, this, _1 ) );
      if (group_menu_handles_.find(key) != group_menu_handles_.end())
        group_menu_handles_.emplace(key, menu);
      else
        group_menu_handles_[key] = menu;

      if(t==def_tol) {
        marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
      } else {
        marker_menus_[name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
      }
    }
  }
  marker_menus_[name].apply( *server_, name );
  server_->applyChanges();
}




int AffordanceTemplate::getEEIDfromWaypointName(const std::string wp_name)
{
  std::string delimiter = ".";
  size_t pos = 0;
  std::string token;
  if ((pos = wp_name.find(delimiter)) != std::string::npos) {
    token = wp_name.substr(0, pos);
    return std::stoi(token);
  }
  ROS_ERROR("AffordanceTemplate::getEEIDfromWaypointName() -- could not find EE ID from %s", wp_name.c_str());
  return -1;
}

void AffordanceTemplate::addInteractiveMarker(visualization_msgs::InteractiveMarker m)
{
  ROS_DEBUG("AffordanceTemplate::addInteractiveMarker() -- %s with frame: %s", m.name.c_str(), m.header.frame_id.c_str());
  std::string name = m.name;

  visualization_msgs::InteractiveMarker im;
  if(server_->get(name, im)) {
    im = m;
  } else {
    server_->insert(m);
    server_->setCallback(m.name, boost::bind( &AffordanceTemplate::processFeedback, this, _1 ));
    server_->applyChanges();
  }
  int_markers_[m.name] = m;

  // add the menu too
  marker_menus_[m.name].apply( *server_, m.name );
  server_->applyChanges();
}

void AffordanceTemplate::removeWaypoint(std::string wp_name)
{
  if(!isWaypoint(wp_name)) {
    ROS_WARN("AffordanceTemplate::removeWaypoint() -- %s is not a waypoint name", wp_name.c_str());
    return;
  }
  ROS_DEBUG("AffordanceTemplate::removeWaypoint() -- %s", wp_name.c_str());
 
  // remove interactive markers
  removeInteractiveMarker(wp_name);
  removeInteractiveMarker(getToolPointFrameName(wp_name));

  // remove frames
  mutex_.lock();
  frame_store_.erase(wp_name);
  frame_store_.erase(getControlPointFrameName(wp_name));
  frame_store_.erase(getEEFrameName(wp_name));
  frame_store_.erase(getToolPointFrameName(wp_name));
  mutex_.unlock();
}

void AffordanceTemplate::removeAllWaypoints()
{
  for(auto &m: int_markers_)
    if(isWaypoint(m.first)) 
      removeWaypoint(m.first);
}

void AffordanceTemplate::removeInteractiveMarker(std::string marker_name) 
{
  ROS_DEBUG("[AffordanceTemplate::removeInteractiveMarker] removing marker %s", marker_name.c_str());
  server_->erase(marker_name);
  server_->applyChanges();
  marker_menus_.erase(marker_name);
}

void AffordanceTemplate::removeAllMarkers() 
{
  for(auto &m: int_markers_)
    removeInteractiveMarker(m.first);
  group_menu_handles_.clear();
  int_markers_.clear();
  marker_menus_.clear();
}

bool AffordanceTemplate::removeMarkerAndRebuild(std::string marker_name)
{
  build_request=true;
  removeInteractiveMarker(marker_name);
  if(!buildTemplate()) {
    ROS_ERROR("AffordanceTemplate::removeMarkerAndRebuild() -- error rebuilding template after removing marker %s", marker_name.c_str());
    return false;
  }
  return true;  
}

bool AffordanceTemplate::updatePoseFrames(std::string name, geometry_msgs::PoseStamped ps) 
{

  ROS_INFO("AffordanceTemplate::updatePoseFrames() -- moving frame: %s", name.c_str());
  // Most frames will just require updating the FrameStore with the given pose (in the right frame).
  // If it is a toolPoint frame and we are moving it (not adjustring it), however, we need to update 
  // the correspodning waypoint frame, and then reset the toolPoint frame back to 0

  // sanity check that we are trying to adjust a frame that has IM controls
  if(!hasControls(name))
    return false;

  std::string current_trajectory = current_trajectory_;

  // get the cooresponding waypoint name if its a toolPoint frame
  std::string wp_name;
  if(isToolPointFrame(name)) {
    wp_name = getWaypointFrame(name);
  } 

  if(isToolPointFrame(name) && waypoint_flags_[current_trajectory].move_offset[wp_name]) {
      
    ROS_INFO("AffordanceTemplate::updatePoseFrames() -- moving tool frame: %s", name.c_str());
    std::string obj_frame = frame_store_[wp_name].second.header.frame_id;       
    geometry_msgs::PoseStamped tool_pose = ps;
    geometry_msgs::Pose wp_pose_new;
    tf::Transform origTtp_new, origTwp_new, wpTtp;
    geometry_msgs::PoseStamped wp_pose, tp_in_obj_frame;
    
    try {
      
      // get the toolpoint in the waypoint's base (object) frame
      tf_listener_.transformPose (obj_frame, tool_pose, tp_in_obj_frame);
      
      // calculate new waypoint location, keeping the toolPoint (wpTtp) fixed
      tf::poseMsgToTF(frame_store_[name].second.pose,wpTtp);
      tf::poseMsgToTF(tp_in_obj_frame.pose,origTtp_new);
      origTwp_new = origTtp_new*wpTtp.inverse();
      tf::poseTFToMsg(origTwp_new, wp_pose_new); 
      geometry_msgs::PoseStamped ps_new;
      ps_new.pose = wp_pose_new;
      ps_new.header.frame_id = frame_store_[wp_name].second.header.frame_id;
      setFrame(wp_name,ps_new);
      // frame_store_[wp_name].second.pose = wp_pose_new;
      
      // reset the toolPoint IM back to 0, now that the frames have been updated
      geometry_msgs::PoseStamped fresh_pose;
      fresh_pose.pose.orientation.w = 1.0;
      server_->setPose(name, fresh_pose.pose);
      server_->applyChanges();

    } catch(...) {
      ROS_WARN("AffordanceTemplate::updatePoseFrames(%s) -- %s transform error while moving tool frame", robot_name_.c_str(), name.c_str());
    }

  } else {

    // this is most cases, just update the frame store with the input pose
    if(ps.header.frame_id != frame_store_[name].second.header.frame_id) {
      try {
        tf_listener_.transformPose (frame_store_[name].second.header.frame_id, ps, ps);
      } catch (...) {
        ROS_WARN("AffordanceTemplate::updatePoseFrames() -- %s transform error while adjustring feedback frame", name.c_str());
      }
    }
    ROS_INFO("AffordanceTemplate::updatePoseFrames() -- storing pose for %s", name.c_str());
    setFrame(name,ps);

  }

  return true;
}

bool AffordanceTemplate::updatePoseInStructure(std::string name, geometry_msgs::Pose p) 
{
  std::string current_trajectory = current_trajectory_;

  // check the objects for the name
  for (auto& d : structure_.display_objects) {
    if (name == d.name) {
      ROS_DEBUG("AffordanceTemplate::updatePoseInStructure() -- saving pose for object %s", name.c_str());
      d.origin = poseMsgToOrigin(p);
      return true;
    }
  }

  // check the waypoints
  for (auto& traj : structure_.ee_trajectories) {
    if (traj.name == current_trajectory) {
      // look for the object the user selected in our waypoint list
      for (auto& wp_list: traj.ee_waypoint_list) {
        int wp_id = -1; // init to -1 because we pre-add
        for (auto& wp: wp_list.waypoints) {
          std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
          if (wp_name == name) {
            ROS_DEBUG("AffordanceTemplate::updatePoseInStructure() -- saving pose for EE waypoint %s", name.c_str());
            wp.origin = poseMsgToOrigin(p);
            return true;
          } else if(isToolPointFrame(name)) {
            if (wp_name == getWaypointFrame(name)) {
              wp.origin = poseMsgToOrigin(frame_store_[wp_name].second.pose);
              wp.tool_offset = poseMsgToOrigin(frame_store_[name].second.pose);
              ROS_WARN("AffordanceTemplate::updatePoseInStructure() -- saving toolPoint and pose for EE waypoint %s", wp_name.c_str());
              return true;
            }
          }
        }
      }
    }
  }

  // didn't find anything that matches the input name
  return false;
}

bool AffordanceTemplate::swapPreviousHandler(const std::string wp_name)
{
  if(!isWaypoint(wp_name)) {
    ROS_ERROR("[AffordanceTemplate::swapPreviousHandler] %s not a waypoint, can't swap waypoint", wp_name.c_str());
    return false;
  }

  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);

  if (wp_id == 0) {
    ROS_DEBUG("[AffordanceTemplate::swapPreviousHandler] currently on first waypoint, cannot swap with previous!");
    return false;
  }

  return doSwap(ee_id, wp_id, wp_id-1);
}

bool AffordanceTemplate::swapNextHandler(const std::string wp_name)
{
  std::string current_trajectory = current_trajectory_;

  if(!isWaypoint(wp_name)) {
    ROS_ERROR("[AffordanceTemplate::swapNextHandler] %s not a waypoint, can't swap waypoint", wp_name.c_str());
    return false;
  }

  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);

  if (wp_id == (getNumWaypoints(current_trajectory, ee_id) - 1)) {
    ROS_DEBUG("[AffordanceTemplate::swapNextHandler] currently on last waypoint, cannot swap with next!");
    return false;
  }

  return doSwap(ee_id, wp_id, wp_id+1);
}

bool AffordanceTemplate::doSwap(int ee_id, int curr_id, int swap_id)
{
  ROS_INFO("[AffordanceTemplate::doSwap] swapping WP %d with %d", curr_id, swap_id);

  std::string curr_name = createWaypointID(ee_id, curr_id);
  std::string swap_name = createWaypointID(ee_id, swap_id);

  std::string current_trajectory = current_trajectory_;

  if (waypoint_flags_.find(current_trajectory) != std::end(waypoint_flags_)) {
    
    std::map<std::string, std::vector<bool> > flag_vals;

    flag_vals[curr_name].push_back(waypoint_flags_[current_trajectory].controls_on[curr_name]);
    flag_vals[curr_name].push_back(waypoint_flags_[current_trajectory].compact_view[curr_name]);
    flag_vals[curr_name].push_back(waypoint_flags_[current_trajectory].adjust_offset[curr_name]);
    flag_vals[curr_name].push_back(waypoint_flags_[current_trajectory].move_offset[curr_name]);

    flag_vals[swap_name].push_back(waypoint_flags_[current_trajectory].controls_on[swap_name]);
    flag_vals[swap_name].push_back(waypoint_flags_[current_trajectory].compact_view[swap_name]);
    flag_vals[swap_name].push_back(waypoint_flags_[current_trajectory].adjust_offset[swap_name]);
    flag_vals[swap_name].push_back(waypoint_flags_[current_trajectory].move_offset[swap_name]);

    waypoint_flags_[current_trajectory].controls_on[curr_name] = flag_vals[swap_name][0];
    waypoint_flags_[current_trajectory].compact_view[curr_name] = flag_vals[swap_name][1];
    waypoint_flags_[current_trajectory].adjust_offset[curr_name] = flag_vals[swap_name][2];
    waypoint_flags_[current_trajectory].move_offset[curr_name] = flag_vals[swap_name][3];

    waypoint_flags_[current_trajectory].controls_on[swap_name] = flag_vals[curr_name][0];
    waypoint_flags_[current_trajectory].compact_view[swap_name] = flag_vals[curr_name][1];
    waypoint_flags_[current_trajectory].adjust_offset[swap_name] = flag_vals[curr_name][2];
    waypoint_flags_[current_trajectory].move_offset[swap_name] = flag_vals[curr_name][3];
    
  } else {
    return false;
  }

  affordance_template_object::EndEffectorWaypoint curr_wp, swap_wp;
  bool get_curr = getWaypoint(current_trajectory, ee_id, curr_id, curr_wp);
  bool get_swap = getWaypoint(current_trajectory, ee_id, swap_id, swap_wp);
  bool set_swap = setWaypoint(current_trajectory, ee_id, curr_id, swap_wp);
  bool set_curr = setWaypoint(current_trajectory, ee_id, swap_id, curr_wp);

  removeMarkerAndRebuild(curr_name);
  removeMarkerAndRebuild(swap_name);

  return (get_curr && get_swap && set_swap && set_curr);
}

bool AffordanceTemplate::addWaypointBeforeHandler(std::string wp_name) {

  affordance_template_object::EndEffectorWaypoint wp, wp_new, wp_prev;

  std::string current_trajectory = current_trajectory_;

  // get the current waypoint
  if(!isWaypoint(wp_name)) {
    ROS_ERROR("AffordanceTemplate::addWaypointBeforeHandler() -- %s not a waypoint, can't add a new Waypoint before it", wp_name.c_str());
    return false;
  }
  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);
  getWaypoint(current_trajectory, ee_id, wp_id, wp);

  // set basic stuff for new wp based on the one we are adding it off of 
  wp_new.ee_pose = wp.ee_pose;
  wp_new.display_object = wp.display_object;
  wp_new.controls = wp.controls;
  wp_new.planner_type = wp.planner_type;
  wp_new.bounds = wp.bounds;
  wp_new.conditioning_metric = wp.conditioning_metric;
  wp_new.task_compatibility = wp.task_compatibility;
  wp_new.origin = wp.origin; // copy the orienation, will adjust position below
  wp_new.tool_offset.position[0] = wp_new.tool_offset.position[1] = wp_new.tool_offset.position[2] = 0.0; 
  wp_new.tool_offset.orientation[0] = wp_new.tool_offset.orientation[1] = wp_new.tool_offset.orientation[2] = 0.0; 

  // set the position to be halfway between selected wp and the previous wp (or just off it, if selected is the first in traj)
  if(wp_id > 0) {
    ROS_DEBUG("AffordanceTemplate::addWaypointBeforeHandler() -- averging position with wp %d", wp_id-1);
    getWaypoint(current_trajectory, ee_id, wp_id-1, wp_prev);
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = (wp_prev.origin.position[i] + wp.origin.position[i])/2.0;
    }            
    ROS_DEBUG("wp_%d pose  = (%.3f,%.3f,%.3f)",wp_id-1,wp_prev.origin.position[0],wp_prev.origin.position[1],wp_prev.origin.position[2]);
    ROS_DEBUG("wp_%d pose  = (%.3f,%.3f,%.3f)",wp_id,wp.origin.position[0],wp.origin.position[1],wp.origin.position[2]);
    ROS_DEBUG("new wp pose = (%.3f,%.3f,%.3f)",wp_new.origin.position[0],wp_new.origin.position[1],wp_new.origin.position[2]);
  } else {
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = wp.origin.position[i] + 0.025;
    }
  }

  // insert the new waypoint into the structure and rebuild the template IMs
  if(!insertWaypointInTrajectory(wp_new, ee_id, wp_id)) {
    ROS_ERROR("AffordanceTemplate::addWaypointBeforeHandler() -- problem inserting new waypoint before %s", wp_name.c_str());
    return false;
  } 
  buildTemplate();
  return true;

}

bool AffordanceTemplate::addWaypointAfterHandler(std::string wp_name) 
{

  ROS_DEBUG("AffordanceTemplate::addWaypointAfterHandler() -- [Add Waypoint After] for EE waypoint %s", wp_name.c_str());
  affordance_template_object::EndEffectorWaypoint wp, wp_new, wp_next;

  std::string current_trajectory = current_trajectory_;

  // get the current waypoint
  if(!isWaypoint(wp_name)) {
    ROS_ERROR("AffordanceTemplate::addWaypointAfterHandler() -- %s not a waypoint, can't add a new Waypoint after it", wp_name.c_str());
    return false;
  }
  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);
  getWaypoint(current_trajectory, ee_id, wp_id, wp);

  // set basic stuff for new wp based on the one we are adding it off of 
  wp_new.ee_pose = wp.ee_pose;
  wp_new.display_object = wp.display_object;
  wp_new.controls = wp.controls;
  wp_new.planner_type = wp.planner_type;
  wp_new.bounds = wp.bounds;
  wp_new.conditioning_metric = wp.conditioning_metric;
  wp_new.task_compatibility = wp.task_compatibility;
  wp_new.origin = wp.origin; // copy the orienation, will adjust position below
  wp_new.tool_offset.position[0] = wp_new.tool_offset.position[1] = wp_new.tool_offset.position[2] = 0.0; 
  wp_new.tool_offset.orientation[0] = wp_new.tool_offset.orientation[1] = wp_new.tool_offset.orientation[2] = 0.0; 

  // set the position to be halfway between selected wp and the previous wp (or just off it, if selected is the first in traj)
  if(wp_id < getNumWaypoints(current_trajectory,ee_id)-1) {
    ROS_DEBUG("AffordanceTemplate::addWaypointAfterHandler() -- averging position with wp %d", wp_id+1);
    getWaypoint(current_trajectory, ee_id, wp_id+1, wp_next);
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = (wp_next.origin.position[i] + wp.origin.position[i])/2.0;
    }            
    ROS_DEBUG("wp_%d pose  = (%.3f,%.3f,%.3f)",wp_id,wp.origin.position[0],wp.origin.position[1],wp.origin.position[2]);
    ROS_DEBUG("wp_%d pose  = (%.3f,%.3f,%.3f)",wp_id+1,wp_next.origin.position[0],wp_next.origin.position[1],wp_next.origin.position[2]);
    ROS_DEBUG("new wp pose = (%.3f,%.3f,%.3f)",wp_new.origin.position[0],wp_new.origin.position[1],wp_new.origin.position[2]);
  } else {
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = wp.origin.position[i] - 0.025;
    }
  }

  // insert the new waypoint into the structure and rebuild the template IMs
  if(!insertWaypointInTrajectory(wp_new, ee_id, wp_id+1)) {
    ROS_ERROR("AffordanceTemplate::addWaypointAfterHandler() -- problem inserting new waypoint after %s", wp_name.c_str());
    return false;
  } 
  buildTemplate();
  return true;

}

bool AffordanceTemplate::addWaypointBeforeTrajectoryHandler(std::string obj_name, int ee_id) 
{
  ROS_DEBUG("AffordanceTemplate::addWaypointBeforeTrajectoryHandler() -- [Add Waypoint BeforeE] for Object %s", obj_name.c_str());
  affordance_template_object::EndEffectorWaypoint wp, wp_new;

  std::string current_trajectory = current_trajectory_;

  // get the current waypoint
  if(!isObject(obj_name)) {
    ROS_ERROR("AffordanceTemplate::addWaypointBeforeTrajectoryHandler() -- %s not an object, can't add a new Waypoint before it", obj_name.c_str());
    return false;
  }
  int wp_id = getNumWaypoints(current_trajectory,ee_id);

  ROS_DEBUG("AffordanceTemplate::addWaypointBeforeTrajectoryHandler() -- ee_id: %d, wp_id: %d", ee_id, wp_id);
             
  if(wp_id > 0) {

    // set basic stuff for new wp based on the last one in the trajectory 
    getWaypoint(current_trajectory, ee_id, 0, wp);
    wp_new.ee_pose = wp.ee_pose;
    wp_new.display_object = wp.display_object;
    wp_new.controls = wp.controls;
    wp_new.planner_type = wp.planner_type;
    wp_new.bounds = wp.bounds;
    wp_new.conditioning_metric = wp.conditioning_metric;
    wp_new.task_compatibility = wp.task_compatibility;
    wp_new.origin = wp.origin; // copy the orienation, will adjust position below
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = wp.origin.position[i] + 0.025;
    }
    
  } else {

    // there are no wp's for this ee-id, so just set default basic stuff 
    wp_new.ee_pose = 0;
    wp_new.display_object = removeID(obj_name); // wtf
    wp_new.controls.translation[0] = wp_new.controls.translation[1] = wp_new.controls.translation[2] = true;
    wp_new.controls.rotation[0] = wp_new.controls.rotation[1] = wp_new.controls.rotation[2] = true;
    wp_new.controls.scale = 0.25;             
    wp_new.origin.position[0] = wp_new.origin.position[1] = wp_new.origin.position[2] = 0.1;
    wp_new.origin.orientation[0] = wp_new.origin.orientation[1] = wp_new.origin.orientation[2] = 0.0;
    
    // what to do here?
    wp_new.planner_type = stringToPlannerType("CARTESIAN");
    wp_new.bounds = wp.bounds;
    wp_new.conditioning_metric = wp.conditioning_metric;
    wp_new.task_compatibility = wp.task_compatibility;
    
  }

  wp_new.tool_offset.position[0] = wp_new.tool_offset.position[1] = wp_new.tool_offset.position[2] = 0.0; 
  wp_new.tool_offset.orientation[0] = wp_new.tool_offset.orientation[1] = wp_new.tool_offset.orientation[2] = 0.0; 

  ROS_DEBUG("new wp pose = (%.3f,%.3f,%.3f)",wp_new.origin.position[0],wp_new.origin.position[1],wp_new.origin.position[2]);

  // insert the new waypoint into the structure and rebuild the template IMs
  if(!insertWaypointInTrajectory(wp_new, ee_id, 0)) {
    ROS_ERROR("AffordanceTemplate::addWaypointBeforeTrajectoryHandler() -- problem inserting new waypoint before %s", obj_name.c_str());
    return false;
  } 
  buildTemplate();
  
  return true;

}

bool AffordanceTemplate::addWaypointAfterTrajectoryHandler(std::string obj_name, int ee_id)
{
  
  ROS_DEBUG("AffordanceTemplate::addWaypointAfterTrajectoryHandler() -- [Add Waypoint After] for Object %s", obj_name.c_str());
  affordance_template_object::EndEffectorWaypoint wp, wp_new;

  std::string current_trajectory = current_trajectory_;

  // get the current waypoint
  if(!isObject(obj_name)) {
    ROS_ERROR("AffordanceTemplate::addWaypointAfterTrajectoryHandler() -- %s not an object, can't add a new Waypoint after it", obj_name.c_str());
    return false;
  }
  int wp_id = getNumWaypoints(current_trajectory,ee_id);

  ROS_DEBUG("AffordanceTemplate::addWaypointAfterTrajectoryHandler() -- ee_id: %d, wp_id: %d", ee_id, wp_id);
             
  if(wp_id > 0) {

    // set basic stuff for new wp based on the last one in the trajectory 
    getWaypoint(current_trajectory, ee_id, wp_id-1, wp);
    wp_new.ee_pose = wp.ee_pose;
    wp_new.display_object = wp.display_object;
    wp_new.controls = wp.controls;
    wp_new.planner_type = wp.planner_type;
    wp_new.bounds = wp.bounds;
    wp_new.conditioning_metric = wp.conditioning_metric;
    wp_new.task_compatibility = wp.task_compatibility;
    wp_new.origin = wp.origin; // copy the orienation, will adjust position below
    for(int i=0; i<3; i++) {
      wp_new.origin.position[i] = wp.origin.position[i] - 0.025;
    }
    
  } else {

    // there are no wp's for this ee-id, so just set default basic stuff 
    wp_new.ee_pose = 0;
    wp_new.display_object = removeID(obj_name); // wtf
    wp_new.controls.translation[0] = wp_new.controls.translation[1] = wp_new.controls.translation[2] = true;
    wp_new.controls.rotation[0] = wp_new.controls.rotation[1] = wp_new.controls.rotation[2] = true;
    wp_new.controls.scale = 0.25;             
    wp_new.origin.position[0] = wp_new.origin.position[1] = wp_new.origin.position[2] = 0.1;
    wp_new.origin.orientation[0] = wp_new.origin.orientation[1] = wp_new.origin.orientation[2] = 0.0;
    
    // what to do here?
    wp_new.planner_type = stringToPlannerType("CARTESIAN");
    wp_new.bounds = wp.bounds;
    wp_new.conditioning_metric = wp.conditioning_metric;
    wp_new.task_compatibility = wp.task_compatibility;
    
  }

  wp_new.tool_offset.position[0] = wp_new.tool_offset.position[1] = wp_new.tool_offset.position[2] = 0.0; 
  wp_new.tool_offset.orientation[0] = wp_new.tool_offset.orientation[1] = wp_new.tool_offset.orientation[2] = 0.0; 

  ROS_DEBUG("new wp pose = (%.3f,%.3f,%.3f)",wp_new.origin.position[0],wp_new.origin.position[1],wp_new.origin.position[2]);

  // insert the new waypoint into the structure and rebuild the template IMs
  if(!insertWaypointInTrajectory(wp_new, ee_id, wp_id)) {
    ROS_ERROR("AffordanceTemplate::addWaypointAfterTrajectoryHandler() -- problem inserting new waypoint after %s", obj_name.c_str());
    return false;
  } 
  buildTemplate();
 
  return true;
}
    
bool AffordanceTemplate::deleteWaypointHandler(std::string wp_name)
{
  ROS_DEBUG("AffordanceTemplate::deleteWaypointHandler() -- %s", wp_name.c_str());
  // get the current waypoint
  if(!isWaypoint(wp_name)) {
    ROS_ERROR("AffordanceTemplate::deleteWaypointHandler() -- %s not a waypoint", wp_name.c_str());
    return false;
  }
  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);
  if(!deleteWaypointFromTrajectory(ee_id, wp_id)){
    ROS_ERROR("AffordanceTemplate::deleteWaypointHandler() -- error deleting waypoint %s", wp_name.c_str());
    return false;
  }
  buildTemplate();
  return true;
}

bool AffordanceTemplate::syncToActualHandler(const std::string wp_name)
{
  ROS_DEBUG("AffordanceTemplate::syncToActualHandler() -- %s", wp_name.c_str());
  
  std::string current_trajectory = current_trajectory_;

  // get the current waypoint
  if(!isWaypoint(wp_name)) {
    ROS_ERROR("AffordanceTemplate::syncToActualHandler() -- %s not a waypoint", wp_name.c_str());
    return false;
  }
  int wp_id = getWaypointIDFromName(wp_name);
  int ee_id = getEEIDFromName(wp_name);

  affordance_template_object::EndEffectorWaypoint wp;
  if (!getWaypoint(current_trajectory, ee_id, wp_id, wp)) {
    ROS_ERROR("AffordanceTemplate::syncToActualHandler() -- couldn't find waypoint %s", wp_name.c_str());
    return false;
  }

  tf::Transform wpTee, eeTwp, objTee, objTwp;

  std::string ee_frame_name = getEEFrameName(wp_name);
  tf::poseMsgToTF(frame_store_[ee_frame_name].second.pose, wpTee);
  eeTwp = wpTee.inverse();
  
  // get tf obj --> ee
  geometry_msgs::TransformStamped g_stamped;
  std::string obj_name = appendID(wp.display_object);
  std::string ee_name = robot_interface_->getPlanner()->getControlFrame(robot_interface_->getEEName(ee_id));
  try {
    tf::StampedTransform stamped;
    ros::Time t = frame_store_[wp_name].second.header.stamp;
    tf_listener_.waitForTransform(obj_name, ee_name, t, ros::Duration(3.0));
    tf_listener_.lookupTransform(obj_name, ee_name, t, stamped);
    
    tf::transformStampedTFToMsg(stamped, g_stamped);
    geometry_msgs::Transform g_tf = g_stamped.transform;
    tf::transformMsgToTF(g_tf, objTee);

  } catch(tf::TransformException ex) {
    ROS_ERROR("AffordanceTemplate::syncToActualHandler() -- trouble getting transform from %s to %s. TransformException: %s", obj_name.c_str(), ee_name.c_str(), ex.what());
    return false;
  }
  
  objTwp = objTee * eeTwp;
  geometry_msgs::PoseStamped ps; 
  ps.header = g_stamped.header;
  ps.pose.position.x = objTwp.getOrigin().getX();
  ps.pose.position.y = objTwp.getOrigin().getY();
  ps.pose.position.z = objTwp.getOrigin().getZ();
  tf::quaternionTFToMsg(objTwp.getRotation(), ps.pose.orientation);
  setFrame(wp_name, ps);

  server_->setPose(wp_name, ps.pose); // if we don't do this here then the visualization of the sync won't happen until the server changes get applied in the main spin loop every 10 seconds
  server_->applyChanges();

  return updatePoseInStructure(wp_name, ps.pose);
}

void AffordanceTemplate::processConditioningMetricFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  // change conditioning metric
  std::vector<std::string> metrics;
  std::string current_trajectory = current_trajectory_;

  robot_interface_->getPlanner()->getConditioningMetrics(metrics);
  for(auto &metric: metrics) {
    MenuHandleKey key;
    key[feedback->marker_name] = {"Conditioning Metric", metric};
    if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[key] == feedback->menu_entry_id) {
        ROS_DEBUG("[AffordanceTemplate::processFeedback] changing conditioning metric to %s for waypoint %s", metric.c_str(), feedback->marker_name.c_str());
        for (auto& traj : structure_.ee_trajectories) {
          if (traj.name == current_trajectory) {
            // look for the object the user selected in our waypoint list
            for (auto& wp_list: traj.ee_waypoint_list) {
              int wp_id = -1; // init to -1 because we pre-add
              for (auto& wp: wp_list.waypoints) {
                std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
                if (wp_name == feedback->marker_name) {
                  wp.conditioning_metric = metric;
                  marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
                  break;
                }
              }
            }
          }
        }
      } else {
        marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
      }
    }
  }
  marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
  server_->applyChanges();
  //marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );

}

void AffordanceTemplate::processPositionToleranceFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  // change position tolerance
  std::vector<std::string> types;
  std::string mode = "position";
  std::string current_trajectory = current_trajectory_;
  robot_interface_->getPlanner()->toleranceUtil->getToleranceTypes(mode, types);
  for(auto &t: types) {
    MenuHandleKey key;
    key[feedback->marker_name] = {"Position Tolerance", t};
    if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[key] == feedback->menu_entry_id) {
        for (auto& traj : structure_.ee_trajectories) {
          if (traj.name == current_trajectory) {
            for (auto& wp_list: traj.ee_waypoint_list) {
              int wp_id = -1; // init to -1 because we pre-add
              for (auto& wp: wp_list.waypoints) {
                std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
                if (wp_name == feedback->marker_name) {
                  tolerance_util::ToleranceVal p_tol;
                  robot_interface_->getPlanner()->toleranceUtil->getToleranceVal(mode, t, p_tol);
                  ROS_DEBUG_STREAM("Updating " << wp_name.c_str() << "pos tol to " << t.c_str());
                  // TODO : remove the magic numbers here
                  for(int i=0; i < 3; i++){
                    // TODO : these might be flipped. Using template format
                    wp.bounds.position[i][0] = p_tol[i];
                    wp.bounds.position[i][1] = -p_tol[i];
                  }
                  marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
                  break;
                }
              }
            }
          }
        }
      }else{
        marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
      }
    }
  }
  marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
  server_->applyChanges();
  //marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );

}


void AffordanceTemplate::processOrientationToleranceFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  // change orientation tolerance
  std::vector<std::string> types;
  std::string mode = "orientation";
  std::string current_trajectory = current_trajectory_;
  robot_interface_->getPlanner()->toleranceUtil->getToleranceTypes(mode, types);
  for(auto &t: types) {
    MenuHandleKey key;
    key[feedback->marker_name] = {"Orientation Tolerance", t};
    if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[key] == feedback->menu_entry_id) {
        for (auto& traj : structure_.ee_trajectories) {
          if (traj.name == current_trajectory) {
            for (auto& wp_list: traj.ee_waypoint_list) {
              int wp_id = -1; // init to -1 because we pre-add
              for (auto& wp: wp_list.waypoints) {
                std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
                if (wp_name == feedback->marker_name) {
                  tolerance_util::ToleranceVal o_tol;
                  robot_interface_->getPlanner()->toleranceUtil->getToleranceVal(mode, t, o_tol);
                  ROS_DEBUG_STREAM("Updating " << wp_name.c_str() << "rot tol to " << t.c_str());
                  // TODO : remove the magic numbers here
                  for(int i=0; i < 3; i++){
                    // TODO : these might be flipped. Using template format
                    wp.bounds.orientation[i][0] = o_tol[i];
                    wp.bounds.orientation[i][1] = -o_tol[i];
                  }
                  marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
                  break;
                }
              }
            }
          }
        }
      }else{
        marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
      }
    }
  }
  marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
  server_->applyChanges();
  //marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );

}


void AffordanceTemplate::processFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  ROS_DEBUG("AffordanceTemplate::processFeedback(%s) -- %s", robot_name_.c_str(), feedback->marker_name.c_str());

  std::string current_trajectory = current_trajectory_;

  interactive_markers::MenuHandler::CheckState state;

  // set up key maps for easy comparison to menu handler ID
  MenuHandleKey swap_prev_wp_key;
  MenuHandleKey swap_next_wp_key;
  MenuHandleKey wp_before_key;
  MenuHandleKey wp_after_key;
  MenuHandleKey reset_key;
  MenuHandleKey save_key;
  MenuHandleKey delete_key;
  MenuHandleKey hide_controls_key;
  MenuHandleKey view_mode_key;
  MenuHandleKey play_plan_key;
  MenuHandleKey loop_key;
  MenuHandleKey autoplay_key;
  MenuHandleKey adjust_offset_key;
  MenuHandleKey move_offset_key;
  MenuHandleKey planner_key;
  MenuHandleKey sync_key;

  swap_prev_wp_key[feedback->marker_name]  = {"Move Back"};
  swap_next_wp_key[feedback->marker_name]  = {"Move Forward"};
  wp_before_key[feedback->marker_name]     = {"Add Waypoint Before"};
  wp_after_key[feedback->marker_name]      = {"Add Waypoint After"};
  reset_key[feedback->marker_name]         = {"Reset"};
  save_key[feedback->marker_name]          = {"Save"};
  delete_key[feedback->marker_name]        = {"Delete Waypoint"};
  hide_controls_key[feedback->marker_name] = {"Hide Controls"};
  view_mode_key[feedback->marker_name]     = {"Compact View"};
  play_plan_key[feedback->marker_name]     = {"(Re)Play Plan"};
  loop_key[feedback->marker_name]          = {"Loop Animation"};
  autoplay_key[feedback->marker_name]      = {"Autoplay"};
  adjust_offset_key[feedback->marker_name] = {"Change Tool Offset"};
  move_offset_key[feedback->marker_name]   = {"Move Tool Point"};
  planner_key[feedback->marker_name]       = {"Use Cartesian Plans"};
  sync_key[feedback->marker_name]          = {"Sync To Actual"};

  // update the fames everytime this is called
  geometry_msgs::PoseStamped ps;
  ps.pose = feedback->pose;
  ps.header = feedback->header;
  if(!updatePoseFrames(feedback->marker_name, ps)) {
    ROS_ERROR("AffordanceTemplate::processFeedback() -- error updating pose frames for %s", feedback->marker_name.c_str());
    return;
  }

  switch ( feedback->event_type ) {

  case visualization_msgs::InteractiveMarkerFeedback::MOUSE_UP :
    {
      if(!updatePoseInStructure(feedback->marker_name, feedback->pose)) {
        ROS_ERROR("AffordanceTemplate::processFeedback() -- problem updating pose in structure for %s", feedback->marker_name.c_str());
        break;
      }
      break;
    }
  case visualization_msgs::InteractiveMarkerFeedback::MENU_SELECT : {
    ROS_DEBUG("[AffordanceTemplate::processFeedback] %s selected menu entry: %d", feedback->marker_name.c_str(), feedback->menu_entry_id);
    // ROS_DEBUG("AffordanceTemplate::processFeedback() --   pose: (%.3f, %.3f, %.3f), (%.3f, %.3f, %.3f, %.3f), frame_id: %s", 
    //                                                             feedback->pose.position.x, feedback->pose.position.y, feedback->pose.position.z, 
    //                                                             feedback->pose.orientation.x, feedback->pose.orientation.y, feedback->pose.orientation.z, feedback->pose.orientation.w,
    //                                                             feedback->header.frame_id.c_str());

     
    // 
    // check for 'Add Waypoint Before' for EE objects
    if (group_menu_handles_.find(wp_before_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[wp_before_key] == feedback->menu_entry_id) {         
        if(!addWaypointBeforeHandler(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error adding new waypoint before %s", feedback->marker_name.c_str());
        }
        break;
      }
    }

    // check for 'Add Waypoint After' for EE objects
    if (group_menu_handles_.find(wp_after_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[wp_after_key] == feedback->menu_entry_id) {
        if(!addWaypointAfterHandler(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error adding new waypoint after %s", feedback->marker_name.c_str());
        }
        break; 
      }
    }

    // 
    // check for 'Add Waypoint Before' for AT object (wheel, door, etc) 
    for (auto& ee: robot_interface_->getEENameMap()) 
      {
        MenuHandleKey key;
        key[feedback->marker_name] = {"Add Waypoint Before", ee.second};
        if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
          if (group_menu_handles_[key] == feedback->menu_entry_id) {
            int ee_id = robot_interface_->getEEID(ee.second);
            if(!addWaypointBeforeTrajectoryHandler(feedback->marker_name, ee_id)) {
              ROS_ERROR("AffordanceTemplate::processFeedback() -- error adding new waypoint before object %s", feedback->marker_name.c_str());
            }
            break;
          }
        }
      }

    // 
    // check for 'Add Waypoint After' for AT object (wheel, door, etc) 
    for (auto& ee: robot_interface_->getEENameMap()) 
      {
        MenuHandleKey key;
        key[feedback->marker_name] = {"Add Waypoint After", ee.second};
        if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
          if (group_menu_handles_[key] == feedback->menu_entry_id) {
            int ee_id = robot_interface_->getEEID(ee.second);
            if(!addWaypointAfterTrajectoryHandler(feedback->marker_name, ee_id)) {
              ROS_ERROR("AffordanceTemplate::processFeedback() -- error adding new waypoint after object %s", feedback->marker_name.c_str());
            }
            break;
          }
        }
      }

    // 
    // check for 'Swap Previous Waypoint'
    if (group_menu_handles_.find(swap_prev_wp_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[swap_prev_wp_key] == feedback->menu_entry_id) {
        if(!swapPreviousHandler(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error swapping %s with previous waypoint", feedback->marker_name.c_str());
        }
        break; 
      }
    }

    //
    // check for 'Swap Next Waypoint'
    if (group_menu_handles_.find(swap_next_wp_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[swap_next_wp_key] == feedback->menu_entry_id) {
        if(!swapNextHandler(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error swapping %s with next waypoint", feedback->marker_name.c_str());
        }
        break; 
      }
    }

    //
    // delete waypoint
    if (group_menu_handles_.find(delete_key) != group_menu_handles_.end()) {
      if (group_menu_handles_[delete_key] == feedback->menu_entry_id) {
        if(!deleteWaypointHandler(feedback->marker_name)){
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error deleting waypoint %s", feedback->marker_name.c_str());
        }
        break;
      }
    }

    //
    // sync waypoint to actual
    if (group_menu_handles_.find(sync_key) != group_menu_handles_.end()) {
      if (group_menu_handles_[sync_key] == feedback->menu_entry_id) {
        if (!syncToActualHandler(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- error syncing waypoint: %s to actual", feedback->marker_name.c_str());
        }
        break;
      }
    }

    //
    // change planner types
    if (group_menu_handles_.find(planner_key) != group_menu_handles_.end()) {
      if (group_menu_handles_[planner_key] == feedback->menu_entry_id) {
        // get the current state
        std::string planner = "CARTESIAN";
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::CHECKED)
            {
              marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::UNCHECKED );
              planner = "JOINT";
            }
          else
            {
              marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::CHECKED );
              planner = "CARTESIAN";
            }

          ROS_DEBUG("[AffordanceTemplate::processFeedback] changing planner type %s for waypoint %s", planner.c_str(), feedback->marker_name.c_str());
          for (auto& traj : structure_.ee_trajectories) {
            if (traj.name == current_trajectory) {
              // look for the object the user selected in our waypoint list
              for (auto& wp_list: traj.ee_waypoint_list) {
                int wp_id = -1; // init to -1 because we pre-add
                for (auto& wp: wp_list.waypoints) {
                  std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
                  if (wp_name == feedback->marker_name) {
                    wp.planner_type = planner;
                    break;
                  }
                }
              }
            }
          }
        }
      }
    }

    //
    // reset AT
    if(group_menu_handles_.find(reset_key) != std::end(group_menu_handles_)) {
      if(group_menu_handles_[reset_key] == feedback->menu_entry_id) {
        resetTemplate();
      }
    }

    //
    // save AT to file
    if (group_menu_handles_.find(save_key) != group_menu_handles_.end()) 
      {
        if (group_menu_handles_[save_key] == feedback->menu_entry_id) 
          {
            ROS_DEBUG("[AffordanceTemplate::processFeedback::Save] saving file");
            std::vector<std::string> keys;
            boost::split(keys, structure_.filename, boost::is_any_of("/"));
            if (keys.size()) {
              saveToDisk(keys.back(), structure_.image, structure_.name, true); //feedback->marker_name
              initial_structure_ = structure_; // reset our initial structure to the one we just saved
            } else {
              ROS_ERROR("[AffordanceTemplate::processFeedback::Save] invalid filename: %s", structure_.filename.c_str());
            }
          }
      }

    //
    // toggle controls
    if(group_menu_handles_.find(hide_controls_key) != std::end(group_menu_handles_)) {
      if(group_menu_handles_[hide_controls_key] == feedback->menu_entry_id) {
        ROS_DEBUG_STREAM("AffordanceTemplate::processFeedback() --   CONTROLS TOGGLE for " << feedback->marker_name << " id: " << feedback->menu_entry_id);
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::CHECKED) {
            marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::CHECKED );
            if(isObject(feedback->marker_name)) {
              object_controls_display_on_[feedback->marker_name] = true;
            } else {
              waypoint_flags_[current_trajectory].controls_on[feedback->marker_name] = true;
            }
          } else {
            marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::UNCHECKED );
            if(isObject(feedback->marker_name)) {
              object_controls_display_on_[feedback->marker_name] = false;
            } else {
              waypoint_flags_[current_trajectory].controls_on[feedback->marker_name] = false;
            }
          }
        }
        if(!removeMarkerAndRebuild(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- failed toggling controls");
        }
      }
    }

    //
    // toggle EE compact view 
    if(group_menu_handles_.find(view_mode_key) != std::end(group_menu_handles_)) {
      if(group_menu_handles_[view_mode_key] == feedback->menu_entry_id) {
        ROS_DEBUG("AffordanceTemplate::processFeedback() --   VIEW MODE TOGGLE");
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::UNCHECKED) {
            marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::CHECKED );
            if(!isObject(feedback->marker_name)) {
              waypoint_flags_[current_trajectory].compact_view[feedback->marker_name] = true;
            }
          } else {
            marker_menus_[feedback->marker_name].setCheckState( feedback->menu_entry_id, interactive_markers::MenuHandler::UNCHECKED );
            if(!isObject(feedback->marker_name)) {
              waypoint_flags_[current_trajectory].compact_view[feedback->marker_name] = false;
            }
          }
        }
        if(!removeMarkerAndRebuild(feedback->marker_name)) {
          ROS_ERROR("AffordanceTemplate::processFeedback() -- failed toggling view mode");
        }
      }
    }

    //
    // toggle adjust tool 6dof markers  
    if(group_menu_handles_.find(adjust_offset_key) != std::end(group_menu_handles_)) {
      // figure out if the menu was selected from the wp or the tp 
      std::string wp_name, tp_name;
      if(isWaypoint(feedback->marker_name)) {
        wp_name = feedback->marker_name;
        tp_name = getToolPointFrameName(wp_name);
      } else if(isToolPointFrame(feedback->marker_name)) {
        tp_name = feedback->marker_name;
        wp_name = getWaypointFrame(tp_name);
      }
      if(group_menu_handles_[adjust_offset_key] == feedback->menu_entry_id) {
        ROS_DEBUG("AffordanceTemplate::processFeedback() --   ADJUST TOOL TOGGLE");
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::UNCHECKED) {
            waypoint_flags_[current_trajectory].adjust_offset[wp_name] = true;
            waypoint_flags_[current_trajectory].move_offset[wp_name] = false;
            marker_menus_[wp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::CHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::CHECKED );
            marker_menus_[wp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );                          
          } else {           
            waypoint_flags_[current_trajectory].adjust_offset[wp_name] = false;
            waypoint_flags_[current_trajectory].move_offset[wp_name] = false;
            marker_menus_[wp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[wp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );
          }
          removeInteractiveMarker(wp_name);
          removeInteractiveMarker(tp_name);                   
          if(!buildTemplate()) {
            ROS_ERROR("AffordanceTemplate::processFeedback() -- failed toggling adust tool controls");
          }
        }
      }
    }

    //
    // toggle move tool 6dof markers  
    if(group_menu_handles_.find(move_offset_key) != std::end(group_menu_handles_)) {
      // figure out if the menu was selected from the wp or the tp 
      std::string wp_name, tp_name;
      if(isWaypoint(feedback->marker_name)) {
        wp_name = feedback->marker_name;
        tp_name = getToolPointFrameName(wp_name);
      } else if(isToolPointFrame(feedback->marker_name)) {
        tp_name = feedback->marker_name;
        wp_name = getWaypointFrame(tp_name);
      }  
      if( group_menu_handles_[move_offset_key] == feedback->menu_entry_id ) {
        ROS_DEBUG("AffordanceTemplate::processFeedback() --   MOVE TOOL TOGGLE");
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::UNCHECKED) {
            waypoint_flags_[current_trajectory].adjust_offset[wp_name] = false;
            waypoint_flags_[current_trajectory].move_offset[wp_name] = true;
            marker_menus_[wp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::CHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::CHECKED );
            marker_menus_[wp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );          
          } else {
            waypoint_flags_[current_trajectory].move_offset[wp_name] = false;
            waypoint_flags_[current_trajectory].adjust_offset[wp_name] = false;
            marker_menus_[wp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[move_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[wp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );
            marker_menus_[tp_name].setCheckState( group_menu_handles_[adjust_offset_key], interactive_markers::MenuHandler::UNCHECKED );
          }
          removeInteractiveMarker(wp_name);
          removeInteractiveMarker(tp_name);
          if(!buildTemplate()) {
            ROS_ERROR("AffordanceTemplate::processFeedback() -- failed toggling adust tool controls");
          }
        }
      }
    }

    //
    // switch trajectories using the context menu
    for (auto &traj: structure_.ee_trajectories) {
      MenuHandleKey key;
      key[feedback->marker_name] = {"Choose Trajectory", traj.name}; // FIXME -- can this be static like this??
      if (group_menu_handles_.find(key) != group_menu_handles_.end()) {
        marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED);
        if (group_menu_handles_[key] == feedback->menu_entry_id)  {
          ROS_DEBUG("[AffordanceTemplate::processFeedback::Choose Trajectory] found matching trajectory name %s", traj.name.c_str());
          setTrajectory(traj.name);
          marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED);
          removeAllMarkers();//FIXME here
          if(!buildTemplate()) {
            ROS_ERROR("AffordanceTemplate::processFeedback() -- failed switching trajectory");
          }
        }
      }
    }

    //
    // handle sub menu item selections
    if(isWaypoint(feedback->marker_name)) {
      int ee_id = getEEIDfromWaypointName(feedback->marker_name);
      std::string ee_name = robot_interface_->getEEName(ee_id);

      // change EE pose

      for(auto &pn : robot_interface_->getEEPoseNames(ee_name)) {
        MenuHandleKey key;
        key[feedback->marker_name] = {"Change End-Effector Pose", pn};
        if (group_menu_handles_.find(key) != std::end(group_menu_handles_)) {
          if (group_menu_handles_[key] == feedback->menu_entry_id) {
            ROS_DEBUG("AffordanceTemplate::processFeedback() -- changing EE[%s] pose to \'%s\'", ee_name.c_str(), pn.c_str());
            for (auto& traj : structure_.ee_trajectories) {
              if (traj.name == current_trajectory) {
                // look for the object the user selected in our waypoint list
                for (auto& wp_list: traj.ee_waypoint_list) {
                  int wp_id = -1; // init to -1 because we pre-add
                  for (auto& wp: wp_list.waypoints) {
                    std::string wp_name = createWaypointID(wp_list.id, ++wp_id);
                    if (wp_name == feedback->marker_name) {
                      wp.ee_pose = robot_interface_->getEEPoseIDMap(ee_name)[pn];
                      if(!removeMarkerAndRebuild(feedback->marker_name)) {
                        ROS_ERROR("AffordanceTemplate::processFeedback() -- failed creating structure with new EE pose");
                      }
                      break;
                    }
                  }
                }
              }
            }
          }
        }
      }


      marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
      server_->applyChanges();
    }

    if (group_menu_handles_.find(play_plan_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[play_plan_key] == feedback->menu_entry_id) {
        ROS_DEBUG("[AffordanceTemplate::processFeedback] playing available plan");
        robot_interface_->getPlanner()->playAnimation();
      }
    }

    if (group_menu_handles_.find(loop_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[loop_key] == feedback->menu_entry_id) {
        ROS_DEBUG("[AffordanceTemplate::processFeedback] changing looping functionality");
        MenuHandleKey key;
        key[feedback->marker_name] = {"Loop Animation"};
        bool loop = false;
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) ) {
          if(state == interactive_markers::MenuHandler::CHECKED) {
            marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
          } else {
            marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
            loop = true; // transitioning from not looping to looping
          }
          marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
          server_->applyChanges();
          std::map<int, std::string> ee_list = robot_interface_->getEENameMap();
          for (auto ee : ee_list) {
            robot_interface_->getPlanner()->loopAnimation(ee.second, loop);
            robot_interface_->getPlanner()->loopAnimation(robot_interface_->getManipulator(ee.second), loop);
          }
        } else {
          ROS_ERROR("[AffordanceTemplate::processFeedback] can't get the loop state!!");
        }
      }
    }

    if (group_menu_handles_.find(autoplay_key) != std::end(group_menu_handles_)) {
      if (group_menu_handles_[autoplay_key] == feedback->menu_entry_id) {
        ROS_WARN("[AffordanceTemplate::processFeedback] flipping autoplay functionality");
        MenuHandleKey key;
        key[feedback->marker_name] = {"Autoplay"};
        if(marker_menus_[feedback->marker_name].getCheckState( feedback->menu_entry_id, state ) )  {
          if(state == interactive_markers::MenuHandler::CHECKED)  {
            marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::UNCHECKED );
            autoplay_display_ = false;
          } else {
            marker_menus_[feedback->marker_name].setCheckState( group_menu_handles_[key], interactive_markers::MenuHandler::CHECKED );
            autoplay_display_ = true;
          }
          marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
          server_->applyChanges();
        } else {
          ROS_ERROR("[AffordanceTemplate::processFeedback] can't get the autoplay state!!");
        }
      }
    }
    break;
  }

  default : 
    ROS_DEBUG("[AffordanceTemplate::processFeedback] got unrecognized or unmatched menu event: %d", feedback->event_type);
    break;
  }
  marker_menus_[feedback->marker_name].apply( *server_, feedback->marker_name );
  server_->applyChanges();
}

bool AffordanceTemplate::isObject(const std::string& obj) {
  for(auto &o : structure_.display_objects) {
    if(o.name == obj) {
      return true;
    }
  }
  return false;
}

bool AffordanceTemplate::isWaypoint(const std::string& wp) {
  return (!isObject(wp) && !isToolPointFrame(wp) && !isControlPointFrame(wp) && !isEEFrame(wp));
}

bool AffordanceTemplate::isToolPointFrame(const std::string& tp) {
  return tp.find("/tp")!=std::string::npos;
}

bool AffordanceTemplate::isControlPointFrame(const std::string& cp) {
  return cp.find("/cp")!=std::string::npos;
}

bool AffordanceTemplate::isEEFrame(const std::string& ee) {
  return ee.find("/ee")!=std::string::npos;
}

bool AffordanceTemplate::hasObjectFrame(std::string obj) {
  return isObject(obj) && (frame_store_.find(obj) != std::end(frame_store_)); 
}

bool AffordanceTemplate::hasWaypointFrame(std::string wp) {
  return isWaypoint(wp) && (frame_store_.find(wp) != std::end(frame_store_)); 
}

bool AffordanceTemplate::hasToolPointFrame(std::string tp) {
  return isToolPointFrame(tp) && (frame_store_.find(tp) != std::end(frame_store_)); 
}

bool AffordanceTemplate::hasControlPointFrame(std::string cp) {
  return isControlPointFrame(cp) && (frame_store_.find(cp) != std::end(frame_store_)); 
}

bool AffordanceTemplate::hasFrame(std::string frame_name) {
  return frame_store_.find(frame_name) != std::end(frame_store_); 
}

void AffordanceTemplate::setFrame(std::string frame_name, geometry_msgs::PoseStamped ps) {

  mutex_.lock();
  if(!hasFrame(frame_name))
    frame_store_[frame_name] = FrameInfo(frame_name, ps);
  else
    frame_store_[frame_name].second = ps;
  mutex_.unlock();
  
}

bool AffordanceTemplate::hasControls(std::string name) {
  return hasObjectFrame(name) || hasWaypointFrame(name) || hasToolPointFrame(name);
}

std::string AffordanceTemplate::getWaypointFrame(std::string frame) {
  std::size_t pos = frame.find("/");
  return frame.substr(0,pos);  
} 

int AffordanceTemplate::getNumWaypoints(const std::string traj_name, const int ee_id) {
  for(auto &traj : structure_.ee_trajectories) {
    if(traj.name == traj_name) {
      for(auto &ee_list : traj.ee_waypoint_list) {
        if(ee_list.id == ee_id) {
          return ee_list.waypoints.size();
        }
      }
    }
  }
  return 0;
}

bool AffordanceTemplate::getTrajectoryEEPoseList(std::string trajectory_name, std::string ee, std::vector<affordance_template_object::EndEffectorWaypoint> &wp_vec) {
  wp_vec.clear();
  for(auto& traj : structure_.ee_trajectories) {
    if(traj.name == trajectory_name) {
      for(auto& ee_list : traj.ee_waypoint_list) {
        if(ee_list.id == robot_interface_->getEEID(ee)) {
          wp_vec = ee_list.waypoints;
          break;
        }
      }
    }
  }
  return true;
}


bool AffordanceTemplate::computePathSequence(std::string traj_name, 
                                             int ee_id, int idx, int steps, 
                                             bool direct, bool backwards, 
                                             std::vector<int> &sequence_ids, 
                                             int &next_path_idx)
{ 
  // ROS_WARN_STREAM("computing path seq for traj "<<traj_name<<" for ee "<<ee_id<<" at index "<<idx<<" with "<<steps<<" steps and will "<<(direct?"be ":"not be ")<<"direct motion");
  
  bool aggregate_steps = false;//(steps == -1);
  int current_idx = idx;
  if(aggregate_steps) {
    steps = getNumWaypoints(traj_name, ee_id) - (idx+1);
    ROS_WARN("AffordanceTemplate::computePathSequence() -- going to try to aggregate steps (up to %d) for %s, from idx: %d", steps, traj_name.c_str(), idx);
  }

  bool on_traj = idx > -1;

  sequence_ids.clear();
  if (direct) {
    sequence_ids.push_back(steps-1);
    next_path_idx = steps-1;
    return true;
  } else if (steps == 0) {
    if(idx<0) return false;
    sequence_ids.push_back(idx);
    next_path_idx = idx;
    return true;
  } else {
    int max_idx = getNumWaypoints(traj_name, ee_id)-1;
    int cap = max_idx+1;
    int inc = 1;
    if(backwards) {
      inc = -1;
    }
    if(idx == -1) {
      if(backwards) {
        sequence_ids.push_back(max_idx);
        idx = max_idx;
      } else {
        sequence_ids.push_back(0);
        idx = 0;
      }
      steps--;
    }
    if (steps == -1) {
      sequence_ids.push_back(idx);
    }
    for(int s=0; s<steps; s++) {
      idx += inc;
      if(idx == -1) {
        if(backwards) {
          idx = max_idx;
        } else {
          idx = 0;
        }
      } else {
        idx = idx%cap;
      }
      sequence_ids.push_back(idx);
    }
    next_path_idx = sequence_ids.back();
  }

  if(sequence_ids.empty()) {
    ROS_ERROR("AffordanceTemplate::planRequest() -- couldnt get sequence_ids list for %s", traj_name.c_str());
    return false;
  }
  ROS_INFO("SEQUENCE IDS:");
  for (auto s : sequence_ids) 
    ROS_INFO_STREAM(" " << s);


  // erase all ids after a gripper change:
  if(aggregate_steps) {

    // look up relavent info on gripper poses
    std::string ee = robot_interface_->getEEName(ee_id);
    std::vector<affordance_template_object::EndEffectorWaypoint> wp_vec; 
    if(!getTrajectoryEEPoseList(traj_name, ee, wp_vec)) {
      ROS_ERROR("AffordanceTemplate::planRequest() -- problem getting waypoint list for %s", traj_name.c_str());
      return false;
    } 
    if(wp_vec.empty()) {
      ROS_ERROR("AffordanceTemplate::planRequest() -- couldnt get EE list for %s", traj_name.c_str());
      return false;
    }
    
    int ee_pose;
    if(!on_traj) {
    
      // this is the first time on the trajectory, so let's just move one step and set the ee pose to what it should be
      ee_pose = -1;
      sequence_ids.erase(sequence_ids.begin()+1, sequence_ids.end());
      ROS_WARN("1. AGGREGATED SEQUENCE IDS:");
      for (auto s : sequence_ids) {
        ROS_WARN_STREAM(" " << s);
        next_path_idx = s;
      }
      return true;

    } else { 

      // store the current ee_pose of what step the AT is on
      ee_pose = wp_vec[current_idx].ee_pose;    
      ROS_WARN("AffordanceTemplate::computePathSequence() -- current ee_pose: %d", ee_pose);
      ROS_WARN("AffordanceTemplate::computePathSequence() -- first ee_pose of traj sequence: %d", wp_vec[sequence_ids[0]].ee_pose);

      // check if the first step on the AT traj is different, cause just want to execute one step if so
      if(wp_vec[sequence_ids[0]].ee_pose != ee_pose) {
        sequence_ids.erase(sequence_ids.begin()+1, sequence_ids.end());
        ROS_WARN("2. AGGREGATED SEQUENCE IDS:");
        for (auto s : sequence_ids) {
          ROS_WARN_STREAM(" " << s);
          next_path_idx = s;
         }
        return true;
      }
    }

    for (int id=1; id<sequence_ids.size(); id++) {
      if(wp_vec[sequence_ids[id]].ee_pose != ee_pose) {
        sequence_ids.erase(sequence_ids.begin()+id, sequence_ids.end());
        ROS_WARN("AffordanceTemplate::computePathSequence() -- found different ee_pose %d at step %d", wp_vec[sequence_ids[id]].ee_pose, id);
        ROS_WARN("3. AGGREGATED SEQUENCE IDS:");
        for (auto s : sequence_ids) {
          ROS_WARN_STREAM(" " << s);
          next_path_idx = s;
        }
        return true;
      }
    }
  }

  return true;
}


bool AffordanceTemplate::computeGroupSequence(std::string group, std::string traj_name, int steps, PlanStatus &plan_status) 
{

  ROS_INFO("[AffordanceTemplate::computeGroupSequence] processing request for %s", group.c_str());

  std::vector<affordance_template_object::EndEffectorWaypoint> wp_vec;
  std::map<std::string, std::vector<geometry_msgs::PoseStamped> > goals;
  std::map<std::string, planner_interface::PlanningGoal> goals_full;

  std::string manipulator_name = robot_interface_->getManipulator(group);
  std::map<int, std::string> ee_pose_map = robot_interface_->getEEPoseNameMap(group);
  int group_id = robot_interface_->getEEID(group);
  int max_idx = getNumWaypoints(traj_name, group_id);
  int current_idx;

  // get our waypoints for this trajectory so we can get the EE pose IDs
  if(!getTrajectoryEEPoseList(traj_name, group, wp_vec)) {
    ROS_ERROR("AffordanceTemplate::computeGroupSequence() -- problem getting waypoint list for %s", traj_name.c_str());
    return false;
  }

  if(plan_status.from_start) {
    current_idx = -1;
    plan_status.current_idx = -1;
  } else {
    current_idx = plan_status.current_idx;
  }

  ROS_INFO("[AffordanceTemplate::computeGroupSequence] -- from_start: %d, current_idx: %d, plan_idx: %d, step: %d", 
           (int)plan_status.from_start, current_idx, plan_status.current_idx, steps);
 
  // make sure it matches our max idx number because that is max num waypoints
  if (wp_vec.size() != max_idx) {
    return false;
  }

  // find our sequence IDs first - will use these to loop on
  if (!computePathSequence(traj_name, group_id, plan_status.current_idx,
                           steps, plan_status.direct,
                           plan_status.backwards, 
                           plan_status.sequence_ids, 
                           plan_status.goal_idx)) {
    ROS_ERROR("[AffordanceTemplate::computeGroupSequence] failed to get path sequence!!");
    return false;
  }
    
  return true;
}


bool AffordanceTemplate::createGoalsFromSequence(std::string group, std::string traj_name, std::vector<int> seq_list, std::vector<planner_interface::PlanningGoal> &planning_goals)
{

  int group_id = robot_interface_->getEEID(group);

  for (auto plan_seq : seq_list) {
    
    std::string next_path_str = createWaypointID(group_id, plan_seq);
    int max_idx = getNumWaypoints(traj_name, group_id);

    std::string wp_frame_name = next_path_str;
    std::string tp_frame_name = getToolPointFrameName(wp_frame_name);       
    std::string cp_frame_name = getControlPointFrameName(wp_frame_name);
    std::string ee_frame_name = getEEFrameName(wp_frame_name);
    
    // get helper transforms
    geometry_msgs::PoseStamped  wpTee_ps, eeTcp_ps, wpTtp_ps, cpTtp_ps, wpTcp_ps;
    tf::Transform wpTee, eeTcp, wpTtp, cpTtp,wpTcp;
    getPoseFromFrameStore(ee_frame_name, wpTee_ps);
    getPoseFromFrameStore(cp_frame_name, eeTcp_ps);
    getPoseFromFrameStore(tp_frame_name, wpTtp_ps);
    tf::poseMsgToTF(wpTee_ps.pose,wpTee);
    tf::poseMsgToTF(eeTcp_ps.pose,eeTcp);
    tf::poseMsgToTF(wpTtp_ps.pose,wpTtp); 
    
    // create goal
    planner_interface::PlanningGoal pg;
    geometry_msgs::PoseStamped pt;    
    getPoseFromFrameStore(tp_frame_name, pt);
    pg.goal = pt;
    
    // transform frame offset back to EE frame
    wpTcp = wpTee*eeTcp;
    cpTtp = wpTcp.inverse()*wpTtp;
    geometry_msgs::Pose tp_offset;
    tf::poseTFToMsg(cpTtp, tp_offset);
    pg.offset = tp_offset;
    robot_interface_->getPlanner()->setToolOffset(robot_interface_->getManipulator(group), pg.offset);

    // get the rest of the waypoint infor for goal
    affordance_template_object::EndEffectorWaypoint wp;
    ROS_WARN_STREAM("about to send traj "<<traj_name<<" with group id "<<group_id<<" plan seq "<<plan_seq<<" to get the waypoint");
    if(!getWaypoint(traj_name, group_id, plan_seq, wp)) {
      ROS_ERROR("[AffordanceTemplate::createGoalsFromSequence] waypoint vector size does not match up!!");
      return false;
    }

    pg.task_compatibility = taskCompatibilityToPoseMsg(wp.task_compatibility);  
    pg.conditioning_metric = wp.conditioning_metric;
    pg.type = stringToPlannerType(wp.planner_type);
    
    for(int i = 0; i < 3; ++i)  {
      for(int j = 0; j < 2; ++j) {
        pg.tolerance_bounds[i][j] = wp.bounds.position[i][j];
        pg.tolerance_bounds[i+3][j] = wp.bounds.orientation[i][j];
      }
    }

    ROS_INFO("[AffordanceTemplate::createGoalsFromSequence] configuring plan goal for trajectory %s, waypoint %s [%d/%d] for %s[%d] on manipulator %s, type: %s", 
      traj_name.c_str(),next_path_str.c_str(), 
      plan_seq+1, max_idx, 
      group.c_str(), group_id, 
      robot_interface_->getManipulator(group).c_str(), 
      wp.planner_type.c_str());

    // goals_full[manipulator_name] = pg;
    planning_goals.push_back(pg);

  }

  return true;    

}

void AffordanceTemplate::planRequest(const PlanGoalConstPtr& goal)
{
  ROS_INFO("[AffordanceTemplate::planRequest] request to plan %sfor \"%s\" trajectory...", (goal->execute?"and execute ":""), goal->trajectory.c_str());

  PlanResult result;
  PlanFeedback planning;
  std::string current_trajectory = current_trajectory_;
  std::map<std::string, std::vector<planner_interface::PlanningGoal> > planning_goals;
  std::map<std::string, trajectory_msgs::JointTrajectory> trajectories;
  std::map<std::string, sensor_msgs::JointState> group_start_states;
  std::map<int, int> loop_seq_map;
  int num_goals = -1;

  while (build_request) {
    ROS_INFO_THROTTLE(1,"[AffordanceTemplate::planRequest]: planRequest waiting on buildRequest to finish");
    ros::Duration(0.05).sleep();
  }

  // switch to the right trajectorystd::vector<planner_interface::PlanningGoal>
  if(goal->trajectory!=current_trajectory) {
    ROS_INFO("[AffordanceTemplate::planRequest] switching to trajectory %s", goal->trajectory.c_str());
    if(!switchTrajectory(goal->trajectory)) {
      ROS_ERROR("[AffordanceTemplate::planRequest] no trajectory %s found!!", goal->trajectory.c_str());
      return;
    }
    current_trajectory = current_trajectory_;
  }

  while (build_request) {
    ROS_INFO_THROTTLE(1,"[AffordanceTemplate::planRequest]: planRequest waiting on buildRequest to finish");
    ros::Duration(0.05).sleep();
  }

  m_plan_.lock();

  // clear out each time a plan is requested
  // we only want to keep track of the current trajectory
  control_trajectories_.clear();

  robot_interface_->getPlanner()->resetAnimation(true);  

  for (auto ee : goal->groups) {
    plan_status_[goal->trajectory][ee].plan_valid = false;
    plan_status_[goal->trajectory][ee].exec_valid = false;
    plan_status_[goal->trajectory][ee].direct     = goal->direct;
    plan_status_[goal->trajectory][ee].from_start = goal->from_start;
    plan_status_[goal->trajectory][ee].backwards  = goal->backwards;
    plan_status_[goal->trajectory][ee].sequence_ids.clear();
    plan_status_[goal->trajectory][ee].sequence_poses.clear();
  }
 
  ++planning.progress;
  planning_server_.publishFeedback(planning);

  // create consise list of EEs to plan for
  std::vector<std::string> planning_groups;
  for (auto ee : goal->groups) {
    for(auto& traj : structure_.ee_trajectories) {
      if(traj.name == goal->trajectory) {
        for(auto& ee_list : traj.ee_waypoint_list) {
          if(ee_list.id == robot_interface_->getEEID(ee)) {
            planning_groups.push_back(ee);
          }
        }
      }
    }
  }

  for (auto ee : planning_groups) {
    if(!computeGroupSequence(ee, goal->trajectory, goal->steps, plan_status_[goal->trajectory][ee])) {
      ROS_ERROR("[AffordanceTemplate::planRequest] problem computing group sequence for group: %s!!", ee.c_str());
      planning.progress = -1;
      planning_server_.publishFeedback(planning);
      result.succeeded = false;
      result.steps = 0;
      planning_server_.setSucceeded(result);
      m_plan_.unlock();
      return;
    }
  }
 
  // make sure we only go as many steps until the first EE change across all manipulators
  int num_steps = 100000;
  for (auto ee : planning_groups) {
    if(plan_status_[goal->trajectory][ee].sequence_ids.size() > 0) {
      num_steps = fmin(num_steps, plan_status_[goal->trajectory][ee].sequence_ids.size());
    }
  }
  ROS_WARN("AffordanceTemplate::planRequest() -- truncated number of steps to plan to %d cause that is the first time any of the EEs change", num_steps);
  for (auto ee : planning_groups) {
    if(plan_status_[goal->trajectory][ee].sequence_ids.size() > num_steps) {
      plan_status_[goal->trajectory][ee].sequence_ids.erase(plan_status_[goal->trajectory][ee].sequence_ids.begin(), plan_status_[goal->trajectory][ee].sequence_ids.begin()+num_steps);
      plan_status_[goal->trajectory][ee].goal_idx = plan_status_[goal->trajectory][ee].sequence_ids.back();
    }
    ROS_INFO("AffordanceTemplate::planRequest() -- SETTING GOAL IDX: %d", plan_status_[goal->trajectory][ee].goal_idx);
  }


  // go get the goals for each of the manipulators
  for (auto ee : planning_groups) {
    if(!createGoalsFromSequence(ee, goal->trajectory, plan_status_[goal->trajectory][ee].sequence_ids, planning_goals[robot_interface_->getManipulator(ee)])) {
      ROS_ERROR("[AffordanceTemplate::planRequest] problem creating goals for %s!!", ee.c_str());
      planning.progress = -1;
      planning_server_.publishFeedback(planning);
      result.succeeded = false;
      result.steps = 0;
      planning_server_.setSucceeded(result);
      m_plan_.unlock();
      return;
    } else {
      planning.progress+=num_steps;
      planning_server_.publishFeedback(planning);
     
      // // keep track of how many times our seq ID has popped up used to create "loops" of AT steps
      // if (loop_seq_map.find(plan_seq) == loop_seq_map.end())
      //   loop_seq_map[plan_seq] = 0;
      // else 
      //   loop_seq_map[plan_seq]+=num_steps;
      // ROS_WARN("[AffordanceTemplate::planRequest] loop_seq_map for plan_seq[%d] is now %d", plan_seq, loop_seq_map[plan_seq]);

      // copy results to the plan status data store
      for(auto pg : planning_goals[robot_interface_->getManipulator(ee)]) {
        plan_status_[goal->trajectory][ee].sequence_poses.push_back(pg.goal);
      }
    }
  }

  // set start states
  sensor_msgs::JointState manipulator_state;
  sensor_msgs::JointState gripper_state;
  for (auto ee : planning_groups) {
    if (gripper_state.position.size()) 
      group_start_states[ee] = gripper_state;
    if (manipulator_state.position.size()) 
      group_start_states[robot_interface_->getManipulator(ee)] = manipulator_state;
  }

  // do plan
  if (robot_interface_->getPlanner()->plan(planning_goals, trajectories, false, false, group_start_states)) {

    int plan_seq;
    ROS_INFO("[AffordanceTemplate::planRequest] planning for %d steps succeeded", num_steps);

    planning.progress += num_steps;
    planning_server_.publishFeedback(planning);

    for (auto ee : planning_groups) {

      std::string manipulator_name = robot_interface_->getManipulator(ee);
      ROS_WARN("[AffordanceTemplate::planRequest] decomposing trajectory for %s (%s)", manipulator_name.c_str(), ee.c_str());

      plan_status_[goal->trajectory][ee].plan_valid = true;

      if (trajectories.find(manipulator_name) != trajectories.end()) {

        int id  = plan_status_[goal->trajectory][ee].sequence_ids.back();
        ROS_WARN("[AffordanceTemplate::planRequest] id: %d, for ee: %s", id, ee.c_str());
        plan_seq = plan_status_[goal->trajectory][ee].sequence_ids[id]; // this is probably not right!!!!
        ROS_WARN("[AffordanceTemplate::planRequest] plan_seq: %d", plan_seq);

        // pair< WP index, loop index>, pair< group, WP trajectory>
        // control_trajectories_.push_back( std::make_pair( std::make_pair( plan_seq, loop_seq_map[plan_seq]), std::make_pair( manipulator_name, trajectories[manipulator_name])));
        control_trajectories_.push_back( std::make_pair( std::make_pair( plan_seq, 0), std::make_pair( manipulator_name, trajectories[manipulator_name])));
        
        // set the start state for ee planning
        manipulator_state.header = trajectories[manipulator_name].header;
        manipulator_state.name = trajectories[manipulator_name].joint_names;
        
        if (trajectories[manipulator_name].points.size()) {
          manipulator_state.position = trajectories[manipulator_name].points.back().positions;
          manipulator_state.velocity = trajectories[manipulator_name].points.back().velocities;
          manipulator_state.effort = trajectories[manipulator_name].points.back().effort;
          group_start_states[manipulator_name] = manipulator_state;
        } else {
          ROS_ERROR("[AffordanceTemplate::planRequest] the resulting plan generated 0 joint trajectory points!!");
        }

      } else {
        ROS_ERROR("[AffordanceTemplate::planRequest] planning succeeded but did not get a trajectory for %s", manipulator_name.c_str());
      }
    }


    // go through and get the plans for the EEs
    for (auto ee : planning_groups) {
    
      std::vector<affordance_template_object::EndEffectorWaypoint> wp_vec;
      // get list of EE pose names related to ID
      std::map<int, std::string> ee_pose_map = robot_interface_->getEEPoseNameMap(ee);

      int id  = plan_status_[goal->trajectory][ee].sequence_ids.back();
      plan_seq = plan_status_[goal->trajectory][ee].sequence_ids.back();
      if(!getTrajectoryEEPoseList(goal->trajectory, ee, wp_vec)) {
        ROS_ERROR("AffordanceTemplate::planRequest() -- problem getting waypoint list for end effector %s in %s", ee.c_str(), goal->trajectory.c_str());
        return;
      }

      // find and add EE joint state to goal
      if (ee_pose_map.find(wp_vec[plan_seq].ee_pose) == ee_pose_map.end()) {
        ROS_WARN("[AffordanceTemplate::planRequest] couldn't find EE Pose ID %d in robot interface map!!", plan_seq);
      } else {
        planning.progress+=num_steps;
        planning_server_.publishFeedback(planning);

        ROS_DEBUG("[AffordanceTemplate::planRequest] setting EE goal pose to %s", ee_pose_map[wp_vec[plan_seq].ee_pose].c_str());
        sensor_msgs::JointState ee_js;
        try {
          if (!robot_interface_->getPlanner()->getRDFModel()->getGroupState( ee, ee_pose_map[wp_vec[plan_seq].ee_pose], ee_js)) { // this is the reason for the try{} block, TODO should put null pointer detection in
            ROS_ERROR("[AffordanceTemplate::planRequest] couldn't get group state!!");
            planning.progress = -1;
            planning_server_.publishFeedback(planning);
            result.succeeded = false;
            result.steps = 0;
            planning_server_.setSucceeded(result);
            m_plan_.unlock();
            return;
          } else {
            planning.progress+=num_steps;
            planning_server_.publishFeedback(planning);
            std::map<std::string, std::vector<sensor_msgs::JointState> > ee_goals;
            ee_goals[ee].push_back(ee_js);
            trajectories.clear();
            if (!robot_interface_->getPlanner()->planJointPath( ee_goals, trajectories, false, false, scale_ee_speed_, group_start_states)) {
              ROS_ERROR("[AffordanceTemplate::planRequest] couldn't plan for end effector joint states!!");
              planning.progress = -1;
              planning_server_.publishFeedback(planning);
              result.succeeded = false;
              result.steps = 0;
              planning_server_.setSucceeded(result);
              m_plan_.unlock();
              return;
            }

            if (trajectories.find(ee) != trajectories.end()) {
              // control_trajectories_.push_back( std::make_pair( std::make_pair( plan_seq, loop_seq_map[plan_seq]), std::make_pair( ee, trajectories[ee])));
              control_trajectories_.push_back( std::make_pair( std::make_pair( plan_seq, 0), std::make_pair( ee, trajectories[ee])));
              
              // set the start state for ee planning
              gripper_state.header = trajectories[ee].header;
              gripper_state.name = trajectories[ee].joint_names;
              if (trajectories[ee].points.size()) {
                gripper_state.position = trajectories[ee].points.back().positions;
                gripper_state.velocity = trajectories[ee].points.back().velocities;
                gripper_state.effort = trajectories[ee].points.back().effort;
                group_start_states[ee] = gripper_state;
              } else {
                ROS_ERROR("[AffordanceTemplate::planRequest] the resulting plan generated 0 joint trajectory points!!");
              }
            } else {
              ROS_ERROR("[AffordanceTemplate::planRequest] planning succeeded but did not get a trajectory for %s", ee.c_str());
            }
          }
        } catch(...) {
          ROS_FATAL("[AffordanceTemplate::planRequest] couldn't get planner or RDF model -- bad pointer somewhere!!");
          planning.progress = -1;
          planning_server_.publishFeedback(planning);
          result.succeeded = false;
          result.steps = 0;
          planning_server_.setSucceeded(result);
          m_plan_.unlock();
          return;
        }
      }
    }


  } else {
    ROS_ERROR("[AffordanceTemplate::planRequest] planning failed for %d steps", num_steps);
    planning.progress = -1;
    planning_server_.publishFeedback(planning);
    result.succeeded = false;
    result.steps = 0;
    planning_server_.setSucceeded(result);
    m_plan_.unlock();
    return;
  }

  if (autoplay_display_)
    robot_interface_->getPlanner()->playAnimation();

  if (goal->execute) {
    ROS_INFO("[AffordanceTemplate::planRequest] planning complete. executing generated plans!");
    
    std::vector<std::string> valid_group;
    for (auto ee : goal->groups)
      if ( plan_status_[goal->trajectory][ee].plan_valid) 
        valid_group.push_back(ee);
    
    if (!moveToTrajectory(valid_group)) {
      ROS_ERROR("[AffordanceTemplate::planRequest] execution of planned trajectories failed!!");
      planning.progress = -1;
      planning_server_.publishFeedback(planning);
      result.succeeded = false;
      result.steps = 0;
      planning_server_.setSucceeded(result);
      m_plan_.unlock();
      return;
    }

    ros::Duration(0.01).sleep(); 
    robot_interface_->getPlanner()->resetAnimation(true);
  }

  ROS_INFO("[AffordanceTemplate::planRequest] finished request");
    
  planning.progress+=num_steps;
  planning_server_.publishFeedback(planning);
  
  result.succeeded = true;
  result.steps = num_steps;
  planning_server_.setSucceeded(result);
  m_plan_.unlock();

  ROS_INFO("[AffordanceTemplate::planRequest] returning");
}


void AffordanceTemplate::executeRequest(const ExecuteGoalConstPtr& goal)
{
  m_plan_.lock();

  ROS_INFO("[AffordanceTemplate::executeRequest] request to execute any stored plans...");

  ExecuteResult result;
  result.succeeded = true;
  ExecuteFeedback exe;

  exe.progress = 1;
  execution_server_.publishFeedback(exe);
  
  std::vector<std::string> valid_group;
  for (auto ee : goal->groups)
    if ( plan_status_[goal->trajectory][ee].plan_valid)
      valid_group.push_back(ee);
  
  if (valid_group.size()) {
    if (!moveToTrajectory(valid_group)) {
      ROS_ERROR("[AffordanceTemplate::executeRequest] execution of planned trajectories failed!!");
      exe.progress = -1;
      execution_server_.publishFeedback(exe);
      result.succeeded = false;
      execution_server_.setSucceeded(result);
    }
  }

  execution_server_.setSucceeded(result);

  robot_interface_->getPlanner()->resetAnimation(true);
  m_plan_.unlock();
}

bool AffordanceTemplate::moveToTrajectory(const std::vector<std::string>& groups)
{
  ros::Time st;
  std::string current_trajectory = current_trajectory_;
  std::vector<std::pair<int, int> > exe_seq;
  for (auto traj : control_trajectories_) {
    if( std::find(exe_seq.begin(), exe_seq.end(), std::make_pair(traj.first.second, traj.first.first)) == exe_seq.end())
      exe_seq.push_back(std::make_pair(traj.first.second, traj.first.first));
  }

  for (auto g : groups) {   
    if (!plan_status_[current_trajectory][g].plan_valid) {
      plan_status_[current_trajectory][g].exec_valid = false;
      ROS_ERROR("[AffordanceTemplate::moveToTrajectory] EE %s in trajectory %s doesn't have a valid plan!!", g.c_str(), current_trajectory.c_str());
      return false;
    }
  }

  std::vector<std::pair<std::string, trajectory_msgs::JointTrajectory> > trajectories;
  for (auto seq : exe_seq) {
  
    //    m_plan_.unlock();

    for (auto traj : control_trajectories_)
      if (traj.first.first == seq.second && traj.first.second == seq.first)
        trajectories.push_back(traj.second);

    st = ros::Time::now();
    if (!robot_interface_->getPlanner()->executePlans(trajectories)) {
      ROS_ERROR("[AffordanceTemplate::moveToTrajectory] execution failed");
      control_trajectories_.clear();
      //m_plan_.lock();
      return false;
    }
    ROS_WARN("AT execute plans took: %f", (ros::Time::now()-st).toSec());
    //m_plan_.lock();
    
    std::vector<std::string> traj_groups;
    for (auto t : trajectories)
      traj_groups.push_back(t.first);

    for (auto g : groups) {
      if (std::find(traj_groups.begin(), traj_groups.end(), g) != traj_groups.end()) {
        plan_status_[current_trajectory][g].current_idx = seq.second;
        ROS_INFO("[AffordanceTemplate::moveToTrajectory] %s's reached waypoint %d succeessfully", g.c_str(), plan_status_[current_trajectory][g].current_idx);
      }
    }

    trajectories.clear();
  }

  for (auto g : groups) {
    plan_status_[current_trajectory][g].plan_valid = false;
    plan_status_[current_trajectory][g].exec_valid = true;
  }

  // buildTemplate(current_trajectory);  
  control_trajectories_.clear();
  ROS_DEBUG("AT moveToTrajectory() done");
  return true;
}


// list of ee waypoints to move to, return true if all waypoints were valid
bool AffordanceTemplate::moveToWaypoints(const std::vector<std::string>& ee_names) 
{
  m_plan_.lock();
  std::string current_trajectory = current_trajectory_;

  std::vector<std::string> valid_ee_plans;
  std::vector<std::string> m_names;
  for(auto ee: ee_names) {
    if (plan_status_[current_trajectory][ee].plan_valid) {
      valid_ee_plans.push_back(ee);
      m_names.push_back(robot_interface_->getManipulator(ee));
    } else {
      plan_status_[current_trajectory][ee].exec_valid = false;
    }
  }
  if(robot_interface_->getPlanner()->executePlans(m_names)) {
    ROS_INFO("AffordanceTemplate::moveToWaypoints() -- execution succeeded");
    for(auto ee: valid_ee_plans) {
      plan_status_[current_trajectory][ee].current_idx = plan_status_[current_trajectory][ee].goal_idx;
      plan_status_[current_trajectory][ee].plan_valid = false;
      plan_status_[current_trajectory][ee].exec_valid = true;
    }
    m_plan_.unlock();
    return true;
  }
  ROS_WARN("AffordanceTemplate::moveToWaypoints() -- execution failed");
  m_plan_.unlock();
  return false;
}

bool AffordanceTemplate::getPoseFromFrameStore(const std::string &frame, geometry_msgs::PoseStamped &ps) 
{
  bool ret = false;
  mutex_.lock();
  auto search = frame_store_.find(frame);
  if(search != frame_store_.end()) {
    ps = frame_store_[frame].second;
    ret = true;
  }
  mutex_.unlock();
  return ret;
}

void AffordanceTemplate::run()
{
  ros::Rate loop_rate(loop_rate_);
  tf::Transform transform;
  FrameInfo fi;
  ros::Time t;
  ros::Time start_t = ros::Time::now();
  bool applied = false;


  ROS_DEBUG("AffordanceTemplate::run() -- spinning...");
  while(running_ && ros::ok()) {

    for(auto f: frame_store_)  {
      if (!build_request || updated_frames) {
        mutex_.lock();
        fi = f.second;
        mutex_.unlock();
        fi.second.header.stamp = ros::Time::now();
        tf::poseMsgToTF(fi.second.pose, transform);
        tf_broadcaster_.sendTransform(tf::StampedTransform(transform, fi.second.header.stamp, fi.second.header.frame_id, fi.first));
      }
      else ROS_WARN_THROTTLE(1.0,"AffordanceTemplate::run() -- Waiting on template to be built.");
    }
    loop_rate.sleep();
  }
  ROS_DEBUG("AffordanceTemplate::run() -- leaving spin thread. template must be shutting down...");
}

void AffordanceTemplate::stop()
{
  ROS_INFO("[AffordanceTemplate::stop] %s being asked to stop..", name_.c_str());
  running_ = false;
  mutex_.lock();
  frame_store_.clear();
  mutex_.unlock();
  removeAllMarkers();
}

bool AffordanceTemplate::setObjectScaling(const std::string& key, double scale_factor, double ee_scale_factor)
{ 
  ROS_DEBUG("[AffordanceTemplate::setObjectScaling] setting object %s scaling to %g, %g", key.c_str(), scale_factor, ee_scale_factor);
  object_scale_factor_[key] = scale_factor;
  ee_scale_factor_[key] = ee_scale_factor;
  removeInteractiveMarker(key);
  return buildTemplate();
}

bool AffordanceTemplate::setObjectPose(const DisplayObjectInfo& obj)
{
  ROS_INFO("[AffordanceTemplate::setObjectPose] setting pose for object %s in template %s:%d", obj.name.c_str(), obj.type.c_str(), obj.id);
  
  for (auto& d : structure_.display_objects) {
    std::string obj_name = obj.name + ":" + std::to_string(obj.id);
    if (d.name == obj_name) {
      ROS_DEBUG("[AffordanceTemplate::setObjectPose] matched object %s in frame: %s", obj_name.c_str(), obj.stamped_pose.header.frame_id.c_str());
      geometry_msgs::PoseStamped ps;
      try {
        if(frame_store_[obj_name].second.header.frame_id != obj.stamped_pose.header.frame_id) {
          tf_listener_.waitForTransform(frame_store_[obj_name].second.header.frame_id, obj.stamped_pose.header.frame_id, obj.stamped_pose.header.stamp, ros::Duration(3.0));
          tf_listener_.transformPose(frame_store_[obj_name].second.header.frame_id, obj.stamped_pose, ps);
        } else {
          ps = obj.stamped_pose;
        }
        setFrame(obj_name,ps);
        server_->setPose(obj_name, ps.pose);
        server_->applyChanges();
        updatePoseInStructure(obj_name, ps.pose);
      } catch(tf::TransformException ex) {
        ROS_ERROR("[AffordanceTemplate::setObjectPose] trouble transforming pose from %s to %s. TransformException: %s",frame_store_[obj_name].second.header.frame_id.c_str(), obj.stamped_pose.header.frame_id.c_str(), ex.what());
        return false;
      }
      break;
    }
  }
  return true;
}
