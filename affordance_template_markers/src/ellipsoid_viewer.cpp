/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/

#include <affordance_template_markers/ellipsoid_viewer.h>
#include <Eigen/Dense>


EllipsoidViewer::EllipsoidViewer(ros::NodeHandle &n, std::string topic_name, bool left_arm)
{
  TRAC_IK::URDFParser parser("robot_description");
  left_arm_ = left_arm;
  KDL::Chain chain;
  // these should be loaded in from a rosparam
  chain_start_ = "r2/robot_base";
  std::string chain_end = "r2/right_palm";
  if(left_arm)
    chain_end = "r2/left_palm";
  boost::shared_ptr<TRAC_IK::JointGroup> group = parser.chainGroup(chain_start_, chain_end);
  bool valid = group->chain_solver->getKDLChain(chain);
  jacsolver_.reset(new KDL::ChainJntToJacSolver(chain));
  arr_.resize(chain.getNrOfJoints());
  ellipsoid_pub_ =  n.advertise<visualization_msgs::Marker>("/ellipsoid", 1);
  fk_pos_solver_.reset(new KDL::ChainFkSolverPos_recursive(chain));
  // fk_pos_solver_ = boost::shared_ptr<KDL::ChainFkSolverPos_recursive>(new KDL::ChainFkSolverPos_recursive(chain));
  joint_sub_ =  n.subscribe(topic_name, 1, &EllipsoidViewer::jointStateCallback, this);
  ROS_INFO_STREAM("Started ellipsoid visualizer!");


}

void EllipsoidViewer::visualize(const KDL::JntArray& arr, const KDL::Frame& pose) {
  KDL::Jacobian jac(arr.data.size());
  jacsolver_->JntToJac(arr,jac);

  Eigen::JacobiSVD<Eigen::MatrixXd> svdsolver(jac.data, Eigen::ComputeThinU | Eigen::ComputeThinV);
  // Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(jac.data*jac.data.transpose());

  // the eignenvectors of U correspond to the eigenvectors of JJ^T
  Eigen::MatrixXd  eigenVecs = svdsolver.matrixU();
  // Eigen::MatrixXd  eigenVecs2 = eigensolver.eigenvectors();
  // for(unsigned int i=0; i < 6; ++i)
  // {
  //   for(unsigned int j=0; j < 6; ++j)
  //   {
  //   ROS_INFO_STREAM("eigenVecs sing " << eigenVecs(j,i) << " eigenvec " << eigenVecs2(j,i));
  //   }
  //   ROS_INFO_STREAM("----");
  //
  // }

  // these are the square roots of non-zero eigenvalues of JJ^T, in descending order
  Eigen::MatrixXd singular_values = svdsolver.singularValues();
  // Eigen::MatrixXd eig_values = eigensolver.eigenvalues();
  // for(unsigned int i=0; i < singular_values.rows(); ++i)
  //   ROS_INFO_STREAM("sqrt eigenvals sing " << singular_values(i,0) << " sqrt eigenval " << sqrt(eig_values(i,0)));




  visualization_msgs::Marker marker;
  marker.header.frame_id = chain_start_;
  marker.header.stamp = ros::Time::now();
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = pose.p.x(); // ee point really
  marker.pose.position.y = pose.p.y(); //
  marker.pose.position.z = pose.p.z(); //
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  // axes length---use eigenvectors and eigenvalues to get coords
  marker.scale.x = 0;
  marker.scale.y = 0;
  marker.scale.z = 0;
  double r = 0;
  double p = 0;
  double y = 0;
  double scale =1;
  for(unsigned int i=0; i < 6; ++i)
  {
    scale *= singular_values(i,0);
  }
  // from http://answers.ros.org/question/11081/plot-a-gaussian-3d-representation-with-markers-in-rviz/
  Eigen::Matrix3d rot;
  rot << eigenVecs(0,1),eigenVecs(0,2),eigenVecs(0,3), eigenVecs(1,1),eigenVecs(1,2),eigenVecs(1,3), eigenVecs(2,1),eigenVecs(2,2),eigenVecs(2,3);
  Eigen::Quaterniond q_;
  q_ = rot;

  marker.pose.orientation.x = q_.x();
  marker.pose.orientation.y = q_.y();
  marker.pose.orientation.z = q_.z();
  marker.pose.orientation.w = q_.w();

  // scale the ellipsoid
  marker.scale.x = 1/singular_values(0,0);
  marker.scale.y = 1/singular_values(1,0);
  marker.scale.z = 1/singular_values(2,0);


  marker.color.r = 1.0;
  marker.color.g = 1.0;
  marker.color.b = 1.0;
  marker.color.a = 0.5;
  ellipsoid_pub_.publish(marker);


}

void EllipsoidViewer::jointStateCallback(const sensor_msgs::JointState& state)
{
  // might be a better way of doing this, but for now
  // build keyed
  // ROS_INFO("GOT AN UPDATE!");
  arr_(0) = state.position[0];

  if(left_arm_)
  {
    // fill out the jntarray with the current state values. being lazy for now
    arr_(1) = state.position[4];
    arr_(2) = state.position[5];
    arr_(3) = state.position[6];
    arr_(4) = state.position[7];
    arr_(5) = state.position[8];
    arr_(6) = state.position[9];
    arr_(7) = state.position[10];
  }
  else
  {

  }

}

void EllipsoidViewer::run()
{

  // compute end effector position via fk
  KDL::Frame end_effector_pose;
  fk_pos_solver_->JntToCart(arr_, end_effector_pose);
  visualize(arr_, end_effector_pose);

  //

}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "EllipsoidViewer");
  ros::NodeHandle n;
  std::string topic = "/ats/joint_states";
  bool left_arm = true;
  EllipsoidViewer node(n, topic, left_arm);
  ros::Rate loop_rate(20);

  while (ros::ok())
  {
    ros::spinOnce();
    node.run();
    loop_rate.sleep();
  }
  return 0;
}
