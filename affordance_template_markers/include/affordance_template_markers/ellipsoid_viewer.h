/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/
 #ifndef _ELLIPSOID_VIEWER_H_
 #define _ELLIPSOID_VIEWER_H_
 #include <ros/ros.h>
 #include <tf/transform_listener.h>
 #include <sensor_msgs/JointState.h>
 #include <geometry_msgs/Pose.h>
 #include <kdl/chainjnttojacsolver.hpp>
 #include <kdl/chainjnttojacsolver.hpp>
 #include <kdl/chain.hpp>
 #include <kdl/jntarray.hpp>
 #include <boost/shared_ptr.hpp>
 #include <Eigen/Geometry>

 #include <visualization_msgs/Marker.h>
 #include <trac_ik/urdf_parser.hpp>


class EllipsoidViewer
{
public:
  EllipsoidViewer(ros::NodeHandle &n, std::string topic_name, bool left_arm);
  ~EllipsoidViewer(){}

  void jointStateCallback(const sensor_msgs::JointState&);
  void visualize(const KDL::JntArray&, const KDL::Frame& );
  void run();

protected:
  ros::Publisher ellipsoid_pub_;
  boost::shared_ptr<KDL::ChainJntToJacSolver> jacsolver_;
  bool left_arm_;
  KDL::JntArray arr_;
  std::string chain_start_;
  boost::shared_ptr<KDL::ChainFkSolverPos_recursive> fk_pos_solver_;
  ros::Subscriber joint_sub_;

};

 #endif
