#! /usr/bin/env python

import roslib; roslib.load_manifest('affordance_template_server')
import rospy

from geometry_msgs.msg import PoseStamped

from affordance_template_msgs.msg import *
from affordance_template_msgs.srv import *

from random import randint
from random import random

if __name__ == '__main__':
  rospy.init_node('reset_test')

  rospy.loginfo("Waiting to connect to servers...")

  resetTemplate = rospy.ServiceProxy("affordance_template_server/reset_template", ResetAffordanceTemplate)
  setPose = rospy.ServiceProxy("affordance_template_server/set_object_pose", SetObjectPose)

  rospy.loginfo("Connected to all servers!")

  cnt = 0
  connected = True
  while not rospy.is_shutdown() and connected is True:
    try:

      objs = DisplayObjectInfo()
      objs.type = "Wheel"
      objs.name = "wheel"
      objs.id = 0

      stamped = PoseStamped()
      stamped.header.stamp = rospy.Time()
      stamped.header.frame_id = "r2/robot_world"
      stamped.pose.position.x = random()
      stamped.pose.position.y = random()
      stamped.pose.position.z = random()
      stamped.pose.orientation.x = 0
      stamped.pose.orientation.y = 0
      stamped.pose.orientation.z = 0
      stamped.pose.orientation.w = 1
      
      objs.stamped_pose = stamped    

      objReq = SetObjectPoseRequest()
      objReq.objects.append(objs)

      objRes = SetObjectPoseResponse()
      objRes = setPose(objReq)
      
      res = ResetAffordanceTemplateResponse()
      req = ResetAffordanceTemplateRequest()
      req.class_type = "Wheel"
      req.id = 0
      res = resetTemplate(req)

      cnt += 1

      if res.status : 
        rospy.loginfo("Reset #%d OK", cnt)
      else:
        rospy.logerr("Reset #%d FAILED", cnt)
        connected = False

    except rospy.ServiceException, e:
      rospy.logerr("service call failed: %s"%e)   
      connected = False
