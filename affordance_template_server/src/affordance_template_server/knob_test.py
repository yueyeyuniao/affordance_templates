#! /usr/bin/env python

import roslib; roslib.load_manifest('affordance_template_server')
import rospy

from affordance_template_msgs.msg import *
from affordance_template_msgs.srv import *

from random import randint
from random import random

if __name__ == '__main__':
  rospy.init_node('knob_test')

  rospy.loginfo("Waiting to connect to servers...")

  addTemplate = rospy.ServiceProxy("affordance_template_server/add_template", AddAffordanceTemplate)
  delTemplate = rospy.ServiceProxy("affordance_template_server/delete_template", DeleteAffordanceTemplate)
  setPose = rospy.ServiceProxy("affordance_template_server/set_object_pose", SetObjectPose)
  setTraj = rospy.ServiceProxy("affordance_template_server/set_template_trajectory", SetAffordanceTemplateTrajectory)

  rospy.loginfo("Connected to all servers!")

  addRes = AddAffordanceTemplateRequest()

  ids = []

  numTemplates = randint(20, 60)
  rospy.loginfo("will add %s ATs!!!"%str(numTemplates))

  for n in range(0, numTemplates) :
    try:
      # rospy.loginfo("adding template")
      req = AddAffordanceTemplateRequest()
      req.class_type = "Knob"
      req.pose.header.frame_id = "world"
      req.pose.pose.position.x = random()
      req.pose.pose.position.y = random()
      req.pose.pose.position.z = random()
      req.pose.pose.orientation.w = 1.0
      addRes = addTemplate(req)
      # rospy.logwarn("should have added template! id: " + str(addRes.id))
      ids.append(addRes.id)
    except rospy.ServiceException, e:
      rospy.logerr("service call failed: %s"%e)   
    # rospy.sleep(0.1)

  rospy.sleep(1)
  rospy.loginfo("now deleting %s templates"%str(len(ids)))
  rospy.sleep(1)

  for i in ids :
    try:
      req = DeleteAffordanceTemplateRequest()
      req.class_type = "Knob"
      req.id = i

      res = DeleteAffordanceTemplateResponse()
      res = delTemplate(req)

      if res.status : 
        rospy.loginfo("deleted id "+str(i))
      else:
        rospy.logerr("couldn't delete id "+str(i))
    except rospy.ServiceException, e:
      rospy.logerr("service call failed: %s"%e)   
