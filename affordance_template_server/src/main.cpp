#include <affordance_template_server/interface.h>

int main(int argc, char **argv)
{

  ros::init(argc, argv, "affordance_template_server_node");
  ros::AsyncSpinner spinner(50);
  ros::NodeHandle pnh("~");
  ros::NodeHandle nh;
  std::string robot_name = "";
  std::string affordance_template_library = "";

  pnh.getParam("robot_config", robot_name);
  pnh.getParam("affordance_template_library", affordance_template_library);

  ROS_INFO("ROBOT NAME: %s", robot_name.c_str());
  if (robot_name.empty())
    robot_name = "r2_roscontrol.yaml";
  ROS_INFO("AT LIBRARY: %s", affordance_template_library.c_str());
  if (affordance_template_library.empty())
    affordance_template_library = "affordance_template_library";

  ROS_INFO("main() -- nh namespace: %s", nh.getNamespace().c_str());
  affordance_template_server::AffordanceTemplateInterface ati(nh, robot_name, affordance_template_library);

  spinner.start();
  ros::waitForShutdown();
  
  ROS_INFO("Success!!");
  
  return 0;
}