#include <affordance_template_server/server.h>

using namespace affordance_template_server;

AffordanceTemplateServer::AffordanceTemplateServer(const ros::NodeHandle &nh, const std::string &robot_yaml="", const std::string &library_pkg="affordance_template_library") :
    nh_(nh),
    robot_yaml_(robot_yaml),
    library_pkg_(library_pkg)
{
    
    if (robot_yaml_.empty()) {
        ROS_WARN("[AffordanceTemplateServer] no robot yaml provided - BE SURE TO LOAD ROBOT FROM SERVICE!!");
    } else {
        if (!loadRobot())
            ROS_ERROR("[AffordanceTemplateServer] couldn't parse robot .yamls!!");
    }

    im_server_.reset( new interactive_markers::InteractiveMarkerServer(std::string("affordance_template_interactive_marker_server"), "", false));

    if (!loadTemplates())
        ROS_ERROR("[AffordanceTemplateServer] couldn't parse robot JSONs!!");

    ROS_INFO("[AffordanceTemplateServer] server configured. spinning...");
}

/**
 * @brief parse robot JSONs
 * @details finds any and all JSON files in the AT Library 'templates' directory 
 *  and parses into the map at_structure_map_ 
 * 
 * @return false if can't find or load the directory, true otherwise
 */
bool AffordanceTemplateServer::loadTemplates()
{
    ROS_INFO("[AffordanceTemplateServer::loadTemplates] searching for JSON templates in package: %s", library_pkg_.c_str());

    boost::mutex::scoped_lock lock(structure_map_mutex_);

    std::string root = getPackagePath(library_pkg_);
    if (root.empty())
        return false;
    
    // clear what we have, start over
    at_structure_map_.clear();

    root += "/templates";
    if (!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root)) {
        ROS_WARN("[AffordanceTemplateServer::loadTemplates] cannot find templates in path %s!!", root.c_str());
        return false;
    }

    // use boost to get all of the files with .yaml extension
    std::map<std::string, std::string> template_paths;
    boost::filesystem::recursive_directory_iterator dir_it(root);

    while (dir_it != boost::filesystem::recursive_directory_iterator()) {
        if (dir_it->path().extension() == ".json") {
            
            boost::char_separator<char> sep("/");
            boost::tokenizer<boost::char_separator<char>> tokens(dir_it->path().string(), sep);
            
            std::string name = "";
            for (auto& t : tokens)
                name = t;

            if (name.empty())
                continue;

            ROS_INFO("[AffordanceTemplateServer::loadTemplates] found template name: %s", name.c_str());
            template_paths[name] = dir_it->path().string();
        }
        ++dir_it;
    }

    // make robot instances with the .yamls we just found
    affordance_template_object::AffordanceTemplateParser atp;
    for (auto& t : template_paths) {
        affordance_template_object::AffordanceTemplateStructure at;
        if (atp.loadFromFile(t.second, at))
            at_structure_map_[at.name] = at;
    }

    return true;
}

bool AffordanceTemplateServer::loadRobot()
{
    ROS_INFO("[AffordanceTemplateServer::loadRobot] loading robot yaml %s from path: %s", robot_yaml_.c_str(), library_pkg_.c_str());

    robot_name_ = "";

    std::string root = getPackagePath(library_pkg_);
    if (root.empty())
        return false;

    root += "/robots/";

    if (!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root))
    {
        ROS_WARN("[AffordanceTemplateServer::loadRobot] cannot find robots in path: %s!!", root.c_str());
        return false;
    }

    // make robot instances with the .yamls we just found
    robot_interface_.reset(new affordance_template_markers::RobotInterface(nh_));
    if ( !robot_interface_->load(library_pkg_, root+robot_yaml_))
    {
        ROS_WARN("[AffordanceTemplateServer::loadRobot] robot yaml %s NOT loaded, ignoring.", robot_yaml_.c_str());
        return false;
    }

    if ( !robot_interface_->configure())
    {
        ROS_WARN("[AffordanceTemplateServer::loadRobot] robot yaml %s NOT configured, ignoring.", robot_yaml_.c_str());
        return false;
    }        

    robot_config_ = robot_interface_->getRobotConfig();
    robot_name_ = robot_config_.name;
    if (getPackagePath(robot_config_.config_package).empty())
        ROS_WARN("[AffordanceTemplateServer::loadRobot] config package %s NOT found, ignoring.", robot_config_.config_package.c_str());
    else
        ROS_INFO("[AffordanceTemplateServer::loadRobot] config package %s found", robot_config_.config_package.c_str());

    return true;
}

std::string AffordanceTemplateServer::getPackagePath(const std::string &pkg_name)
{
    ROS_INFO("[AffordanceTemplateServer::getPackagePath] finding path for package %s.......", pkg_name.c_str());
    std::string path = ros::package::getPath(pkg_name); // will be empty if not found
    if (path.empty())
        ROS_WARN("[AffordanceTemplateServer::getPackagePath] couldn't find path to package %s!!", pkg_name.c_str());
    else
        ROS_INFO("[AffordanceTemplateServer::getPackagePath] found path: %s", path.c_str());
    return path;
}

int AffordanceTemplateServer::getNextID(const std::string &type)
{
    std::vector<int> ids;
    for (auto at : at_map_) {
        if (at.second->getType() == type)
            ids.push_back(at.second->getID());
    }

    int next = 0;
    while(1) {
        if (std::find(ids.begin(), ids.end(), next) == ids.end())
            return next;
        else 
            ++next;
    }

    return next;
}

//################
// public methods
//################

std::vector<affordance_template_msgs::AffordanceTemplateConfig> AffordanceTemplateServer::getAvailableTemplates(const std::string &name)
{
    std::vector<affordance_template_msgs::AffordanceTemplateConfig> templates;

    boost::mutex::scoped_lock lock(structure_map_mutex_);

    if (!name.empty()) {
        
        affordance_template_msgs::AffordanceTemplateConfig atc;
        atc.filename = at_structure_map_[name].filename;
        atc.type = at_structure_map_[name].name;
        atc.image_path = at_structure_map_[name].image;
        
        for (auto ee : at_structure_map_[name].ee_trajectories) {
        
            affordance_template_msgs::WaypointTrajectory wp;
            wp.name = ee.name;
        
            for (int w = 0; w < ee.ee_waypoint_list.size(); ++w) {
                affordance_template_msgs::WaypointInfo wi;
                wi.id = ee.ee_waypoint_list[w].id;
                wi.end_effector_name = robot_interface_->getEENameMap()[wi.id];
                wi.num_waypoints = ee.ee_waypoint_list[w].waypoints.size();
                wp.waypoint_info.push_back(wi);
            }
            atc.trajectory_info.push_back(wp);
        }

        for (auto d : at_structure_map_[name].display_objects)
            atc.display_objects.push_back(d.name);
        templates.push_back(atc);

    } else { 
        for (auto t : at_structure_map_) {
            affordance_template_msgs::AffordanceTemplateConfig atc;
            atc.filename = t.second.filename;
            atc.type = t.second.name;
            atc.image_path = t.second.image;
            
            for (auto ee : t.second.ee_trajectories) {

                affordance_template_msgs::WaypointTrajectory wp;
                wp.name = ee.name;
                
                for (int w = 0; w < ee.ee_waypoint_list.size(); ++w) {
                    affordance_template_msgs::WaypointInfo wi;
                    wi.id = ee.ee_waypoint_list[w].id;
                    wi.end_effector_name = robot_interface_->getEENameMap()[wi.id];
                    wi.num_waypoints = ee.ee_waypoint_list[w].waypoints.size();
                    wp.waypoint_info.push_back(wi);
                }
                atc.trajectory_info.push_back(wp);
            }

            for (auto d : t.second.display_objects)
                atc.display_objects.push_back(d.name);
            templates.push_back(atc);
        }
    }
    
    return templates;
}

std::vector<std::string> AffordanceTemplateServer::getRunningTemplates(const std::string &name)
{
    std::vector<std::string> templates;
    mutex_.lock();
    for (auto a : at_map_)
       templates.push_back(a.first);
    mutex_.unlock();

    return templates;
}

bool AffordanceTemplateServer::loadRobot(const std::string &name="")
{
    if (!name.empty())
        return false;

    mutex_.lock();
    robot_interface_->tearDown();
    mutex_.unlock();

    return robot_interface_->load(library_pkg_, name);
} 

bool AffordanceTemplateServer::loadRobot(const affordance_template_msgs::RobotConfig &msg)
{
    std::string name = msg.name;
    mutex_.lock();
    robot_interface_->tearDown();
    mutex_.unlock();
    return robot_interface_->load(msg);
}

bool AffordanceTemplateServer::resetTemplate(const std::string& type, const uint8_t id)
{
    if (type.empty())
      return false;

    mutex_.lock();
    std::string key = type + ":" + std::to_string(id);
    ROS_INFO("[AffordanceTemplateServer::resetTemplate] creating new affordance template with ID: %d and key: %s", id, key.c_str());
    bool b = at_map_[key]->resetTemplate();
    mutex_.unlock();

    return b;
}


bool AffordanceTemplateServer::addTemplate(const std::string& type, uint8_t& id)
{
    geometry_msgs::PoseStamped pose;
    return addTemplate(type, id, pose);
}

bool AffordanceTemplateServer::addTemplate(const std::string &type, uint8_t& id, geometry_msgs::PoseStamped &pose)
{
    if (type.empty())
      return false;

    mutex_.lock();
    id = getNextID(type);
    std::string key = type + ":" + std::to_string(id);
    ROS_INFO("[AffordanceTemplateServer::addTemplate] creating new affordance template with ID: %d and key: %s", id, key.c_str());

    at_map_[key] = boost::shared_ptr<affordance_template::AffordanceTemplate>(new affordance_template::AffordanceTemplate(nh_, im_server_, robot_interface_, library_pkg_, robot_name_, type, id));
    
    affordance_template_object::AffordanceTemplateStructure structure;
    geometry_msgs::Pose p;

    bool b = at_map_[key]->loadFromFile( at_structure_map_[type].filename, p, structure);
    mutex_.unlock();

    return b;
}

bool AffordanceTemplateServer::removeTemplate(const std::string &type, const uint8_t id)
{
    mutex_.lock();

    std::string key = type + ":" + std::to_string(id);
    if (at_map_.find(key) == at_map_.end()) {
        mutex_.unlock();
        return false;
    }
    at_map_[key]->stop();
    at_map_.erase(key);
    mutex_.unlock();

    robot_interface_->getPlanner()->resetAnimation(true);
    return true;
}

bool AffordanceTemplateServer::updateTemplate(const std::string& type, const uint8_t id, const geometry_msgs::PoseStamped& pose)
{
    mutex_.lock();
    std::string key = type + ":" + std::to_string(id);
    if (at_map_.find(key) == at_map_.end()) {
        mutex_.unlock();
        return false;
    }

    ROS_INFO("[AffordanceTemplateServer::updateTemplate] updating %s to X: %g Y: %g Z: %g, x: %g y: %g z: %g w: %g", key.c_str(), pose.pose.position.x, pose.pose.position.y, pose.pose.position.z, pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w);
    bool r = at_map_[key]->setTemplatePose(pose); 
    mutex_.unlock();
    return r; 
}

bool AffordanceTemplateServer::getTemplateInstance(const std::string &type, const uint8_t id, boost::shared_ptr<affordance_template::AffordanceTemplate> &ati)
{
    mutex_.lock();
    std::string key = type + ":" + std::to_string(id);
    if (at_map_.find(key) == at_map_.end()) {
        mutex_.unlock();
        return false;
    }
    ati = at_map_[key];
    mutex_.unlock();
    return true;
}

bool AffordanceTemplateServer::getTemplateInstance(const std::string &key, boost::shared_ptr<affordance_template::AffordanceTemplate> &ati)
{
    mutex_.lock();
    if (at_map_.find(key) == at_map_.end()) {
        mutex_.unlock();
        return false;
    }
    ati = at_map_[key];
    mutex_.unlock();
    return true;
}
