/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/


#ifndef _AFFORDANCE_TEMPLATE_INTERFACE_H_
#define _AFFORDANCE_TEMPLATE_INTERFACE_H_

#include <affordance_template_server/server.h>
#include <affordance_template_library/affordance_template_structure.h>

#include <affordance_template_msgs/AffordanceTemplateConfigArray.h>
#include <affordance_template_msgs/AffordanceTemplateStatusArray.h>
#include <affordance_template_msgs/GetRobotConfigInfo.h>
#include <affordance_template_msgs/GetAffordanceTemplateConfigInfo.h>
#include <affordance_template_msgs/LoadRobotConfig.h>
#include <affordance_template_msgs/ResetAffordanceTemplate.h>
#include <affordance_template_msgs/AddAffordanceTemplate.h>
#include <affordance_template_msgs/DeleteAffordanceTemplate.h>
#include <affordance_template_msgs/GetRunningAffordanceTemplates.h>
#include <affordance_template_msgs/AffordanceTemplatePlanCommand.h>
#include <affordance_template_msgs/AffordanceTemplateExecuteCommand.h>
#include <affordance_template_msgs/SaveAffordanceTemplate.h>
#include <affordance_template_msgs/AddAffordanceTemplateTrajectory.h>
#include <affordance_template_msgs/ScaleDisplayObject.h>
#include <affordance_template_msgs/ScaleDisplayObjectInfo.h>
#include <affordance_template_msgs/GetAffordanceTemplateStatus.h>
#include <affordance_template_msgs/GetAffordanceTemplateServerStatus.h>
#include <affordance_template_msgs/SetAffordanceTemplateTrajectory.h>
#include <affordance_template_msgs/SetAffordanceTemplatePose.h>
#include <affordance_template_msgs/GetObjectPose.h>
#include <affordance_template_msgs/SetObjectPose.h>
#include <affordance_template_msgs/DisplayObjectInfo.h>
#include <affordance_template_msgs/ObjectInfo.h>
#include <affordance_template_msgs/WaypointInfo.h>
#include <affordance_template_msgs/WaypointViewMode.h>
#include <affordance_template_msgs/SetWaypointViewModes.h>
#include <affordance_template_msgs/ConfigureWaypoint.h>
#include <affordance_template_msgs/WaypointConfig.h>

using namespace affordance_template_msgs;

namespace affordance_template_server
{
    typedef boost::shared_ptr<affordance_template::AffordanceTemplate> ATPointer;

    inline std::string boolToString(bool b) { return (b ? "true" : "false"); }
    inline std::string successToString(bool b) { return (b ? "succeeded" : "failed"); }

    class AffordanceTemplateInterface
    {
        // srv handlers
        bool handleRobotRequest(GetRobotConfigInfo::Request&, GetRobotConfigInfo::Response&);
        bool handleTemplateRequest(GetAffordanceTemplateConfigInfo::Request&, GetAffordanceTemplateConfigInfo::Response&);
        bool handleLoadRobot(LoadRobotConfig::Request&, LoadRobotConfig::Response&);
        bool handleAddTemplate(AddAffordanceTemplate::Request&, AddAffordanceTemplate::Response&);
        bool handleDeleteTemplate(DeleteAffordanceTemplate::Request&, DeleteAffordanceTemplate::Response&);
        bool handleRunning(GetRunningAffordanceTemplates::Request&, GetRunningAffordanceTemplates::Response&);
        bool handlePlanCommand(AffordanceTemplatePlanCommand::Request&, AffordanceTemplatePlanCommand::Response&);
        bool handleExecuteCommand(AffordanceTemplateExecuteCommand::Request&, AffordanceTemplateExecuteCommand::Response&);
        bool handleSaveTemplate(SaveAffordanceTemplate::Request&, SaveAffordanceTemplate::Response&);
        bool handleAddTrajectory(AddAffordanceTemplateTrajectory::Request&, AddAffordanceTemplateTrajectory::Response&);
        bool handleObjectScale(ScaleDisplayObject::Request&, ScaleDisplayObject::Response&);
        bool handleTemplateStatus(GetAffordanceTemplateStatus::Request&, GetAffordanceTemplateStatus::Response&);
        bool handleServerStatus(GetAffordanceTemplateServerStatus::Request&, GetAffordanceTemplateServerStatus::Response&);
        bool handleSetTrajectory(SetAffordanceTemplateTrajectory::Request&, SetAffordanceTemplateTrajectory::Response&);
        bool handleSetPose(SetAffordanceTemplatePose::Request&, SetAffordanceTemplatePose::Response&);
        bool handleSetObject(SetObjectPose::Request&, SetObjectPose::Response&);
        bool handleGetObject(GetObjectPose::Request&, GetObjectPose::Response&);
        bool handleSetWaypointViews(SetWaypointViewModes::Request&, SetWaypointViewModes::Response&);
        bool handleConfigureWaypoint(ConfigureWaypoint::Request&, ConfigureWaypoint::Response&);
        bool handleResetTemplate(ResetAffordanceTemplate::Request &req, ResetAffordanceTemplate::Response &res);

        void runPlanAction();
        void runExecuteAction();
        void runStateThread();
        void handleObjectScaleCallback(const ScaleDisplayObjectInfo&);
        bool doesTrajectoryExist(const ATPointer&, const std::string&);
        bool doesEndEffectorExist(const ATPointer&, const std::string&);
        AffordanceTemplateStatus getTemplateStatus(const std::string& template_name, const int template_id, std::string& traj_name);
        
        bool setupActionClients(const std::string type, const int id);
        bool setupPlanningActionClient(const std::string type, const int id);
        bool setupExecutionActionClient(const std::string type, const int id);
        bool shutdownActionClients(const std::string type, const int id);
        bool shutdownPlanningActionClient(const std::string type, const int id);
        bool shutdownExecutionActionClient(const std::string type, const int id);

        ros::Subscriber scale_stream_sub_;
        boost::shared_ptr<AffordanceTemplateServer> at_server_;
        std::map<std::string, boost::shared_ptr<actionlib::SimpleActionClient<affordance_template_msgs::PlanAction> > > at_plan_clients_;
        std::map<std::string, boost::shared_ptr<actionlib::SimpleActionClient<affordance_template_msgs::ExecuteAction> > > at_exec_clients_;
        tf::TransformListener listener_;
        std::map<std::string, ros::ServiceServer> at_srv_map_;
        ros::Publisher config_pub_, status_pub_;
        ros::NodeHandle nh_;
        std::string library_pkg_;
        std::string robot_yaml_;

        // P&E thread stuffs
        std::deque<affordance_template_msgs::AffordanceTemplatePlanCommand::Request> plan_stack_;
        std::deque<affordance_template_msgs::AffordanceTemplateExecuteCommand::Request> exe_stack_;
        boost::scoped_ptr<boost::thread> plan_thread_, exe_thread_, state_thread_;
        boost::mutex plan_mutex_, exe_mutex_;

    public:
        AffordanceTemplateInterface(const ros::NodeHandle&, const std::string&, const std::string&);
        ~AffordanceTemplateInterface();
    };       
}

#endif