/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/


#ifndef _AFFORDANCE_TEMPLATE_SERVER_H_
#define _AFFORDANCE_TEMPLATE_SERVER_H_

#include <iostream>

#include <ros/ros.h>
#include <ros/package.h>

#include <tf/transform_listener.h>

#include <affordance_template_markers/robot_interface.h>
#include <affordance_template_markers/affordance_template.h>

#include <affordance_template_library/affordance_template_structure.h>
#include <affordance_template_library/affordance_template_parser.h>

#include <affordance_template_msgs/RobotConfig.h>
#include <affordance_template_msgs/AffordanceTemplateConfig.h>

#include <interactive_markers/interactive_marker_server.h>

#include <geometry_msgs/PoseStamped.h>

#include <boost/tokenizer.hpp>
#include <boost/thread.hpp>
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>

namespace affordance_template_server
{
  class AffordanceTemplateServer
  {
    void configureServer();
    bool loadRobot();
    bool loadTemplates();
    int getNextID(const std::string&);
    std::string getPackagePath(const std::string&);

    ros::NodeHandle nh_;
    boost::mutex mutex_;
    boost::mutex structure_map_mutex_;

    tf::TransformListener listener_;
    affordance_template_msgs::RobotConfig robot_config_;

    boost::shared_ptr<interactive_markers::InteractiveMarkerServer> im_server_;
    boost::shared_ptr<affordance_template_markers::RobotInterface> robot_interface_;

    std::map<std::string, boost::shared_ptr<affordance_template::AffordanceTemplate> > at_map_;
    std::map<std::string, affordance_template_object::AffordanceTemplateStructure> at_structure_map_;

    bool status_;
    std::string library_pkg_;
    std::string robot_yaml_;
    std::string robot_name_;
    
  public:
    AffordanceTemplateServer(){} // default constructor 
    AffordanceTemplateServer(const ros::NodeHandle &, const std::string&, const std::string&);
    ~AffordanceTemplateServer(){}
     
    inline bool getStatus() { return status_; }
    inline void setStatus(bool status) { status_ = status; }
    inline bool findTemplate(const std::string &name) { at_structure_map_.find(name) == at_structure_map_.end() ? false : true; }
    inline affordance_template_msgs::RobotConfig getRobotConfig() { return robot_config_; }
    
    std::vector<affordance_template_msgs::AffordanceTemplateConfig> getAvailableTemplates(const std::string &name="");
    std::vector<std::string> getRunningTemplates(const std::string &name="");

    bool loadRobot(const std::string&); // from file
    bool loadRobot(const affordance_template_msgs::RobotConfig&); // from msg

    bool refreshTemplates() { return loadTemplates(); }

    bool resetTemplate(const std::string&, const uint8_t);
    bool addTemplate(const std::string&, uint8_t&);
    bool addTemplate(const std::string&, uint8_t&, geometry_msgs::PoseStamped&);
    bool removeTemplate(const std::string&, const uint8_t);
    bool updateTemplate(const std::string&, const uint8_t, const geometry_msgs::PoseStamped&);

    bool getTemplateInstance(const std::string&, const uint8_t, boost::shared_ptr<affordance_template::AffordanceTemplate>&);
    bool getTemplateInstance(const std::string&, boost::shared_ptr<affordance_template::AffordanceTemplate>&);
  };
}

#endif 