/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/


#include <rviz_affordance_template_panel/affordance.h>


using namespace rviz_affordance_template_panel;

Affordance::Affordance(const std::string& class_type, const std::string& image_path, const std::vector<std::string> &display_objects, const std::string& filename) :
    class_type_(class_type),
    image_path_(image_path),
    filename_(filename),
    display_objs_(display_objects)
{

}

void Affordance::print()
{
    std::cout << "Affordance:" << std::endl;
    std::cout << "  type: " << class_type_ << std::endl;
    for(auto &s: display_objs_)
        std::cout << "    obj: " << s << std::endl;

}
