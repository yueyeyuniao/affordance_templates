/********************************************************************************
 *
 *   Copyright 2016 TRACLabs, Inc.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 ********************************************************************************/

#include <rviz_affordance_template_panel/waypoint_display.h>

using namespace rviz_affordance_template_panel;
using namespace std;

WaypointDisplay::WaypointDisplay(QObject *_parent) : QObject(_parent) {}


void WaypointDisplay::setupWaypointDisplayInfo(AffordanceTemplateStatusInfo::EndEffectorInfo wp_info) {

    ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo()");
           
    ui_->waypointDisplayTree->clear();
    eeNameMap_.clear();

    ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo");
    for(auto &wp : wp_info) {

      ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo() -- new waypoint");

      std::map<std::string,RobotConfigSharedPtr>::const_iterator it = robotMap_.find(robotName_);
      if (it == robotMap_.end() ) {
        ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo() -- error looking up key: %s in robot map!!", robotName_.c_str());
        return;
      }

      for (auto& e: (*robotMap_[robotName_]).endeffectorMap) {
        std::string ee_name = e.second->name();
        int n = wp_info[ee_name]->num_waypoints;
        ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo() -- num_waypoints: %d", n);
        if(n > 0) {
          //cout << "wpi[" << ee_name << "]: " << wp_info[ee_name]->compact_view.size() << endl;
          QTreeWidgetItem *eeTreeItem = new QTreeWidgetItem(ui_->waypointDisplayTree);
          eeTreeItem->setText(0, QString(ee_name.c_str()));
          eeTreeItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
          eeTreeItem->setCheckState(1,Qt::Unchecked);
          // eeTreeItem->setExpanded(true);
          ROS_DEBUG("WaypointDisplay::setupWaypointDisplayInfo() -- creating %d waypoint display boxes for %s", n, ee_name.c_str());
          for(int i=0; i<n; i++) {
            QTreeWidgetItem *wpTreeItem = new QTreeWidgetItem();
            wpTreeItem->setText(0, QString(std::to_string(i).c_str()));
            wpTreeItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
            if(wp_info[ee_name]->waypoint_config.size() == n) {
                if(wp_info[ee_name]->waypoint_config[i].compact_view) {
                    wpTreeItem->setCheckState(1,Qt::Checked);
                } else {
                    wpTreeItem->setCheckState(1,Qt::Unchecked);                            
                }
            } else {
                wpTreeItem->setCheckState(1,Qt::Unchecked);                            
            }
            eeTreeItem->addChild(wpTreeItem);
            auto p = std::make_pair(ee_name, i);
            waypointDisplayItems_[p] = wpTreeItem;
            eeNameMap_[ee_name] = e.second->id();
          }
        }
      }
    }
}

void WaypointDisplay::displayEventChecked(QTreeWidgetItem *item, int i) {

    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    
    std::string str = item->text(0).toStdString();
    auto search = eeNameMap_.find(str);
    std::vector<std::string> wp_names;
    std::vector<bool> modes; 
    if(search != eeNameMap_.end()) {
        std::string ee_name = item->text(0).toStdString();
        for(auto &wpItem: waypointDisplayItems_) {
            QTreeWidgetItem *wpTreeItem = wpItem.second;
            auto p = wpItem.first; 
            if(p.first == str) {
                wpItem.second->setCheckState(1,item->checkState(i));  // swhart: surprised this doesnt call the callback. why not? 
                QString wpItemStr = wpItem.second->text(0);
                std::string wp_id = wpItemStr.toStdString();
                modes.push_back(item->checkState(i) == Qt::Checked);
                std::string wp_name = std::to_string(eeNameMap_[ee_name]) + "." + wp_id + ":" + template_status_->getName() + ":" + std::to_string(template_status_->getID());
                wp_names.push_back(wp_name);
            }  
        }
    } else {
        QTreeWidgetItem *eeTreeItem = item->parent();
        QString eeItemStr = eeTreeItem->text(0);
        QString wpItemStr = item->text(0);
        std::string ee_name = eeItemStr.toStdString();
        std::string wp_id = wpItemStr.toStdString();
        bool compact_mode = item->checkState(i) == Qt::Checked;
        std::string wp_name = std::to_string(eeNameMap_[ee_name]) + "." + wp_id + ":" + template_status_->getName() + ":" + std::to_string(template_status_->getID());
        wp_names.push_back(wp_name);
        modes.push_back(compact_mode);
    }
    callWaypointDisplayService(wp_names, modes);
}


void WaypointDisplay::callWaypointDisplayService(std::vector<std::string> wp_names, std::vector<bool> modes) {
    if(wp_names.size() != modes.size()) {
        ROS_ERROR("WaypointDisplay::callWaypointDisplayService() size mismatch!");
    }
    affordance_template_msgs::SetWaypointViewModes srv;
    for(size_t idx=0; idx<wp_names.size(); idx++) {
        srv.request.waypoint_names.push_back(wp_names[idx]);
        srv.request.compact_view.push_back(modes[idx]);
    }
    if (setWaypointDisplayService_.call(srv))
        ROS_INFO("WaypointDisplay::callWaypointDisplayService() -- set waypoint display mode successful");
    else
        ROS_ERROR("WaypointDisplay::callWaypointDisplayService() -- Failed to set waypoint display mode");
}
