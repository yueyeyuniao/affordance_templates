#ifndef AFFORDANCE_TEMPLATE_MSG_HEADERS_HPP
#define AFFORDANCE_TEMPLATE_MSG_HEADERS_HPP

// messages
#include <affordance_template_msgs/AffordanceTemplateConfig.h>
#include <affordance_template_msgs/EndEffectorConfig.h>
#include <affordance_template_msgs/EndEffectorPoseData.h>
#include <affordance_template_msgs/RobotConfig.h>
#include <affordance_template_msgs/WaypointInfo.h>
#include <affordance_template_msgs/WaypointTrajectory.h>
#include <affordance_template_msgs/WaypointConfig.h>
#include <affordance_template_msgs/GripperActionMap.h>
#include <affordance_template_msgs/AffordanceTemplateConfigArray.h>
#include <affordance_template_msgs/AffordanceTemplateStatusArray.h>

// services
#include <affordance_template_msgs/AddAffordanceTemplate.h>
#include <affordance_template_msgs/AddAffordanceTemplateTrajectory.h>
#include <affordance_template_msgs/SaveAffordanceTemplate.h>
#include <affordance_template_msgs/AffordanceTemplatePlanCommand.h>
#include <affordance_template_msgs/AffordanceTemplateExecuteCommand.h>
#include <affordance_template_msgs/DeleteAffordanceTemplate.h>
#include <affordance_template_msgs/GetAffordanceTemplateConfigInfo.h>
#include <affordance_template_msgs/GetRobotConfigInfo.h>
#include <affordance_template_msgs/GetRunningAffordanceTemplates.h>
#include <affordance_template_msgs/GetAffordanceTemplateStatus.h>
#include <affordance_template_msgs/GetAffordanceTemplateServerStatus.h>
#include <affordance_template_msgs/LoadRobotConfig.h>
#include <affordance_template_msgs/ScaleDisplayObjectInfo.h>
#include <affordance_template_msgs/ScaleDisplayObject.h>
#include <affordance_template_msgs/SetAffordanceTemplateTrajectory.h>
#include <affordance_template_msgs/SetWaypointViewModes.h>
#include <affordance_template_msgs/SetObjectPose.h>
#include <affordance_template_msgs/ConfigureWaypoint.h>

#endif // AFFORDANCE_TEMPLATE_MSG_HEADERS_HPP
