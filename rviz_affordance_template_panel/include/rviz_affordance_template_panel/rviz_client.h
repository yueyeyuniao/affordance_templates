#ifndef AFFORDANCE_TEMPLATE_RVIZ_CLIENT_HPP
#define AFFORDANCE_TEMPLATE_RVIZ_CLIENT_HPP

/* ROS Includes */
#include <ros/ros.h>
#include <rviz/panel.h>
#include <ros/package.h>

/* stuff for getting urdf */
#include <urdf/model.h>
#include <kdl/frames.hpp>

/* qt */
#include <QGraphicsScene>
#include <QTableWidgetItem>
#include <QSlider>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QPainter>
#include <QRectF>

/* Project Include */
#include <rviz_affordance_template_panel/affordance.h>
#include <rviz_affordance_template_panel/robot_config.h>
#include <rviz_affordance_template_panel/controls.h>
#include <rviz_affordance_template_panel/waypoint_display.h>
#include <rviz_affordance_template_panel/util.h>
#include "ui_rviz_affordance_template_panel.h"

#include <geometry_msgs/Pose.h>

#include <rviz_affordance_template_panel/msg_headers.h>
#include <rviz_affordance_template_panel/template_status_info.h>

#define PIXMAP_SIZE 100
#define XOFFSET 20
#define YOFFSET 20

namespace Ui {
class RVizAffordanceTemplatePanel;
}

namespace rviz_affordance_template_panel
{
    class AffordanceTemplateRVizClient 
    {

    public:

        // typedefs
        typedef boost::shared_ptr<Affordance> AffordanceSharedPtr;
        typedef boost::shared_ptr<RobotConfig> RobotConfigSharedPtr;
        typedef boost::shared_ptr<EndEffectorConfig> EndEffectorConfigSharedPtr;
        typedef boost::shared_ptr<EndEffectorPoseConfig> EndEffectorPoseIDConfigSharedPtr;
        typedef boost::shared_ptr<Controls> ControlsSharedPtr;
        typedef boost::shared_ptr<WaypointDisplay> WaypointDisplaySharedPtr;
        typedef std::pair<std::string, int> TemplateInstanceID;

        // Constructors
        AffordanceTemplateRVizClient(ros::NodeHandle &nh, Ui::RVizAffordanceTemplatePanel* ui);
        ~AffordanceTemplateRVizClient();

        // print stored template status info
        void printTemplateStatus();
        
        void refreshCallback();

        void getAvailableTemplates();
        void getAvailableRobots();
        void getRunningItems();

        void addAffordanceDisplayItem();
        void addTrajectory();

        void selectAffordanceTemplate(QListWidgetItem* item);
        void deleteAffordanceTemplate();
        void killAffordanceTemplate(QListWidgetItem* item);

        void saveAffordanceTemplate();
        void loadConfig();
        void safeLoadConfig();

        void changeRobot(int id);
        void changeSaveInfo(int id);
        void changeEndEffector(int id);

        void goToStart();
        void goToEnd();
        void stepBackward();
        void stepForward();
        void goToCurrentWaypoint();
        void executePlan();

        void updateRobotConfig(const QString& text);
        void updateEndEffectorGroupMap(const QString&);
        void updateObjectScale(int value);
        void updateEndEffectorScaleAdjustment(int value);
        void selectScaleObject(const std::string& object_name);
        void scaleSliderReleased();
        void controlStatusUpdate();
        void resetScale();      
        void enableConfigPanel(int state);
        void selectTemplateTrajectory(const QString& text);
        void selectTemplate(const QString& text);

        void handleConfigCallback(const affordance_template_msgs::AffordanceTemplateConfigArray &data);
        void handleStatusCallback(const affordance_template_msgs::AffordanceTemplateStatusArray &data);
        
    protected:

        void setupRobotPanel(const string& key);
        void setupEndEffectorConfigPanel(const string& key);

        bool loadRobotFromYAML();

        int  sendAffordanceTemplateAdd(const string& class_name);
        void sendAffordanceTemplateKill(const string& class_name, int id);
        void sendSaveAffordanceTemplate();
        void sendAddTrajectory();

        void sendObjectScale(affordance_template_msgs::ScaleDisplayObjectInfo scale_info);
        void streamObjectScale(affordance_template_msgs::ScaleDisplayObjectInfo scale_info);

        bool addAffordanceIcon(const string& class_type, const string& image_path, const string& filename, int xoffset, int yoffset);
        // bool addAffordance(AffordanceSharedPtr& obj);
        bool removeAffordance(const std::string key);
        bool checkAffordance(const std::string key);

        bool addRobot(const RobotConfigSharedPtr& obj);
        bool removeRobot(const RobotConfigSharedPtr& obj);
        bool checkRobot(const RobotConfigSharedPtr& obj);

        void setLabelText(QColor color, std::string text);

        void updateStatusFromControls();   

        void updateTables(std::string name, std::string trajectory);
        void updateControlsTable(std::string name, std::string trajectory);
        void updateWaypointDisplayTable(std::string name, std::string trajectory);

        void doCommand(Controls::CommandType command_type);
        void sendScaleInfo();
        void setupDisplayObjectSliders(TemplateInstanceID template_instance);
        bool endEffectorInTrajectory(AffordanceTemplateStatusInfo::EndEffectorInfo ee_info);

        std::string createShortName(const std::string&);
        std::string getLongName(const std::string&);
        std::map<std::string, std::string> robot_name_map_; 
                
        std::string getRobotFromDescription();
        std::vector<std::string> getSelectedEndEffectors();

        // status flag
        bool busy_flag_;
        int server_status_;
        
        // ros node handle
        ros::NodeHandle nh_;

        // status subscribers
        ros::Subscriber status_sub_;
        ros::Subscriber config_sub_;

        // UI reference
        Ui::RVizAffordanceTemplatePanel* ui_;

        // GUI Widgets
        // QGraphicsScene* affordanceTemplateGraphicsScene_;

        // map to track instantiated object templates
        std::map<std::string, AffordanceSharedPtr> affordanceMap_;
        std::map<std::string, QGraphicsPixmapItem *> iconMap_;
        std::map<std::string, RobotConfigSharedPtr> robotMap_;
        std::string descriptionRobot_;
        std::string robot_name_;
        bool robot_configured_;
        bool waypoint_display_configured_;

        // affordance template services
        ros::ServiceClient add_template_client_;
        ros::ServiceClient delete_template_client_;
        ros::ServiceClient add_trajectory_client_;
        ros::ServiceClient plan_command_client_;
        ros::ServiceClient execute_command_client_;
        ros::ServiceClient get_robots_client_;
        ros::ServiceClient get_running_client_;
        ros::ServiceClient get_templates_client_;
        ros::ServiceClient load_robot_client_;
        ros::ServiceClient save_template_client_;
        ros::ServiceClient scale_object_client_;
        ros::ServiceClient get_template_status_client_;
        ros::ServiceClient set_template_trajectory_client_;
        ros::ServiceClient set_waypoint_view_client_;

        // affordance template publishers
        ros::Publisher scale_object_streamer_;

        // control helper class
        ControlsSharedPtr controls_;

        // waypoint display helper class
        WaypointDisplaySharedPtr waypointDisplay_;
        std::map<std::string,bool> waypointExpansionStatus_;

        // template bookkeeping
        TemplateInstanceID selected_template;
        std::map<std::pair<TemplateInstanceID, std::string>, int> display_object_scale_map;
        std::map<std::pair<TemplateInstanceID, std::string>, int> end_effector_adjustment_map;
        std::map<std::string, AffordanceTemplateStatusInfo*> template_status_info; 

        boost::mutex status_mutex_, config_mutex_, affordance_mutex_, display_mutex_;
        affordance_template_msgs::AffordanceTemplateStatusArray current_status_;
        affordance_template_msgs::AffordanceTemplateConfigArray current_config_;

        std::vector<std::string> runningTemplates_;

        // extra graphics stuff
        QPalette *label_palette_;
        QColor red, blue, green;

    };
}
#endif // AFFORDANCE_TEMPLATE_RVIZ_CLIENT_HPP