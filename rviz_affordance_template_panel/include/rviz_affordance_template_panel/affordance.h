#ifndef AFFORDANCE_HPP
#define AFFORDANCE_HPP

// qt

#include <QVariant>
#include <QMap>

#include <iostream>


namespace rviz_affordance_template_panel
{
    class Affordance //: public QGraphicsPixmapItem
    {
    public:
        Affordance(const std::string& class_type, const std::string& image_path, const std::vector<std::string> &display_objects, const std::string& filename);
        ~Affordance() {
            // map_.clear();
        }

        void print();

        std::string getType() { return class_type_; }
        std::string getImagePath() { return image_path_; }
        std::string getFilename() { return filename_; }
        std::vector<std::string> getDisplayObjects() const { return display_objs_; }
        
    private:

        std::string class_type_;
        std::string image_path_;
        std::string filename_;
        std::vector<std::string> display_objs_;

        
    };
}

#endif