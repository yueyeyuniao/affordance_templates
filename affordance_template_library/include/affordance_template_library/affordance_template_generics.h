#ifndef _AT_GENERICS_H_
#define _AT_GENERICS_H_

#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>
#include <planner_interface/planner_interface.h>

namespace affordance_template_object
{
    struct Origin
    {
        double position[3]; // xyz
        double orientation[3]; // rpy
    };

    struct ToleranceBounds
    {
        double position[3][2]; // xyz [lo,hi]
        double orientation[3][2]; // rpy [lo,hi]
    };

    struct TaskCompatibility
    {
        int position[3]; // xyz
        int orientation[3]; // rpy
    };

    inline geometry_msgs::Pose originToPoseMsg(Origin origin) 
    { 
      geometry_msgs::Pose p;
      p.position.x = origin.position[0];
      p.position.y = origin.position[1];
      p.position.z = origin.position[2];
      p.orientation = tf::createQuaternionMsgFromRollPitchYaw(origin.orientation[0],origin.orientation[1],origin.orientation[2]);
      return p;
    }

    inline void originToVector3Msg(Origin origin, geometry_msgs::Vector3 &xyz, geometry_msgs::Vector3 &rpy) 
    { 
      xyz.x = origin.position[0];
      xyz.y = origin.position[1];
      xyz.z = origin.position[2];
      rpy.x = origin.orientation[0];
      rpy.y = origin.orientation[1];
      rpy.z = origin.orientation[2];
    }

    inline Origin poseMsgToOrigin(geometry_msgs::Pose p) 
    { 
      Origin origin;
      tf::Quaternion q;
      double roll, pitch, yaw;

      tf::quaternionMsgToTF(p.orientation, q);
      tf::Matrix3x3(q).getRPY(roll, pitch, yaw);

      origin.position[0] = p.position.x;
      origin.position[1] = p.position.y;
      origin.position[2] = p.position.z;
      origin.orientation[0] = roll;
      origin.orientation[1] = pitch;
      origin.orientation[2] = yaw;

      return origin;
    }

    inline Origin Vector3MsgToOrigin(geometry_msgs::Vector3 xyz, geometry_msgs::Vector3 rpy) 
    { 
      Origin origin;
      origin.position[0] = xyz.x;
      origin.position[1] = xyz.y;
      origin.position[2] = xyz.z;
      origin.orientation[0] = rpy.x;
      origin.orientation[1] = rpy.y;
      origin.orientation[2] = rpy.z;
      return origin;
    }

    inline geometry_msgs::Pose taskCompatibilityToPoseMsg(TaskCompatibility tc) 
    { 
      geometry_msgs::Pose p;
      p.position.x = tc.position[0];
      p.position.y = tc.position[1];
      p.position.z = tc.position[2];
      p.orientation = tf::createQuaternionMsgFromRollPitchYaw(tc.orientation[0],tc.orientation[1],tc.orientation[2]);
      return p;
    }

    inline planner_interface::PlannerType stringToPlannerType(std::string planner_type) {
        if(planner_type == "CARTESIAN") {
            return planner_interface::CARTESIAN;
        } else {
            return planner_interface::JOINT;  
        }
    }    

    struct Shape
    {
        std::string color;
        double rgba[4];
        std::string type;
        std::string mesh; // if type == "mesh" then we need to fill the file name in here
        double size[3]; // for boxes and meshes
        double length; // for cylinders
        double radius; // for spherse and cylinders
    };

    inline std::string toBoolString(bool b) { return (b ? "true" : "false"); }

    struct Control
    {
        bool translation[3]; // xyz
        bool rotation[3]; // rpy
        double scale;
    };
}

#endif